#!/usr/bin/python

### script for checking conformity between ABI and Instances
### script was done based on extractSize.py in system-contracts
import json
import sys
import glob
import re
from os import path

class UnixTermColor():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    BOLD = '\33[1m'
    BLINK= '\33[5m'
    RESET = '\033[0m'

ABIPaths = sys.argv[1:]
ABIPath = 'src/abi/'
InstancesPath = 'src/contracts/**/'
contractRegistryInstance = 'ContractRegistryInstance.ts'

# Store function names for each ABI
ABIFunctions = {}
# Store functions names for each instance
InstanceFunctions = {}

def getABIFunctions(json_object):
    return [item.get('name') for item in json_object if item.get('type') == 'function' and (len(item.get('inputs')) == 0 or item.get('inputs')[0]['name'] != '')]

def openFile(fileName):
    try:
        f = open(fileName, encoding = 'utf-8', mode = 'r')
    except Exception as e:
        print(e, file = sys.stderr)
        return False
    return f

def readFile(fileName):
    f = openFile(fileName)
    if f:
        return f.read()
    return False

def findPath(pathPattern):
    files = glob.glob(pathPattern, recursive=True)
    return files

def findInText(pattern, text):
    return re.findall(pattern, text)

def getInstanceNames():
    pathPattern = InstancesPath + contractRegistryInstance
    files = findPath(pathPattern)
    if len(files) == 0:
        print('Contract registry was not found')
        return []
    pthName = files[0]
    registryText = readFile(pthName)
    instanceNames = findInText(r'async \w+\(.+Promise\s{0,}<(\w+)>', registryText)
    return instanceNames

def getABIName(instanceText):
    return findInText(r'import \{.+\} from .+\./web3-contracts/(\w+)', instanceText)

def getBaseInstance(instanceText):
    return findInText(r'export[\sa-z]{0,9} class .+ extends (\b\w+\b)<.+>', instanceText)

def getInstanceFunctions(instanceName):
    if instanceName in InstanceFunctions:
        return InstanceFunctions[instanceName]
    instancePathPattern = '{}{}.ts'.format(InstancesPath, instanceName)
    files = findPath(instancePathPattern)
    if len(files) == 0:
        print('Cannot find instance {}'.format(instanceName))
        return []
    instanceText = readFile(files[0])
    if not instanceText:
        print('Cannot read instance {}'.format(instanceName))
        return []
    InstanceFunctions[instanceName] = findInText(r'async (\b\w+\b)\s{0,}\(', instanceText)
    baseInstances = getBaseInstance(instanceText)
    if len(baseInstances) == 0:
        return InstanceFunctions[instanceName]
    baseInstance = baseInstances[0]
    if baseInstance not in InstanceFunctions:
        InstanceFunctions[instanceName] += getInstanceFunctions(baseInstance)
    else:
        InstanceFunctions[instanceName] += InstanceFunctions[baseInstance]
    return InstanceFunctions[instanceName]

def prepareForChecking():
    ABIFunctions.clear()
    for fileName in ABIPaths:
        f = openFile(fileName)
        if not f:
            continue
        
        try:
            abiFileContent = json.load(f)
        except Exception as e:
            print(e, fileName, "is it json file?", file = sys.stderr)
            continue

        f.close()
        ABINAme = path.basename(fileName).split('.')[0]
        ABIFunctions[ABINAme] = getABIFunctions(abiFileContent)

def checkMissingFunctions(name):
    instanceName = '{}.ts'.format(name)
    pathPattern = InstancesPath + instanceName
    files = findPath(pathPattern)
    print('\t{}{}'.format(UnixTermColor.WHITE, (name).ljust(35)), end = '')
    if len(files) == 0:
        print('{}Instance not found: {}{}'.format(UnixTermColor.YELLOW, instanceName, UnixTermColor.RESET))
        return
    pathName = files[0]
    instanceData = readFile(pathName)
    if not instanceData:
        print('{}Cannot read file: {}{}'.format(UnixTermColor.YELLOW, pathName, UnixTermColor.RESET))
        return
    notFoundFunctions = []
    instanceFunctions = getInstanceFunctions(name)
    ABINames = getABIName(instanceData)
    if len(ABINames) == 0 or ABINames[0] not in ABIFunctions:
        print('{}ABI was not found{}'.format(UnixTermColor.RED, UnixTermColor.RESET))
        return
    functions = ABIFunctions[ABINames[0]]
    for function in functions:
        if function not in instanceFunctions and function != 'initialize':
            notFoundFunctions.append(function)

    notFoundFunctionsLength = len(notFoundFunctions)
    if notFoundFunctionsLength == 0:
        print('{}Instance found, all functions found{}'.format(UnixTermColor.GREEN, UnixTermColor.RESET))
    else:
        print('{}Instance found, functions not found: {}{}{}'.format(UnixTermColor.YELLOW, UnixTermColor.RED, notFoundFunctionsLength, UnixTermColor.RESET))
        for function in notFoundFunctions:
            print('\t\t{}{}{}'.format(UnixTermColor.RED, function, UnixTermColor.RESET))

def checkABIConformity():
    print('Checking ABI conformity:')
    prepareForChecking()
    instanceNames = getInstanceNames()
    for instanceName in instanceNames:
        checkMissingFunctions(instanceName)

checkABIConformity()