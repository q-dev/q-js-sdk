#!/bin/sh

rm -rf docs/
git fetch
git checkout -f $1
git pull origin $1
if [ ! -z $2 ]; then
  sed -i -z "s/\"pattern\": \"@network\",\n\s*\"replace\": \"\w*\"/\"pattern\": \"@network\",\n                \"replace\": \"$2\"/" "tsconfig.json"
fi
npm i
npm run generate-types
npm run docgen
cp -r docs public/$2
