<div align="center"><img src="img/q-logo.png" width="128"></div>

<div align="center"><h1>Q JS SDK</h1></div>

<div align="center">
  
  **[Documentation](https://q-dev.gitlab.io/q-js-sdk/latest)** •
  **[System Contracts](https://gitlab.com/q-dev/system-contracts)** •
  **[Q Blockchain](https://q.org)**

</div>

<div align="center"><p>Typescript Library to interact with Q System Contracts</p></div>

<div align="center">
  
  [![npm](https://img.shields.io/npm/v/@q-dev/q-js-sdk?color=ca0001)](https://www.npmjs.com/package/@q-dev/q-js-sdk)
  [![npm min zipped size](https://img.shields.io/bundlephobia/minzip/@q-dev/q-js-sdk)](https://bundlephobia.com/package/@q-dev/q-js-sdk@1.4.0)
  [![license](https://img.shields.io/npm/l/@q-dev/q-js-sdk)](LICENSE)
  [![pipeline status](https://gitlab.com/q-dev/q-js-sdk/badges/master/pipeline.svg)](https://gitlab.com/q-dev/q-js-sdk/-/commits/master)

</div>


## Installation

Install package as dependency:

```bash
npm install --save @q-dev/q-js-sdk
```

or

```bash
yarn add @q-dev/q-js-sdk
```

### Requirements

* [Web3 ^1.7.0](https://www.npmjs.com/package/web3)


## Usage

### Initialize contract registry:

```ts
const contractRegistry = new ContractRegistryInstance(web3)
```

### Retrieve SDK version

```ts
import { Web3Adapter, Web3Factory, Web3 } from '@q-dev/q-js-sdk'

const web3 = Web3Factory.createFromMnemonic('<YOUR_RPC_URL>', '<MNEMONIC>')
const adapter = new Web3Adapter(web3)
const SDK_VERSION = adapter.SDK_VERSION
```

### Obtain system contract instances

```ts
const validators = await contractRegistry.validators()
```

### Call contract getter

```ts
const longList = await validators.getLongList()
```

### Submit contract transaction


To be able to process various transaction events, use [PromiEvent](https://web3js.readthedocs.io/en/v1.7.5/callbacks-promises-events.html)

```ts
const { promiEvent } = await validators.commitStake({from: '<USER_ADDRESS>', value: '<COMMIT_AMOUNT>'})
/**
 * We do not wait for the transaction to complete. 
 * This gives us the opportunity to subscribe to PromiEvent events
 */
promiEvent
  .once('sending', (payload) => { ... })
  .once('sent', (payload) => { ... })
  .once('transactionHash', (hash) => { ... })
  .once('receipt', (receipt) => { ... })
  .on('confirmation', (confNumber, receipt, latestBlockHash) => { ... })
  .on('error', (error) => { ... })

await promiEvent
```

## Local development

### Install packages
    
```bash
npm install
```

### Generate types

```bash
npm run generate-types
```

If generate-types indicates 0 matching files (on Windows machines) you can try the adapted script `npm run generate-types-win`

### Contribute

Make sure to run `pre-commit` script before commit

```bash
npm run pre-commit
```

You may check conformity between system-contracts and sdk via `abi-conformity-check`. But remember, we don't need all methods from each contract. Most of them are used by the system.

```bash
npm run abi-conformity-check
```

### Update ABI files

Download latest abi files, for a specific system contracts **tag**:
* [Devnet](https://gitlab.com/q-dev/system-contracts/-/jobs/artifacts/devnet/download?job=extract-abi)
* [Master](https://gitlab.com/q-dev/system-contracts/-/jobs/artifacts/master/download?job=extract-abi)
* Custom tag: https://gitlab.com/q-dev/system-contracts/-/jobs/artifacts/\<TAG_NAME\>/download?job=extract-abi

Then call

```bash
./update-abis.sh <path/to/abi.zip>
```

### Run tests

You may run tests via

```bash
npm run test
```

### Get access to private Gitlab registry

Log in to GitLab NPM package registry. You need a gitlab personal access token with `read_api` option enabled. This can be created via [GitLab User Settings / Access Tokens](https://gitlab.com/-/profile/personal_access_tokens).

```bash
npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' <YOUR_ACCESS_TOKEN>
```

Furthermore you need to tell npm that all `q-dev` packages should be resolved via private gitlab registry. Create a file `.npmrc` with the following content:

```
@q-dev:registry=https://gitlab.com/api/v4/packages/npm/
```

## Code of Conduct

This project and everyone participating in it is governed by the
[Q JS SDK Code of Conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

## Resources

* [Changelog](CHANGELOG.md)
* [Contributing Guide](CONTRIBUTING.md)
* [System Contracts](https://q-dev.gitlab.io/system-contracts)
* [Web3 Documentation](https://web3js.readthedocs.io/en/v1.7.0/)

## License

[LGPL-3.0](LICENSE)
