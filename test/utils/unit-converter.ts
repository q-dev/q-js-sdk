import { should } from 'chai'
import { UnitConverter } from '../../src/utils/unit-converter'
import { MathHelper } from '../helpers/mathHelper'

should()

describe('/utils/unit-converter', () => {
  it('should convert timestamp back and forth', async () => {
    const converter = new UnitConverter()
    const date = new Date('2021-03-15T08:48:00.000Z')
    const timestamp = converter.toTimestamp(date)
    const convertedBack = converter.fromTimestamp(timestamp)

    convertedBack.toISOString().should.equal(date.toISOString(), 'converting back and forth must result in equal value')
    timestamp.should.equal('1615798080')
  })

  it('should convert arbitrary smallest unit representation back and forth', async () => {
    const converter = new UnitConverter()
    const num = '0.05'
    const digitShift = 8
    const smallestUnitStr = converter.toSmallestUnit(num, digitShift)
    const convertedBack = converter.fromSmallestUnit(smallestUnitStr, digitShift)

    convertedBack.should.equal(num, 'converting back and forth must result in equal value')
    smallestUnitStr.should.equal('5000000')
  })

  it('should convert fraction back and forth', async () => {
    const converter = new UnitConverter()
    const num = '1.05'
    const fraction = converter.toFraction(num)
    const convertedBack = converter.fromFraction(fraction)

    convertedBack.should.equal(num, 'converting back and forth must result in equal value')
    fraction.should.equal('1050000000000000000000000000')
  })

  it('should convert fraction as percentage back and forth', async () => {
    const converter = new UnitConverter()
    const num = '10.5'
    const fraction = converter.toFraction(num, true)
    const convertedBack = converter.fromFraction(fraction, true)

    convertedBack.should.equal(num, 'converting back and forth must result in equal value')
    fraction.should.equal('105000000000000000000000000')
  })

  it('should convert number without fraction part to fraction ', async () => {
    const converter = new UnitConverter()
    const num = 5
    const fraction = converter.toFraction(num)

    fraction.should.equal('5000000000000000000000000000')
  })

  it('should convert annual rate back and forth', async () => {
    const converter = new UnitConverter()
    const ratePerYear = 0.10 // 10%
    const ratePerSecond = converter.toRatePerSecond(ratePerYear)
    const convertedBack = converter.fromRatePerSecond(ratePerSecond)

    MathHelper.assertAlmostEqual(convertedBack, ratePerYear, 4, 'converting back and forth must result in equal value')
    ratePerSecond.should.equal('3022266000000000000')
  })

  it('should convert annual rate > 100 %', async () => {
    const converter = new UnitConverter()
    const ratePerYear = 2 // 200%
    const ratePerSecond = converter.toRatePerSecond(ratePerYear)
    const convertedBack = converter.fromRatePerSecond(ratePerSecond)

    MathHelper.assertAlmostEqual(convertedBack, ratePerYear, 4, 'converting back and forth must result in equal value')
    ratePerSecond.should.equal('34836767700000000000')
  })
})
