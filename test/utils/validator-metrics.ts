import { StakeInfo, ValidatorMetrics } from '../../src/utils/validator-metrics'
import { UnitConverter, ValidatorPoolInfo } from '../../src'
import { should } from 'chai'
import { assert } from 'console'

should()
const c = new UnitConverter()

function getNewValidatorInfo(
  accountableStake: number,
  delegatedStake: number,
  selfStake: number,
  totalStake: number
): StakeInfo {
  return {
    address: '0x0000000000000000000000000000000000000000',
    selfStake: selfStake.toString(),
    delegatedStake: delegatedStake.toString(),
    totalStake: totalStake.toString(),
    currentDelegationFactor: totalStake / selfStake,
    accountableStake: accountableStake.toString(),
  }
}

function getNewValidatorPoolInfo(
  delegatedStake: number,
  delegatorsShare: number
): ValidatorPoolInfo {
  return {
    poolBalance: '',
    reservedForClaims: '',
    aggregatedNormalizedStake: '',
    delegatedStake: c.toSmallestUnit(delegatedStake, 18),
    compoundRate: '',
    lastUpdateOfCompoundRate: '',
    delegatorsShare: c.toFraction(delegatorsShare, true),
  }
}

function generateTestData(
  accountableStakes: number[],
  delegatedStakes: number[],
  delegatorsShare: number[],
  selfStakes: number[],
  totalStakes: number[]
): [StakeInfo[], ValidatorPoolInfo[]] {
  const itemNumber = accountableStakes.length
  assert(itemNumber === delegatedStakes.length)
  assert(itemNumber === delegatorsShare.length)
  assert(itemNumber === selfStakes.length)
  assert(itemNumber === totalStakes.length)

  const validatorInfo: StakeInfo[] = delegatedStakes.map((delegatedStake, i) =>
    getNewValidatorInfo(
      accountableStakes[i],
      delegatedStake,
      selfStakes[i],
      totalStakes[i]
    )
  )
  const validatorPoolInfo: ValidatorPoolInfo[] = delegatedStakes.map(
    (delegatedStake, i) =>
      getNewValidatorPoolInfo(delegatedStake, delegatorsShare[i])
  )

  return [validatorInfo, validatorPoolInfo]
}

async function testDelegationEfficiency(
  validatorMetrics: ValidatorMetrics,
  accountableStakes: number[],
  delegatedStakes: number[],
  delegatorsShare: number[],
  expectedDelegationEfficiencies: number[]
): Promise<void> {
  const selfStakes = new Array(accountableStakes.length).fill(1)
  const totalStakes = new Array(accountableStakes.length).fill(1)
  const [validatorInfo, validatorPoolInfo] = generateTestData(
    accountableStakes,
    delegatedStakes,
    delegatorsShare,
    selfStakes,
    totalStakes
  )

  validatorMetrics.setSnapShot(validatorInfo, validatorPoolInfo, '0')

  const delegationEfficiency = validatorMetrics.getDelegationEfficiency()

  for (let i = 0; i < delegationEfficiency.length; i++) {
    const realDelegationEfficiency = (+delegationEfficiency[i]
      .delegationEfficiency).toFixed(2)
    const expectedDelegationEfficiency =
      expectedDelegationEfficiencies[i].toFixed(2)
    realDelegationEfficiency.should.eq(
      expectedDelegationEfficiency,
      'real delegation efficiency does not equal to expected efficiency'
    )
  }
}

async function testDelegationSaturation(
  validatorMetrics: ValidatorMetrics,
  selfStakes: number[],
  totalStakes: number[],
  stakeDelegationFactor: string,
  expectedDelegationSaturations: number[]
): Promise<void> {
  const accountableStakes = new Array(selfStakes.length).fill(1)
  const delegatedStakes = new Array(selfStakes.length).fill(1)
  const delegatorsShare = new Array(selfStakes.length).fill(1)
  const [validatorInfo, validatorPoolInfo] = generateTestData(
    accountableStakes,
    delegatedStakes,
    delegatorsShare,
    selfStakes,
    totalStakes
  )

  validatorMetrics.setSnapShot(
    validatorInfo,
    validatorPoolInfo,
    stakeDelegationFactor
  )

  const delegationSaturations = validatorMetrics.getDelegationSaturation()

  for (let i = 0; i < delegationSaturations.length; i++) {
    const realDelegationSaturation = (+delegationSaturations[i]).toFixed(2)
    const expectedDelegationSaturation =
      expectedDelegationSaturations[i].toFixed(2)
    realDelegationSaturation.should.eq(
      expectedDelegationSaturation,
      'real delegation saturation does not equal to expected saturation'
    )
  }
}

describe('/validatorMetrics', () => {
  const validatorMetrics = new ValidatorMetrics()
  const stakeDelegationFactor = c.toSmallestUnit(10, 27)

  it('should calculate delegation efficiency, artificially prepared dataset', async () => {
    const accountableStakes: number[] = [50000, 30000, 20000]
    const delegatedStakes: number[] = [20000, 15000, 15000]
    const delegatorsShare: number[] = [45, 75, 10]

    const expectedDelegEfficiency: number[] = [40.785498, 54.380665, 4.833837]

    await testDelegationEfficiency(
      validatorMetrics,
      accountableStakes,
      delegatedStakes,
      delegatorsShare,
      expectedDelegEfficiency
    )
  })

  it('should calculate delegation efficiency, dataset with dust delegation stake', async () => {
    const accountableStakes: number[] = [50000, 30000, 20000, 1000]
    const delegatedStakes: number[] = [20000, 15000, 15000, 100]
    const delegatorsShare: number[] = [45, 75, 10, 80]

    const expectedDelegEfficiency: number[] = [40.785498, 54.380665, 4.833837, 0]

    await testDelegationEfficiency(
      validatorMetrics,
      accountableStakes,
      delegatedStakes,
      delegatorsShare,
      expectedDelegEfficiency
    )
  })

  it('should calculate delegation efficiency, real dataset', async () => {
    const accountableStakes: number[] = [
      84000.1, 40000, 38099.08, 3865.18, 572.76, 207, 178.65, 674, 110, 30, 10,
      2, 2, 2, 1.8,
    ]
    const delegatedStakes: number[] = [
      5248.99, 0, 10098, 859, 273.76, 0, 79.67, 1073.05, 0, 0, 0, 0, 0, 0, 0,
    ]
    const delegatorsShare: number[] = [
      1, 0, 13, 7, 5, 20, 19, 50, 50, 0, 0, 0, 0, 0, 0,
    ]

    const expectedDelegEfficiency = [
      11.56, 0, 35.44, 22.76, 7.56, 0, 0, 22.69, 0,
      0, 0, 0, 0, 0, 0,
    ]

    await testDelegationEfficiency(
      validatorMetrics,
      accountableStakes,
      delegatedStakes,
      delegatorsShare,
      expectedDelegEfficiency
    )
  })

  it('should calculate delegation efficiency, artificially prepared dataset, required in QDEV-2960', async () => {
    const accountableStakes: number[] = [50000, 30000, 500000, 100]
    const delegatedStakes: number[] = [0, 15000, 10000000, 100]
    const delegatorsShare: number[] = [45, 0, 50, 100]

    const expectedDelegEfficiency: number[] = [0, 0, 100, 0]

    await testDelegationEfficiency(
      validatorMetrics,
      accountableStakes,
      delegatedStakes,
      delegatorsShare,
      expectedDelegEfficiency
    )
  })

  it('should calculate delegation saturation, artificially prepared dataset', async () => {
    const selfStakes: number[] = [20000, 15000, 15000]
    const totalStakes: number[] = [50000, 30000, 20000]

    const expectedDelegSaturation: number[] = [25, 20, 13.333333]

    await testDelegationSaturation(
      validatorMetrics,
      selfStakes,
      totalStakes,
      stakeDelegationFactor,
      expectedDelegSaturation
    )
  })

  it('should calculate delegation saturation, real dataset', async () => {
    const selfStakes: number[] = [
      989878.30212440795979, 804202.52327314670571, 1012, 710,
    ]
    const totalStakes: number[] = [
      994587.11974917236471373, 830294.521354971383501974,
      34363.687373591329510951, 710,
    ]

    const expectedDelegSaturation: number[] = [
      10.047569661994395, 10.324445613222261, 339.56212819754273, 10,
    ]

    await testDelegationSaturation(
      validatorMetrics,
      selfStakes,
      totalStakes,
      stakeDelegationFactor,
      expectedDelegSaturation
    )
  })
})
