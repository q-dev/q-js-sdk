import { should } from 'chai'
import { ParameterType } from '../../src/types'
import { ParamVotingHelper } from '../../src/utils/param-voting-helper'

should()

describe('/utils/param-voting-helper', () => {
  it('should map addrValue to RawParameter and back', async () => {
    const helper = new ParamVotingHelper()
    const paramInput  = { paramKey: 'paramKey', paramType: ParameterType.ADDRESS, paramValue: '1x0000000000000000000000000000000000000000' }
    const mapped = helper.mapParameterToOnChainJson(paramInput)
    const mappedaddr = mapped[2]

    mappedaddr.should.equal('1x0000000000000000000000000000000000000000', 'wrong address conversion')

    const reverted = helper.mapOnChainJsonToParameter(mapped)
    reverted.paramKey.should.equal(paramInput.paramKey, 'key has not been mapped properly')
    reverted.paramType.should.equal(paramInput.paramType, 'type has not been mapped properly')
    reverted.paramValue.should.equal(paramInput.paramValue, 'value has not been mapped properly')
  })

  it('should map boolValue "true" to RawParameter and back', async () => {
    const helper = new ParamVotingHelper()
    const paramInput  = { paramKey: 'paramKey', paramType: ParameterType.BOOL, paramValue: 'true' }
    const mapped = helper.mapParameterToOnChainJson(paramInput)
    const mappedBool = mapped[3]

    mappedBool.should.equal(true, 'wrong boolean conversion')

    const reverted = helper.mapOnChainJsonToParameter(mapped)
    reverted.paramKey.should.equal(paramInput.paramKey, 'key has not been mapped properly')
    reverted.paramType.should.equal(paramInput.paramType, 'type has not been mapped properly')
    reverted.paramValue.should.equal(paramInput.paramValue, 'value has not been mapped properly')
  })

  it('should map boolValue "false" to RawParameter and back', async () => {
    const helper = new ParamVotingHelper()
    const paramInput  = { paramKey: 'paramKey', paramType: ParameterType.BOOL, paramValue: 'false' }
    const mapped = helper.mapParameterToOnChainJson(paramInput)
    const mappedBool = mapped[3]

    mappedBool.should.equal(false, 'wrong boolean conversion')

    const reverted = helper.mapOnChainJsonToParameter(mapped)
    reverted.paramKey.should.equal(paramInput.paramKey, 'key has not been mapped properly')
    reverted.paramType.should.equal(paramInput.paramType, 'type has not been mapped properly')
    reverted.paramValue.should.equal(paramInput.paramValue, 'value has not been mapped properly')
  })

  it('should map byteValue to RawParameter and back', async () => {
    const helper = new ParamVotingHelper()
    const paramInput  = { paramKey: 'paramKey', paramType: ParameterType.BYTE, paramValue: '0x0000000000000000000000000000000000000000000000000000000000000001' }
    const mapped = helper.mapParameterToOnChainJson(paramInput)
    const mappedByte = mapped[4]

    mappedByte.should.equal('0x0000000000000000000000000000000000000000000000000000000000000001', 'wrong byte conversion')

    const reverted = helper.mapOnChainJsonToParameter(mapped)
    reverted.paramKey.should.equal(paramInput.paramKey, 'key has not been mapped properly')
    reverted.paramType.should.equal(paramInput.paramType, 'type has not been mapped properly')
    reverted.paramValue.should.equal(paramInput.paramValue, 'value has not been mapped properly')
  })

  it('should map stringValue to RawParameter and back', async () => {
    const helper = new ParamVotingHelper()
    const paramInput  = { paramKey: 'paramKey', paramType: ParameterType.STRING, paramValue: 'true' }
    const mapped = helper.mapParameterToOnChainJson(paramInput)
    const mappedString = mapped[5]

    mappedString.should.equal('true', 'wrong string conversion')

    const reverted = helper.mapOnChainJsonToParameter(mapped)
    reverted.paramKey.should.equal(paramInput.paramKey, 'key has not been mapped properly')
    reverted.paramType.should.equal(paramInput.paramType, 'type has not been mapped properly')
    reverted.paramValue.should.equal(paramInput.paramValue, 'value has not been mapped properly')
  })

  it('should map uintValue to RawParameter and back', async () => {
    const helper = new ParamVotingHelper()
    const paramInput  = { paramKey: 'paramKey', paramType: ParameterType.UINT, paramValue: '1' }
    const mapped = helper.mapParameterToOnChainJson(paramInput)
    const mappedUint = mapped[6]

    mappedUint.should.equal('1', 'wrong Uint conversion')

    const reverted = helper.mapOnChainJsonToParameter(mapped)
    reverted.paramKey.should.equal(paramInput.paramKey, 'key has not been mapped properly')
    reverted.paramType.should.equal(paramInput.paramType, 'type has not been mapped properly')
    reverted.paramValue.should.equal(paramInput.paramValue, 'value has not been mapped properly')
  })
})
