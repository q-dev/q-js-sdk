export class MochaHelper {
  public static skipWithReason(mochaContext: Mocha.Context, reason: string): void {
    console.warn(`Skiping test... Reason: ${reason}`)
    mochaContext.skipReason = reason
    mochaContext.skip()
  }
}