import { should } from 'chai'
should()

export class MathHelper {
  public static assertAlmostEqual(a: number | string, b: number | string, precision: number, message?: string): void {
    const aNum = Number(a)
    const bNum = Number(b)

    const aRounded = aNum.toFixed(precision)
    const bRounded = bNum.toFixed(precision)

    aRounded.should.equal(bRounded, message)
  }
}