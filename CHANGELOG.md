# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0-rc.21] - 2024-01-22
### Added
- `getEncodedDataByABI` & `getDecodedDataByABI` to `abi-helpers`

## [2.0.0-rc.20] - 2023-12-13
### Fixed
- `DUSTED_DELEGATED_STAKE` on validator metrics

## [2.0.0-rc.19] - 2023-12-07
### Fixed
- `dueCycles` type of `FirstObservedApproval`

## [2.0.0-rc.18] - 2023-12-07
### Fixed
- `delegationEfficiency` for dusted validators

## [2.0.0-rc.17] - 2023-11-28
### Fixed
- `GenericContractRegistryVotingInstance` base class
- `genericContractRegistryVoting` in `ContractRegistryInstance`
- `stats_` type in root node approval contracts' ABIs

## [2.0.0-rc.16] - 2023-11-20
### Fixed
- `gasBuffer` override on `BaseContractInstance`

## [2.0.0-rc.15] - 2023-10-11
### Added
- Generic Contract Registry Voting Instance

## [2.0.0-rc.14] - 2023-10-10
### Added
- ABI encode/decode helpers
- Gnosis Safe helpers
- Multicall support for `ContractRegistryInstance`

## [2.0.0-rc.13] - 2023-09-20
### Changed
- `getRootNodeMetrics` response interface on `indexer`

## [2.0.0-rc.12] - 2023-09-19
### Removed
- Error handling from indexer methods

## [2.0.0-rc.11] - 2023-09-14
### Added
- Methods `getRNQTHVotingsAggregated`, `getRNVotingsAggregated`, `getRNProposalsAggregated` to `indexer`

## [2.0.0-rc.10] - 2023-09-05
### Added
- Methods `getRootNodeMetrics` to `indexer`

### Changed
- Method name `getRootNodeMetrics` to `getOnchainRootNodeMetrics` on `indexer`

## [2.0.0-rc.9] - 2023-08-08
### Added
- Methods `getL0ExclusionList` and `getL0RootList` to `indexer`

## [2.0.0-rc.8] - 2023-06-21
### Fixed
- `BigNumber` instance
- `fromWei` on `Web3Adapter`

## [2.0.0-rc.7] - 2023-06-20
### Added
- `getRootNodeMetrics` on `Indexer`

## [2.0.0-rc.6] - 2023-06-19
### Changed
- Function calls and data processing in `ValidatorMetrics` class

## [2.0.0-rc.5] - 2023-06-15
### Fixed
- `TimeLockHelperInstance` methods on `RootNodesInstance`, `ValidatorsInstance`, `QVaultInstance`, `VestingInstance`
- `ERC20HelperInstance` on `QVaultInstance`

## [2.0.0-rc.4] - 2023-06-14
### Added
- `getPayableTxOptions` on `BaseContractInstance`

### Removed
- `processPayableTxOptions` on `BaseContractInstance`

### Fixed
- `refreshTxDefaultOptions` on `BaseContractInstance`
- `createProposal` default params on `RootNodesMembershipVotingInstance`

## [2.0.0-rc.3] - 2023-06-01
### Changed
- Migrate from `web3` to `ethers`

### Fixed
- Estimate gas on `BaseContractInstance`
- `getStatus` return type on `BaseVotingInstance`
- Initializing `Web3Adapter`

## [2.0.0-rc.2] - 2023-04-18
### Added
- Votings methods on indexer

## [2.0.0-rc.1] - 2023-02-13
### Fixed
- Indexer. Fix inactive validators count.

## [2.0.0-rc.0] - 2022-12-21
### Added
- `BaseContractInstance.HANDLE_REVERT` variable

### Changed
- `submitTransaction` returns a wrapped PromiEvent
- `handleRevert` default is `false`

## [1.7.0] - 2022-11-22
### Added
- Reward KPI.

## [1.6.0] - 2022-11-15
### Added
- `WithdrawAddresses` contract.

## [1.5.2] - 2022-11-09
### Changed
- BaseContractInstance. Allow modifying default gas buffer.

## [1.5.1] - 2022-11-08
### Changed
- Indexer. Use main account for validator stats.

## [1.5.0] - 2022-10-31
### Added
- Changelog file.

## [1.4.0] - 2022-09-21
### Added
- Validator metrics to indexer.
- `SystemContractInstance` class for system contracts.
- CI script for publishing to NPM registry.

### Changed
- Make `BaseContractInstance` class reusable.

### Fixed
- Web3 dependencies.
- Proposal contract types.

## [1.3.1] - 2022-06-06
### Added
- Alias owner getter for `AccountAliases` contract.

## [1.3.0] - 2022-05-25
### Added
- `AccountAliasesInstance` contract instance.

## [1.2.0] - 2022-05-18
### Added
- Indexer method for getting inactive validators count.

### Fixed
- Validator stats in indexer.

## [1.1.9] - 2022-04-27
### Fixed
- Validator stats in indexer.
- Package lock dependencies.

## [1.1.8] - 2022-04-22
### Added
- `Indexer` class for interaction with indexer service.

## [1.1.7] - 2022-03-14
### Added
- `status` property for contract update proposals.

## [1.1.6] - 2022-02-21
### Added
- EPRS ABI imports to `AbiImporter`.

## [1.1.5] - 2022-02-16
### Added
- EPRS contract instances to `ContractRegistryInstance`.

## [1.1.4] - 2022-02-16
### Added
- EPRS contract instances.

## [1.1.3] - 2022-01-25
### Fixed
- Getting proposal stats for voting contracts.

## [1.1.2] - 2022-01-18
### Added
- Contract update ABI imports to `AbiImporter`.

### Fixed
- Getting proposal on contract update contract instances.

## [1.1.1] - 2022-01-18
### Added
- Contract update contract instances to `ContractRegistryInstance`.

## [1.1.0] - 2022-01-16
### Changed
- Update ABI files.

## [1.0.5] - 2022-01-11
### Added
- `RootNodeApprovalVoting` contract instance.
- `ContractRegistryAddressVoting` contract instance.
- `ContractRegistryUpgradeVoting` contract instance.

## [1.0.4] - 2021-12-07
### Fixed
- Registry key & ABI path in `TokenBridgeAdminProxyInstance`.

## [1.0.3] - 2021-12-03
### Added
- `purgePendingSlashings` method to `RootNodesInstance`.

## [1.0.2] - 2021-12-03
### Added
- `TokenBridgeAdminProxyInstance` contract instance.

### Changed
- Update docgen to match network.

## [1.0.1] - 2021-11-25
### Added
- ABI conformity check to pre-commit hook.
- Validator properties to `ValidationRewardPoolsInstance`.

### Changed
- Update README.

## [1.0.0] - 2021-11-16
### Added
- Docs README.
- Typedoc comments for contract instances.

### Changed
- Update LICENCE to LGPL-3.0.
- Update typedoc config.

## [0.9.5] - 2021-10-28

## [0.9.4] - 2021-10-26

## [0.9.3] - 2021-10-22

## [0.9.2] - 2021-10-20

## [0.9.1] - 2021-10-20

## [0.9.0] - 2021-10-13

## [0.8.22] - 2021-10-13

## [0.8.21] - 2021-10-11

## [0.8.20] - 2021-10-11

## [0.8.19] - 2021-09-24

## [0.8.18] - 2021-09-23

## [0.8.17] - 2021-09-22

## [0.8.16] - 2021-09-22

## [0.8.15] - 2021-09-22

## [0.8.14] - 2021-09-15

## [0.8.13] - 2021-09-09

## [0.8.12] - 2021-09-03

## [0.8.11] - 2021-09-01

## [0.8.10] - 2021-08-30

## [0.8.9] - 2021-08-30

## [0.8.8] - 2021-08-18

## [0.8.7] - 2021-08-16

## [0.8.6] - 2021-08-10

## [0.8.5] - 2021-08-10

## [0.8.4] - 2021-08-09

## [0.8.3] - 2021-08-06

## [0.8.2] - 2021-08-05

## [0.8.1] - 2021-08-04

## [0.8.0] - 2021-08-04

## [0.7.6] - 2021-07-26

## [0.7.5] - 2021-07-13

## [0.7.4] - 2021-07-12

## [0.7.3] - 2021-07-12

## [0.7.2] - 2021-07-12

## [0.7.1] - 2021-07-12

## [0.7.0] - 2021-07-01

## [0.6.4] - 2021-07-01

## [0.6.3] - 2021-06-28

## [0.6.2] - 2021-06-23

## [0.6.1] - 2021-06-17

## [0.6.0] - 2021-06-16

## [0.5.7] - 2021-05-10

## [0.5.6] - 2021-04-30

## [0.5.5] - 2021-04-16

## [0.5.4] - 2021-04-14

## [0.5.3] - 2021-04-09

## [0.5.2] - 2021-04-07

## [0.5.1] - 2021-04-01

## [0.5.0] - 2021-03-31

## [0.4.4] - 2021-03-24

## [0.4.3] - 2021-03-23

## [0.4.2] - 2021-03-22

## [0.4.1] - 2021-03-18

## [0.4.0] - 2021-03-18

## [0.3.3] - 2021-03-12

## [0.3.2] - 2021-03-10

## [0.3.1] - 2021-03-09

## [0.3.0] - 2021-03-09

## [0.2.5] - 2021-03-09

## [0.2.4] - 2021-03-09

## [0.2.3] - 2021-03-04

## [0.2.2] - 2021-03-04

## [0.2.1] - 2021-03-02

## 0.2.0 - 2021-03-01

[Unreleased]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.21...master
[2.0.0-rc.21]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.20...v2.0.0-rc.21
[2.0.0-rc.20]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.19...v2.0.0-rc.20
[2.0.0-rc.19]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.18...v2.0.0-rc.19
[2.0.0-rc.18]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.17...v2.0.0-rc.18
[2.0.0-rc.17]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.16...v2.0.0-rc.17
[2.0.0-rc.16]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.15...v2.0.0-rc.16
[2.0.0-rc.15]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.14...v2.0.0-rc.15
[2.0.0-rc.14]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.13...v2.0.0-rc.14
[2.0.0-rc.13]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.12...v2.0.0-rc.13
[2.0.0-rc.12]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.11...v2.0.0-rc.12
[2.0.0-rc.11]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.10...v2.0.0-rc.11
[2.0.0-rc.10]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.9...v2.0.0-rc.10
[2.0.0-rc.9]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.8...v2.0.0-rc.9
[2.0.0-rc.8]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.7...v2.0.0-rc.8
[2.0.0-rc.7]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.6...v2.0.0-rc.7
[2.0.0-rc.6]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.5...v2.0.0-rc.6
[2.0.0-rc.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.4...v2.0.0-rc.5
[2.0.0-rc.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.3...v2.0.0-rc.4
[2.0.0-rc.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.2...v2.0.0-rc.3
[2.0.0-rc.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.1...v2.0.0-rc.2
[2.0.0-rc.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v2.0.0-rc.0...v2.0.0-rc.1
[2.0.0-rc.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.7.0...v2.0.0-rc.0
[1.7.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.6.0...v1.7.0
[1.6.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.5.2...v1.6.0
[1.5.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.5.1...v1.5.2
[1.5.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.5.0...v1.5.1
[1.5.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.4.0...v1.5.0
[1.4.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.3.1...v1.4.0
[1.3.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.3.0...v1.3.1
[1.3.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.9...v1.2.0
[1.1.9]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.8...v1.1.9
[1.1.8]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.7...v1.1.8
[1.1.7]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.6...v1.1.7
[1.1.6]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.5...v1.1.6
[1.1.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.4...v1.1.5
[1.1.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.3...v1.1.4
[1.1.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.0.5...v1.1.0
[1.0.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.0.4...v1.0.5
[1.0.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.0.3...v1.0.4
[1.0.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.9.5...v1.0.0
[0.9.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.9.4...v0.9.5
[0.9.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.9.3...v0.9.4
[0.9.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.9.2...v0.9.3
[0.9.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.9.1...v0.9.2
[0.9.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.9.0...v0.9.1
[0.9.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.22...v0.9.0
[0.8.22]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.21...v0.8.22
[0.8.21]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.20...v0.8.21
[0.8.20]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.19...v0.8.20
[0.8.19]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.18...v0.8.19
[0.8.18]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.17...v0.8.18
[0.8.17]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.16...v0.8.17
[0.8.16]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.15...v0.8.16
[0.8.15]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.14...v0.8.15
[0.8.14]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.13...v0.8.14
[0.8.13]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.12...v0.8.13
[0.8.12]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.11...v0.8.12
[0.8.11]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.10...v0.8.11
[0.8.10]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.9...v0.8.10
[0.8.9]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.8...v0.8.9
[0.8.8]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.7...v0.8.8
[0.8.7]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.6...v0.8.7
[0.8.6]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.5...v0.8.6
[0.8.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.4...v0.8.5
[0.8.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.3...v0.8.4
[0.8.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.2...v0.8.3
[0.8.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.6...v0.8.0
[0.7.6]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.5...v0.7.6
[0.7.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.4...v0.7.5
[0.7.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.3...v0.7.4
[0.7.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.2...v0.7.3
[0.7.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.1...v0.7.2
[0.7.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.6.4...v0.7.0
[0.6.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.6.3...v0.6.4
[0.6.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.6.2...v0.6.3
[0.6.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.7...v0.6.0
[0.5.7]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.6...v0.5.7
[0.5.6]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.4.4...v0.5.0
[0.4.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.4.3...v0.4.4
[0.4.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.3.3...v0.4.0
[0.3.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.2.5...v0.3.0
[0.2.5]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.2.4...v0.2.5
[0.2.4]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/q-dev/q-js-sdk/compare/v0.2.0...v0.2.1
