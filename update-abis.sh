#!/bin/bash

ZIP_FILE=${1:-~/Desktop/abi.zip}
DEST=./src

rm $DEST/abi/*.json
echo Unzipping $ZIP_FILE to $DEST

unzip -uo $ZIP_FILE -d $DEST

echo updating generated types...
npm run generate-types
