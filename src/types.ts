import type { Signer, providers } from 'ethers'

import BN from 'bn.js'
import { CallOverrides } from 'ethers'

/**
 * Ethers signer or provider instance
 */
export type SignerOrProvider = Signer | providers.Provider

/* eslint-disable @typescript-eslint/no-explicit-any */
export type FnArgsWithoutOverrides<T extends (...args: any) => any> =
    T extends (...args: infer P) => any
      ? P extends [...infer Rest, any?]
        ? Rest
        : any
      : never
/* eslint-enable @typescript-eslint/no-explicit-any */

/**
 * Helps specify gasBuffer in non-payable transactions.
 */
export interface QNonPayableTx extends CallOverrides {
  /**
   * @field factor to be applied on the gas limit vs. the estimated gas use.
   * 1.3 would mean 30% above estimate.
   */
  gasBuffer?: string | number;
}

/**
 * Helps specify sent value in Q in payable transactions.
 */
export type QPayableTx = QNonPayableTx & {
  /**
   * @field sent amount in Q.
   * 1 would mean 10^18 Wei.
   */
  qAmount?: string | number;
}

/**
 * Used by {@link BaseParametersInstance} to retrieve parameters with special type.
 */
export interface KeyValuePair<TK, TV> {
  key: TK;
  value: TV;
}

export interface Identifiable {
  id: string;
}

/**
 * account info.
 * Used by {@link ValidatorsInstance}.
 * Also used by web3-adapter.
 */
export interface AddressWithBalance {
  address: string;
  balance: string;
}

export type ValidatorInfoRaw = {
  withdrawal: ValidatorsWithdrawalInfo
  pendingSlashingProposals: string[]
}

/**
 * General Validator info.
 * Retrieved by {@link ValidatorsInstance.getValidatorInfo} method.
 */
export interface ValidatorInfo {
  address: string
  selfStake: string
  pendingStake: string
  delegatedStake: string
  totalStake: string
  currentDelegationFactor: number
  accountableStake: string
}

/**
 * General Validation reward pool info.
 * Retrieved by {@link ValidationRewardPoolsInstance.getPoolInfo} method.
 */
export interface ValidatorPoolInfo {
  poolBalance: string
  reservedForClaims: string
  aggregatedNormalizedStake: string
  delegatedStake: string
  compoundRate: string
  lastUpdateOfCompoundRate: string
  delegatorsShare: string
}

/**
 * QVault stake delegation info.
 * Retrieved by {@link QVaultInstance.getDelegationsList} method.
 */
export interface StakeDelegationInfo {
  validator: string
  actualStake: string
  normalizedStake: string
  compoundRate: string
  latestUpdateOfCompoundRate: string
  idealStake: string
  claimableReward: string
}

/**
 * @deprecated use StakeDelegationInfo instead.
 */
export type DelegationValidatorInfo = StakeDelegationInfo

export type SystemContractWithQBalance = AddressWithBalance & {
  registryKey: string;
}

/**
 * Voting base information.
 * Used by {@link BaseProposal}
 */
export interface VotingParams {
  votingStartTime: string;
  votingEndTime: string;
  vetoEndTime: string;
  proposalExecutionP: string;
  requiredQuorum: string;
  requiredMajority: string;
  requiredSMajority: string;
  requiredSQuorum: string;
}

/**
 * Counter of voting weight.
 * Used by {@link BaseProposal}
 */
export interface VotingCounters {
  weightFor: string;
  weightAgainst: string;
  vetosCount: string;
}

/**
 * Voting statistics.
 * Used by {@link BaseVotingInstance.getProposalStats} and all derived instances.
 */
export interface VotingStats {
  requiredQuorum: string;
  currentQuorum: string;
  requiredMajority: string;
  currentMajority: string;
  currentVetoPercentage: string;
}

export enum ProposalStatus { NONE = '0', PENDING = '1', REJECTED = '2', ACCEPTED = '3', PASSED = '4', EXECUTED = '5', OBSOLETE = '6', EXPIRED = '7' }

/**
 * Base information about proposal.
 * Used by all voting instances {@link BaseVotingInstance}
 */
export interface BaseProposal {
  remark: string;
  params: VotingParams;
  counters: VotingCounters;
  executed: boolean;
}

export interface ProposalWithBaseInfo {
  base: BaseProposal;
}

export interface ProposalWithStatus extends ProposalWithBaseInfo, Identifiable {
  status: ProposalStatus;
}

/**
 * General voting weight info.
 * Used by {@link VotingWeightProxyInstance.getBaseVotingWeightInfo}
 */
export interface BaseVotingWeightInfo {
  ownWeight: string
  votingAgent: string
  delegationStatus: string
  lockedUntil: string
}

/**
 * Delegation info.
 * Used by {@link VotingWeightProxyInstance.getDelegationInfo}
 */
export interface VotingDelegationInfo {
  /**
   * @field total locked for voting amount.
   */
  receivedWeight: string
  /**
   * @field account that can use locked token amount for voting instead of locker (here may be specified the locker).
   */
  votingAgent: string
  isPending: boolean
  votingAgentPassOverTime: string
}

export enum DelegationStatus {
  SELF = '0',
  DELEGATED = '1',
  PENDING = '2'
}

/**
 * General information about available amount to withdraw from ValidatorsInstance.
 * Used by {@link ValidatorsInstance.getWithdrawalInfo}
 */
export type ValidatorsWithdrawalInfo = {
  /**
   * @field end time of locking. After this time user will be available to withdraw announced amount.
   */
  endTime: string
  /**
   * @field announced amount to withdraw.
   */
  amount: string
}

/**
 * General information about available amount to withdraw from RootNodesInstance.
 * Used by {@link RootNodesInstance.getWithdrawalInfo}
 */
export type RootNodesWithdrawalInfo = ValidatorsWithdrawalInfo

/**
 * General information about voting lock.
 * Used by instances that may lock some amount for voting: RootNodes, Validators, QVault, Vesting.
 * Also used by {@link VotingWeightProxyInstance.getLockInfo}
 */
export type VotingLockInfo = {
  lockedAmount: string
  lockedUntil: string
  pendingUnlockAmount: string
  pendingUnlockTime: string
}

/**
 * Timelock information.
 * Used by {@link TimeLockHelperInstance}
 */
export interface TimeLockEntry {
  amount: string
  releaseStart: string
  releaseEnd: string
}

/**
 * Vault information.
 * Used by {@link BorrowingCoreInstance}
 */
export type Vault = {
  colKey: string
  colAsset: string
  normalizedDebt: string
  mintedAmount: string
  isLiquidated: boolean
  liquidationFullDebt: string
}

/**
 * Borrowing totals collateral statistics.
 * Used by {@link VaultStats}
 */
export interface ColStats {
  key: string
  balance: string
  price: string
  withdrawableAmount: string
  liquidationPrice: string
}

/**
 * Borrowing totals stablecoin statistics.
 * Used by {@link VaultStats}
 */
export interface StcStats {
  key: string
  outstandingDebt: string
  normalizedDebt: string
  compoundRate: string
  lastUpdateOfCompoundRate: string
  borrowingLimit: string
  availableToBorrow: string
  liquidationLimit: string
  borrowingFee: string
}

/**
 * Borrowing totals.
 * Used by {@link BorrowingCoreInstance.getVaultStats}
 */
export interface VaultStats {
  colStats: ColStats
  stcStats: StcStats
}

/**
 * Borrowing totals.
 * Used by {@link BorrowingCoreInstance.getAggregatedTotals}
 */
export interface BorrowingTotals {
  outstandingDebt: string;
  mintedAmount: string;
  owedBorrowingFees: string;
}

/**
 * Saving balance details.
 * Used by {@link SavingInstance.getBalanceDetails}
 */
export interface SavingBalanceDetails {
  currentBalance: string;
  normalizedBalance: string;
  compoundRate: string;
  lastUpdateOfCompoundRate: string;
  interestRate: string;
}

/**
 * QVault balance details.
 * Used by {@link QVaultInstance.getBalanceDetails}
 */
export type QVaultBalanceDetails = SavingBalanceDetails

/**
 * System balance details.
 * Used by {@link SystemBalanceInstance.getBalanceDetails}
 */
export interface SystemBalanceDetails {
  currentDebt: string;
  debtThreshold: string;
  isDebtAuctionPossible: boolean;

  currentSurplus: string;
  surplusThreshold: string;
  isSurplusAuctionPossible: boolean;
}

export enum AuctionStatus { NONE = '0', ACTIVE = '1', CLOSED = '2' }

/**
 * General auction info.
 * Used by {@link LiquidationAuctionInstance.getAuctionInfo}
 */
export interface AuctionInfo {
  status: AuctionStatus;
  bidder: string;
  highestBid: string;
  endTime: Date;
}
export type LiquidationAuctionInfo = AuctionInfo

/**
 * System surplus auction info.
 * Used by {@link SystemSurplusAuctionInstance.getAuctionInfo}
 */
export interface SystemSurplusAuctionInfo extends AuctionInfo {
  lot: string;
}

/**
 * System surplus auction info.
 * Used by {@link SystemDebtAuctionInstance.getAuctionInfo}
 */
export interface SystemDebtAuctionInfo extends AuctionInfo {
  lot: string;
}

export enum ParameterType { NONE = '0', ADDRESS = '1', UINT = '2', STRING = '3', BYTE = '4', BOOL = '5' }

export interface RawParameter {
  paramKey: string;
  paramType: ParameterType;
  addrValue: string;
  boolValue: boolean;
  bytes32Value: string;
  strValue: string;
  uintValue: string;
}

export type RawParameterAsArray = [string, string, string, boolean, string, string, string]

export interface Parameter {
  paramType: ParameterType;
  paramKey: string;
  paramValue: string;
}

/**
 * Expert Constitution Proposal classification.
 * Used by {@link ConstitutionProposal}
 */
export enum Classification {
  BASIC,
  FUNDAMENTAL,
  DETAILED
}

/**
 * Expert Membership Proposal Details.
 * Used by {@link ExpertMembershipProposal}
 */
export interface ExpertMembershipProposalDetails {
  addressToAdd: string,
  addressToRemove: string
}

export type uint256 = number | string | BN

/**
 * General Arbitration Info's decision.
 * Used by {@link ArbitrationInfo}
 */
export interface Decision {
  notAppealed: boolean;
  proposer: string;
  confirmers: string;
  externalReference: string;
  percentage: uint256;
  confirmationCount: uint256;
  endDate: uint256;
  hash: string | number[];
}

/**
 * General Arbitration Info's params.
 * Used by {@link ArbitrationInfo}
 */
export interface ArbitrationParams {
  slashedAmount: uint256;
  objectionEndTime: uint256;
  appealEndTime: uint256;
}

/**
 * General Arbitration Info.
 * Retrieved by {@link BaseSlashingEscrowInstance | SlashingEscrow instances}
 */
export interface ArbitrationInfo {
  remark: string;
  proposerRemark: string;
  params: ArbitrationParams;
  decision: Decision;
  executed: boolean;
  appealConfirmed: boolean;
}

export interface ValidatorProperties {
  balance: string;
  reservedForClaim: string;
  delegatorsShare: string;
  aggregatedNormalizedStake: string;
  compoundRate: string;
}

export interface ProposalStats {
  currentMajority: uint256;
  requiredMajority: uint256;
}

export interface GenericContractRegistryProposal {
  id: uint256;
  status: ProposalStatus;
  executed: boolean;
  votingStartTime: uint256;
  votingExpiredTime: uint256;
  requiredMajority: uint256;
  callData: string;
  remark: string;
}

export interface SetKeyProposal {
  id: uint256;
  executed: boolean;
  votingStartTime: uint256;
  votingExpiredTime: uint256;
  key: string;
  proxy: string;
  status: ProposalStatus;
}
export interface UpgradeProposal {
  id: uint256;
  executed: boolean;
  votingStartTime: uint256;
  votingExpiredTime: uint256;
  proxy: string;
  implementation: string;
  status: ProposalStatus;
}

export enum AliasPurpose {
  BLOCK_SEALING = '0xac1c67647cbdc0261ee21863e0dcd233307d62845e0ab39b5e890ce32de5a917', // keccak256("BLOCK_SEALING")
  ROOT_NODE_OPERATION = '0x33a9d3006f267399569cda2996bb19776f92c98b990053176d19c710ed251a5d' // keccak256("ROOT_NODE_OPERATION")
}
export interface Alias {
  address: string;
  purpose: AliasPurpose;
}

export interface WithdrawalAccount {
  addr: string;
  isFinalized: boolean;
}

export interface DecodedData {
  functionName: string;
  arguments: any;
}
