export function buildSignatureForUpgrade(accounts: string[]): string {
  const accountSignatures = accounts.map((account) => {
    return '0'.repeat(24) +
        account.toLowerCase().replace('0x', '') +
        `${'0'.repeat(65)}1`
  })

  return `0x${accountSignatures.join('')}`
}
