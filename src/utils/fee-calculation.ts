import { SignerOrProvider } from '../types'
import { EPQFIParametersInstance } from '../contracts/governance/experts/EPQFIParametersInstance'
import { IFxPriceFeedInstance } from '../contracts/defi/oracles/IFxPriceFeedInstance'
import BN from 'bn.js'
import { ContractRegistryInstance } from '../contracts/ContractRegistryInstance'

export class FeeCalculation {
  qfiParams: EPQFIParametersInstance

  constructor(public readonly signerOrProvider: SignerOrProvider) {
  }

  async lazyInit(): Promise<void> {
    if (this.qfiParams) return

    const registry = new ContractRegistryInstance(this.signerOrProvider)
    this.qfiParams = await registry.epqfiParameters()
  }

  async calculateGasPrice(): Promise<number> {
    try {
      await this.lazyInit()
    } catch (e) {
      console.log(`Cannot initialize fee calculater: ${e}`)
      return this.getDefaultGasPrice()
    }

    let txFee = new BN(0)
    try {
      txFee = new BN(await this.qfiParams.getUint('governed.EPQFI.txFee'))
    } catch (e) {
      console.log(`Cannot determine transaction fee: ${e}`)
      return this.getDefaultGasPrice()
    }

    let oracleAddress
    try {
      oracleAddress = await this.qfiParams.getAddr('governed.EPQFI.Q_QUSD_source')
    }
    catch (e) {
      console.log(`Cannot determine oracle address: ${e}`)
      return this.getDefaultGasPrice()
    }

    const priceFeed = await new IFxPriceFeedInstance(this.signerOrProvider, oracleAddress)

    let exchangeRateRaw = '0'
    try {
      exchangeRateRaw = await priceFeed.exchangeRate()
    }
    catch (e) {
      console.log(`Cannot determine exchangeRate: ${e}`)
    }

    if (exchangeRateRaw == '0') {
      console.log('Exchange rate is zero')
      return this.getDefaultGasPrice()
      // console.log('Exchange rate is zero. Continue with 1:1, instead')
      // exchangeRateRaw = '1e+18'
    }

    let txSize = new BN(0)
    try {
      txSize = new BN(await this.qfiParams.getUint('governed.EPQFI.normalizedTransactionSize'))
    } catch (e) {
      console.log('Cannot get normalized txSize')
      return this.getDefaultGasPrice()
    }

    const exchangeRate = new BN(exchangeRateRaw)
    const gp = txFee.mul(new BN(1e+18)).div(exchangeRate).div(txSize).toString()
    return Number(gp)
  }

  private async getDefaultGasPrice(): Promise<number> {
    return (await this.signerOrProvider.getGasPrice()).toNumber()
  }
}
