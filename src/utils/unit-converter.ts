/**
 * Allows to use handy unit conversions.
 * @module unit-converter
 */
import { BigNumber as BigNumberBase } from 'bignumber.js'

const BigNumber = BigNumberBase.clone()
BigNumber.config({ EXPONENTIAL_AT: 1e9 })
/**
 * Helps with converting numbers
 */
export class UnitConverter {
  private readonly SECONDS_IN_A_YEAR: number = 31536000 // 365*24*60*60
  private readonly FRACTION_PRECISION = 27

  /**
   * Returns the given input number as BigNumber object (as defined by bignumber.js library)
   * @memberof UnitConverter
   */
  public toBigNumber = (value: string|number): BigNumberBase => {
    return new BigNumber(value)
  }

  /**
   * Returns the given BigNumber object (as defined by bignumber.js library) to a string that can be passed to any system smart contract.
   * @memberof UnitConverter
   */
  public fromBigNumber = (value: BigNumberBase): string => {
    return value.toFixed()
  }

  /**
   * Converts date format to timestamp.
   * @param date Date object to convert to timestamp.
   * @returns Timestamp.
   */
  toTimestamp(date: Date): string {
    return Math.floor(date.getTime()/1000).toString()
  }

  /**
   * Converts timestamp to date format.
   * @param timestamp Timestamp to convert to date.
   * @returns Date object.
   */
  fromTimestamp(timestamp: string): Date {
    const timestampInMs = Number(timestamp) * 1000
    return new Date(timestampInMs)
  }

  /**
   * Convert APY to ratePerSecond format.
   * @param annualPercentageYield Specifies annual percentage yield NOT as onchain fraction, i.e. 5% = 0.05 or '0.05'.
   * @returns rate per second in onchain fraction precision as string, e.g. the output for toRatePerSecond(0.05) returns '1547125982881430000'.
   */
  toRatePerSecond(annualPercentageYield: number|string): string {
    const decimalsPart = this.toBigNumber(annualPercentageYield)
    const result = this.toBigNumber((decimalsPart.plus(1).toNumber()) ** (1 / this.SECONDS_IN_A_YEAR))
      .minus(1)

    return this.toFraction(result.toString())
  }

  /**
   * Convert ratePerSecond format to APY.
   * @param ratePerSecond Specifies the rate per second in onchain fraction precision as string
   * @returns Annual percentage yield NOT as onchain fraction
   */
  fromRatePerSecond(ratePerSecond: string): string {
    const decimalsPart = this.toBigNumber(this.fromFraction(ratePerSecond))
    const result = this.toBigNumber((decimalsPart.plus(1).toNumber()) ** this.SECONDS_IN_A_YEAR)
      .minus(1)

    return result.toFixed()
  }

  /**
   * Gets the integer representation of the given number with a specifed shift of decimal places.
   * Example: toSmallestUnit(0.054, 8) => '5400000'
   * @param decimal Decimal number to convert.
   * @param digitShift The number of decimal places to apply in the integer representation
   * @returns the given number - shifted by the given decimal places - as string
   */
  toSmallestUnit(decimal: number | string, digitShift: number): string {
    const factor = this.toBigNumber(10).pow(digitShift)
    return factor.times(decimal).toFixed()
  }

  /**
   * Converts the integer representation with a specifed shift of decimal places to a decimal number.
   * Example: fromSmallestUnit( '5400000', 8) => '0.054'
   * @param smallestUnitStr The number represented in smallest units.
   * @param digitShift The number of decimal places of the integer representation
   * @returns the smallest unit amount - shifted by the given decimal places - as string
   */
  fromSmallestUnit(smallestUnitStr: string, digitShift: number): string {
    const factor = this.toBigNumber(10).pow(digitShift)
    return this.toBigNumber(smallestUnitStr).div(factor).toFixed()
  }

  /**
   * Converts decimal to 10**27 precision format.
   * @param decimal Decimal to convert.
   * @param isPercentage If true, then the input is interpreted as percentage, i.e. 10.5% is passed as 10.5 instead of 0.105
   * @returns decimal in system precision format.
   */
  toFraction(decimal: number | string, isPercentage = false): string {
    let digitShift = this.FRACTION_PRECISION
    if (isPercentage) digitShift-=2
    return this.toSmallestUnit(decimal, digitShift)
  }

  /**
   * Converts 10**27 precision format to decimal.
   * @param fraction System precision format value.
   * @param asPercentage If true, then the output is represented as percentage, i.e. 10.5% is returned as 10.5 instead of 0.105
   * @returns Regular decimal.
   */
  fromFraction(fractionStr: string, asPercentage = false): string {
    let digitShift = this.FRACTION_PRECISION
    if (asPercentage) digitShift-=2
    return this.fromSmallestUnit(fractionStr, digitShift)
  }
}
