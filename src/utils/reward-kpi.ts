import { SignerOrProvider } from '../types'
import { Web3Adapter } from './web3-adapter'
import BigNumber from 'bignumber.js'
import { ContractRegistryInstance } from '../contracts/ContractRegistryInstance'
import { UnitConverter } from './unit-converter'

const SECONDS_PER_BLOCK = 5
const SECONDS_IN_YEAR = 365 * 24 * 60 * 60

export class RewardKPI {
  private _adapter: Web3Adapter
  private _registry: ContractRegistryInstance
  private _converter: UnitConverter
  private _rewardsPerSecond: BigNumber

  constructor(signerOrProvider: SignerOrProvider, registry: ContractRegistryInstance) {
    this._adapter = new Web3Adapter(signerOrProvider)
    this._registry = registry
    this._converter = new UnitConverter()
    this._rewardsPerSecond = this._converter.toBigNumber('0')
  }

  async getRootNodesAPR(): Promise<string> {
    const [rootNodes, constitution] = await Promise.all([
      this._registry.rootNodes(),
      this._registry.constitution()
    ])

    const [stakes, rewardsShare] = await Promise.all([
      rootNodes.getStakes(),
      constitution.getUint('constitution.rewardShareRootNodes')
    ])

    const totalStake = this._getTotalStake(stakes.map(r => this._adapter.fromWei(r.value)))
    const yearlyRewards = await this._getYearlyRewards(rewardsShare)

    return this._getAPR(totalStake, yearlyRewards)
  }

  async getValidatorsAPR(): Promise<string> {
    const [validators, constitution] = await Promise.all([
      this._registry.validators(),
      this._registry.constitution()
    ])

    const [shortList, rewardsShare] = await Promise.all([
      validators.getShortList(),
      constitution.getUint('constitution.rewardShareValidatorNodes')
    ])

    const yearlyRewards = await this._getYearlyRewards(rewardsShare)
    const totalStake = this._getTotalStake(shortList.map(v => v.balance))

    return this._getAPR(totalStake, yearlyRewards)
  }

  private async _getYearlyRewards(share: string): Promise<string> {
    if (this._rewardsPerSecond.isZero()) {
      // Optimize loading by getting proxy events only once
      await this._calculateRewardsPerSecond()
    }

    return this._rewardsPerSecond
      .multipliedBy(SECONDS_IN_YEAR)
      .multipliedBy(this._converter.fromFraction(share))
      .toString()
  }

  private async _calculateRewardsPerSecond(): Promise<void> {
    const [proxy, latestBlock] = await Promise.all([
      this._registry.defaultAllocationProxy(),
      this._adapter.getBlockNumber()
    ])

    const [proxyBalance, events] = await Promise.all([
      proxy.getBalance(),
      proxy.instance.queryFilter(
        proxy.instance.filters.Allocated(),
        latestBlock - 100_000,
        'latest',
      )
    ])

    const latestEventBlock = events[events.length - 1]?.blockNumber || 0
    const blocksDelta = latestBlock - latestEventBlock

    this._rewardsPerSecond = this._converter
      .toBigNumber(proxyBalance)
      .div(blocksDelta * SECONDS_PER_BLOCK)
  }

  private _getTotalStake(stakes: string[]): string {
    const totalStake = stakes.reduce((acc, item) => {
      return acc.plus(this._converter.toBigNumber(item))
    }, this._converter.toBigNumber('0'))

    return totalStake.toString()
  }

  private async _getAPR(totalStake: string, yearlyRewards: string): Promise<string> {
    return this._converter
      .toBigNumber(yearlyRewards)
      .div(totalStake)
      .multipliedBy(100)
      .toString()
  }
}
