import { BigNumber } from 'ethers'

import { DecodedData } from '../types'

import { getEthersInterface } from '../abi/AbiImporter'
import { utils } from 'ethers'
import { Fragment, JsonFragment } from '@ethersproject/abi'

type ABI = string | ReadonlyArray<Fragment | JsonFragment | string>


function hasNumericProperty(object: utils.Result): boolean {
  return Object.keys(object).some((key) => Number.isInteger(+key))
}

function isSolidityStruct(object: utils.Result): boolean {
  return !hasNumericProperty(object) && !(object instanceof BigNumber)
}

function isSolidityStructArray(object: utils.Result): boolean {
  return Array.isArray(object) && object.length > 0 && isSolidityStruct(object[0])
}

function cast(object: utils.Result): utils.Result {
  if (isSolidityStructArray(object)) {
    return object.map((element) => cast(element))
  }

  if (!isSolidityStruct(object)) {
    return object
  }

  return Object.keys(object).reduce((acc, currentKey) => {
    if (!Number.isInteger(+currentKey)) {
      acc[currentKey] = cast(object[currentKey])
    }

    return acc
  }, {}) as utils.Result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getEncodedData(abiName: string, name: string, ...args: any[]): string {
  const contractInterface = getEthersInterface(`${abiName}.json`)
  return contractInterface.encodeFunctionData(name, [...args])
}

export function getDecodedData(abiName: string, calldata: string): DecodedData {
  const contractInterface = getEthersInterface(`${abiName}.json`)
  const decodedData = contractInterface.parseTransaction({ data: calldata, value: 0 })

  return {
    functionName: decodedData.name,
    arguments: cast(decodedData.args),
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getEncodedDataByABI(abi: ABI, name: string, ...args: any[]): string {
  const contractInterface = new utils.Interface(abi)

  return contractInterface.encodeFunctionData(name, [...args])
}

export function getDecodedDataByABI(abi: ABI, calldata: string): DecodedData {
  const contractInterface = new utils.Interface(abi)
  const decodedData = contractInterface.parseTransaction({ data: calldata, value: 0 })

  return {
    functionName: decodedData.name,
    arguments: cast(decodedData.args),
  }
}
