import { AddressWithBalance } from '..'
import { version } from './version.json'

import { Signer, utils, providers, BigNumberish } from 'ethers'
import { SignerOrProvider } from '../types'
import { UnitConverter } from '../utils/unit-converter'

export interface ConnectionInfo {
  networkId: number;
  nodeInfo: string;
  rpcUrl: string;
}

const c = new UnitConverter()

export class Web3Adapter {
  public readonly provider: providers.Provider
  public readonly signer?: Signer

  constructor(public readonly signerOrProvider: SignerOrProvider) {
    this.provider = (signerOrProvider as Signer)?._isSigner
      ? (signerOrProvider as Signer).provider
      : signerOrProvider as providers.Provider
    this.signer = (signerOrProvider as Signer)?._isSigner
      ? signerOrProvider as Signer
      : undefined
  }

  public readonly ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
  public readonly ZERO_BYTES_32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
  public readonly SDK_VERSION: string = version

  toWei = utils.parseUnits
  fromWei(value: BigNumberish, unitName?: BigNumberish): string {
    return c.toBigNumber(utils.formatUnits(value, unitName)).toFixed()
  }

  async getDefaultAccount(): Promise<string> {
    return this.signer?.getAddress() || ''
  }

  async getBalance(address: string, convertToQ = true): Promise<string> {
    const balance = await this.provider.getBalance(address)
    if (!convertToQ) return balance.toString()
    return this.fromWei(balance)
  }

  async getBalances(addresses: string[], convertToQ = true): Promise<AddressWithBalance[]> {
    const getBalanceOperation = async (address: string): Promise<AddressWithBalance> => {
      const balance = await this.getBalance(address, convertToQ)
      return {
        address,
        balance,
      }
    }

    return this.forEachAddress(addresses, getBalanceOperation)
  }

  async forEachAddress<T>(addresses: string[], operation: (address: string, i?: number) => Promise<T>): Promise<T[]> {
    const values = []
    const errors = []

    const robustOperation = async (address: string, i: number): Promise<void> => {
      try {
        const value = await operation(address, i)
        values[i] = value
      } catch (error) {
        const reason = error.message ?? error.toString()
        errors.push({ address, reason })
      }
    }

    const promises = addresses.map(robustOperation)
    await Promise.all(promises)

    if (errors.length > 0) {
      const msg = errors.map(e => `operation rejected for address ${e.address}: ${e.reason}`).join('\n')
      throw new Error(msg)
    }

    return values
  }

  async getBlockNumber(): Promise<number> {
    return this.provider.getBlockNumber()
  }
}