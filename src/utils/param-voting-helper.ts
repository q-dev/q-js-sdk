import { Parameter, ParameterType, RawParameter, RawParameterAsArray } from '../types'

/**
 * Helps work with Parameters voting instances
 */
export class ParamVotingHelper {
  mapParameterToOnChainJson(parameter: Parameter): RawParameterAsArray & RawParameter {
    const value = parameter.paramValue

    const rawParameter: RawParameter = {
      paramKey: parameter.paramKey,
      paramType: parameter.paramType,
      addrValue: '0x0000000000000000000000000000000000000000',
      boolValue: false,
      bytes32Value: '0x0000000000000000000000000000000000000000000000000000000000000000',
      strValue: '',
      uintValue: '0',
    }

    switch (parameter.paramType) {
      case ParameterType.ADDRESS:
        rawParameter.addrValue = value
        break
      case ParameterType.BOOL:
        rawParameter.boolValue= (value.toLowerCase() === 'true')
        break
      case ParameterType.BYTE:
        rawParameter.bytes32Value = value
        break
      case ParameterType.STRING:
        rawParameter.strValue = value
        break
      case ParameterType.UINT:
        rawParameter.uintValue = value
        break
    }

    const parameterAsArray: RawParameterAsArray = [rawParameter.paramKey, rawParameter.paramType, rawParameter.addrValue, rawParameter.boolValue, rawParameter.bytes32Value, rawParameter.strValue, rawParameter.uintValue]
    const mappedParameter = Object.assign(parameterAsArray, rawParameter)
    return mappedParameter
  }

  mapOnChainJsonToParameter(rawParameter: RawParameter): Parameter {
    let paramValue: string
    switch (rawParameter.paramType) {
      case ParameterType.ADDRESS:
        paramValue = rawParameter.addrValue
        break
      case ParameterType.BOOL:
        paramValue = rawParameter.boolValue.toString()
        break
      case ParameterType.BYTE:
        paramValue = rawParameter.bytes32Value
        break
      case ParameterType.STRING:
        paramValue = rawParameter.strValue
        break
      case ParameterType.UINT:
        paramValue = rawParameter.uintValue
        break
    }

    return {
      paramKey: rawParameter.paramKey,
      paramType: rawParameter.paramType,
      paramValue
    }
  }
}
