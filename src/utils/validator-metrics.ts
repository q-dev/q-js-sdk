import { AddressWithBalance, UnitConverter, ValidatorPoolInfo } from '..'
import { ContractRegistryInstance } from '../contracts/ContractRegistryInstance'
import { ValidationRewardPoolsInstance } from '../contracts/tokeneconomics/ValidationRewardPoolsInstance'
import { ValidatorsInstance } from '../contracts/governance/validators/ValidatorsInstance'
import { EPQFIParametersInstance } from '../contracts/governance/experts/EPQFIParametersInstance'
import { SystemContractInstance } from '../contracts/SystemContractInstance'
import { AddressStorageStakes } from '../ethers-contracts'
import { utils } from 'ethers'

const c = new UnitConverter()
const DUSTED_DELEGATED_STAKE = '100'
/**
 * Helps calculate delegation efficiency and saturation
 */
export class ValidatorMetrics {
  private validatorsInfo: StakeInfo[]
  private poolsInfo: ValidatorPoolInfo[]
  private validators: AddressWithBalance[]
  private stakeDelegationFactor: string

  /**
   * @internal
   * Takes snapshot using given data.
   * Used by unit tests.
   * @param validatorsInfo ValidatorInfo struct array
   * @param poolsInfo ValidatorPoolInfo struct array
   * @param stakeDelegationFactor current stake delegation factor
   */
  setSnapShot(
    validatorsInfo: StakeInfo[],
    poolsInfo: ValidatorPoolInfo[],
    stakeDelegationFactor: string
  ): void {
    if (validatorsInfo.length !== poolsInfo.length) {
      throw new RangeError(
        'validatorsInfo length does not equal to poolsInfo length'
      )
    }
    this.validatorsInfo = validatorsInfo
    this.poolsInfo = poolsInfo
    this.stakeDelegationFactor = stakeDelegationFactor
  }

  /**
   * Takes snapshot using registry
   * @param registry Contract registry instance
   */
  async takeSnapshotFromNetwork(
    registry: ContractRegistryInstance
  ): Promise<void> {
    await this.takeSnapshot(
      await registry.validators(),
      await registry.validationRewardPools(),
      await registry.epqfiParameters()
    )
  }

  /**
   * @internal
   * Takes snapshot using Validators, ValidationRewardPools and EPQFIParameters instances.
   * Used by takeSnapshotFromNetwork method
   * @param validatorsInstance ValidatorsInstance
   * @param poolsInstance ValidationRewardPoolsInstance
   * @param parameters EPQFIParametersInstance
   */
  async takeSnapshot(
    validatorsInstance: ValidatorsInstance,
    poolsInstance: ValidationRewardPoolsInstance,
    parameters: EPQFIParametersInstance
  ): Promise<void> {
    const longListAddress = await validatorsInstance.instance.longList()
    const { instance: stakesInstance } =
      new SystemContractInstance<AddressStorageStakes>(
        validatorsInstance.instance.provider,
        'AddressStorageStakes.json',
        longListAddress
      )

    this.validators = await validatorsInstance.getShortList()
    const stakeDelegationFactorKey = 'governed.EPQFI.stakeDelegationFactor'

    const stakePromises = Promise.all(
      this.validators.map((v) =>
        this.getStakeInfo(validatorsInstance, stakesInstance, v.address)
      )
    )
    const poolPromises = Promise.all(
      this.validators.map((v) => poolsInstance.getPoolInfo(v.address))
    )

    const [infos, pools, stakeDelegationFactor] = await Promise.all([
      stakePromises,
      poolPromises,
      parameters.getUint(stakeDelegationFactorKey),
    ])

    this.setSnapShot(infos, pools, stakeDelegationFactor)
  }

  /**
   * Returns stored snapshot
   * @returns ValidatorSnapshot struct array
   */
  getSnapshot(): ValidatorSnapshot[] {
    const validatorSnapshots = this.validatorsInfo.map((v, i) => {
      return {
        address: v.address,
        accountableStake: v.accountableStake,
        delegatedStake: v.delegatedStake,
        delegatorsShare: this.poolsInfo[i].delegatorsShare,
        selfStake: v.selfStake,
        totalStake: v.totalStake,
        stakeDelegationFactor: this.stakeDelegationFactor,
      }
    })
    return validatorSnapshots
  }

  /**
   * Calculates delegation efficiency using snapshot
   * @returns Delegation efficiency
   */
  getDelegationEfficiency(): DelegationEfficiency[] {
    const globalAccountableStake = this.validatorsInfo?.reduce(
      (acc, v) => acc.plus(v.accountableStake),
      c.toBigNumber(0)
    )
    const delegators = this.validatorsInfo.map((v, i) => {
      const poolInfo = this.poolsInfo[i]
      const globalStakeShare = globalAccountableStake.isGreaterThan(0)
        ? c.toBigNumber(v.accountableStake).div(globalAccountableStake)
        : c.toBigNumber(0)
      const payoutToDelegator = globalStakeShare.multipliedBy(
        poolInfo.delegatorsShare
      )
      const delegatedStake = c.toBigNumber(v.delegatedStake)
      const payoutPerDelegatedQ = delegatedStake.isGreaterThan(DUSTED_DELEGATED_STAKE)
        ? payoutToDelegator.div(delegatedStake).toString()
        : '0'

      return {
        address: v.address,
        payoutPerDelegatedQ,
        globalStakeShare: globalStakeShare.toString(),
        delegatorShare: poolInfo.delegatorsShare.toString(),
        payoutToDelegators: payoutToDelegator.toString(),
      }
    })

    const totalPayoutPerDelegatedQ = delegators.reduce(
      (acc, v) => acc.plus(v.payoutPerDelegatedQ),
      c.toBigNumber(0)
    )
    const delegationEfficiency = delegators.map((v) => {
      const delegationEfficiency = globalAccountableStake.isGreaterThan(0)
        ? c
          .toBigNumber(v.payoutPerDelegatedQ)
          .multipliedBy(100)
          .div(totalPayoutPerDelegatedQ)
          .toString()
        : '0'

      return {
        ...v,
        delegationEfficiency,
      }
    })

    return delegationEfficiency
  }

  /**
   * Calculates delegation saturation using snapshot
   * @returns DelegationSaturation
   */
  getDelegationSaturation(): string[] {
    const stakeDelegationFactor = c.fromSmallestUnit(
      this.stakeDelegationFactor,
      29
    )
    return this.validatorsInfo.map((v) => {
      const saturation = c
        .toBigNumber(v.currentDelegationFactor)
        .div(stakeDelegationFactor)

      return saturation.toString()
    })
  }

  /**
   * Return short validator list using snapshot
   * @returns AddressWithBalance[]
   */
  getValidatorsShortList(): AddressWithBalance[] {
    return this.validators
  }

  /**
   * Calculates stake information using snapshot
   * @returns StakeInfo
   */
  async getStakeInfo(
    validatorsInstance: ValidatorsInstance,
    stakesInstance: AddressStorageStakes,
    address: string
  ): Promise<StakeInfo> {
    const [{ stake, delegatedStake }, withdrawalInfo] = await Promise.all([
      stakesInstance.addrStake(address),
      validatorsInstance.getWithdrawalInfo(address),
    ])

    const selfStake = utils.formatUnits(stake)
    const totalStake = utils.formatUnits(
      stake.add(delegatedStake).sub(withdrawalInfo.amount)
    )
    const accountableStake =
      this.validators.find((v) => v.address === address)?.balance || '0'

    return {
      address,
      selfStake,
      delegatedStake: utils.formatUnits(delegatedStake),
      totalStake,
      accountableStake: accountableStake.toString(),
      currentDelegationFactor: c
        .toBigNumber(totalStake)
        .div(selfStake)
        .toNumber(),
    }
  }
}

/**
 * Stake info
 */
export interface StakeInfo {
  /**
   * @field Validator address
   */
  address: string;
  /**
   * @field Validator self stake from {@link StakeInfo.selfStake}
   */
  selfStake: string;
  /**
   * @field Validator delegation stake from {@link StakeInfo.delegatedStake}
   */
  delegatedStake: string;
  /**
   * @field Validator self stake from {@link StakeInfo.totalStake}
   */
  totalStake: string;
  /**
   * @field Validator account stake from {@link StakeInfo.accountableStake}
   */
  accountableStake: string;
  /**
   * @field Validator delegation factor
   */
  currentDelegationFactor: number;
}

/**
 * Delegation efficiency info
 */
export interface DelegationEfficiency {
  /**
   * @field Validator address
   */
  address: string;
  /**
   * @field Validator {@link ValidatorInfo.accountableStake | accountable stake} / (sum of all accountable stakes)
   */
  globalStakeShare: string;
  /**
   * @field Validator delegation share from {@link ValidatorPoolInfo.delegatorsShare}
   */
  delegatorShare: string;
  /**
   * @field {@link DelegationEfficiency.globalStakeShare | globalStakeShare} * delegatorsShare
   */
  payoutToDelegators: string;
  /**
   * @field {@link DelegationEfficiency.payoutToDelegators | payoutToDelegator} / delegatedStake
   */
  payoutPerDelegatedQ: string;
  /**
   * @field Validator delegation efficiency
   */
  delegationEfficiency: string;
}

/**
 * Validator snapshot info
 */
export interface ValidatorSnapshot {
  /**
   * @field Validator address
   */
  address: string;
  /**
   * @field Validator accountable stake from {@link StakeInfo.accountableStake}
   */
  accountableStake: string;
  /**
   * @field Validator delegation stake from {@link StakeInfo.delegatedStake}
   */
  delegatedStake: string;
  /**
   * @field Validator delegation share from {@link ValidatorPoolInfo.delegatorsShare}
   */
  delegatorsShare: string;
  /**
   * @field Validator self stake from {@link StakeInfo.selfStake}
   */
  selfStake: string;
  /**
   * @field Validator self stake from {@link StakeInfo.totalStake}
   */
  totalStake: string;
  /**
   * @field System stake delegation factor
   */
  stakeDelegationFactor: string;
}
