import { getAbi } from '../abi/AbiImporter'
import { ProposalWithBaseInfo, SignerOrProvider } from '../types'
import { IVoting } from '../ethers-contracts/IVoting'
import { BaseVotingInstance } from './BaseVotingInstance'

export abstract class SystemVotingInstance<T extends IVoting, P extends ProposalWithBaseInfo> extends BaseVotingInstance<T, P> {
  /**
   * Constructor
   * @param provider ethers provider instance
   * @param abiFile abi json file
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abiFile: string, public readonly address: string) {
    const abi = getAbi(abiFile)
    super(signerOrProvider, abi, address)
  }
}