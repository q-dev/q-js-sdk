import type { ContractTransaction, BigNumberish } from 'ethers'

import { QNonPayableTx, TimeLockEntry, QPayableTx } from '..'
import { ATimeLockBase } from '../ethers-contracts/ATimeLockBase'
import { SystemContractInstance } from './SystemContractInstance'

/**
 * Abstract Timelock instance to interact with QVault, Saving, RootNodes and Validators contracts.
 * See [onchain documentation](@system-contracts-repo/@network/ATimeLockBase/) for more details.
 */
export class TimeLockHelperInstance<T extends ATimeLockBase> extends SystemContractInstance<T> {
  /**
   * [External documentation](@system-contracts-repo/@network/ATimeLockBase/#depositonbehalfof)
   */
  async depositOnBehalfOf(account: string, start: BigNumberish, end: BigNumberish,
    txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'depositOnBehalfOf', ATimeLockBase>('depositOnBehalfOf', [account, start, end], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ATimeLockBase/#gettimelocks)
   */
  async getTimeLocks(account: string): Promise<TimeLockEntry[]> {
    const timelocks = await this.instance.getTimeLocks(account)
    return timelocks.map(i => ({
      amount: i.amount.toString(),
      releaseStart: i.releaseStart.toString(),
      releaseEnd: i.releaseEnd.toString()
    }))
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ATimeLockBase/#getminimumbalance)
   */
  async getMinimumBalance(account: string, timestamp: BigNumberish): Promise<string> {
    return (await this.instance.getMinimumBalance(account, timestamp)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ATimeLockBase/#purgetimelocks)
   */
  async purgeTimeLocks(account: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'purgeTimeLocks', ATimeLockBase>('purgeTimeLocks', [account], txOptions)
  }
}
