import { ProposalWithBaseInfo } from '../types'
import { ISlashingVoting } from '../ethers-contracts/ISlashingVoting'
import { SystemContractInstance } from './SystemContractInstance'

export interface SlashingProposal extends ProposalWithBaseInfo{
  proposer: string;
  candidate: string;
  amountToSlash: string;
}

/**
 * Slashing interface to interact with Slashing implementation contracts.
 * See [onchain documentation](@system-contracts-repo/@network/ISlashingVoting/) for more details.
 */
export class SlashingVotingHelper<T extends ISlashingVoting> extends SystemContractInstance<T> {
  async getSlashingVictim(proposalId: string | number): Promise<string> {
    return this.instance.getSlashingVictim(proposalId)
  }

  async getSlashingProposer(proposalId: string | number): Promise<string> {
    return this.instance.getSlashingProposer(proposalId)
  }
}
