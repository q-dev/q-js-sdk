import type { ContractTransaction, BigNumberish } from 'ethers'

import { ContractRegistryAddressVoting } from '../../ethers-contracts/ContractRegistryAddressVoting'
import { SetKeyProposal, QNonPayableTx, SignerOrProvider } from '../../types'
import { SystemRootNodeApprovalVoting } from '../SystemRootNodeApprovalVoting'

export class ContractRegistryAddressVotingInstance extends SystemRootNodeApprovalVoting<ContractRegistryAddressVoting> {
  public static readonly registryKey = 'governance.address.contractRegistryVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'ContractRegistryAddressVoting.json', address)
  }

  async createProposal(key: string, proxy: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [key, proxy], txOptions)
  }

  async getProposal(id: BigNumberish): Promise<SetKeyProposal> {
    const proposal = await this.instance.getProposal(id)
    return {
      id: id.toString(),
      status: await this.getStatus(id),
      executed: proposal.executed,
      votingStartTime: proposal.votingStartTime.toString(),
      votingExpiredTime: proposal.votingExpiredTime.toString(),
      key: proposal.key,
      proxy: proposal.proxy
    }
  }
}
