import type { ContractTransaction, BigNumberish } from 'ethers'

import { ASlashingEscrow } from '../../ethers-contracts/ASlashingEscrow'
import { BaseContractInstance } from '../BaseContractInstance'
import { QNonPayableTx, ArbitrationInfo } from '../../types'

/**
 * Abstract Slashing Escrow instance to interact with Validator and RootNodes slashing escrow contract.
 * See [onchain documentation](@system-contracts-repo/@network/ASlashingEscrow/) for more details.
 */
export class BaseSlashingEscrowInstance<T extends ASlashingEscrow> extends BaseContractInstance<T> {
  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#confirmdecision)
   */
  async confirmDecision(id: BigNumberish, hash: string | number[], txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'confirmDecision', ASlashingEscrow>('confirmDecision', [id, hash], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#arbitrationinfos)
   */
  async arbitrationInfos(id: BigNumberish): Promise<ArbitrationInfo> {
    const info = await this.instance.arbitrationInfos(id)
    return {
      remark: info.remark,
      proposerRemark: info.proposerRemark,
      params: {
        slashedAmount: info.params.slashedAmount.toString(),
        objectionEndTime: info.params.objectionEndTime.toString(),
        appealEndTime: info.params.appealEndTime.toString(),
      },
      decision: {
        notAppealed: info.decision.notAppealed,
        proposer: info.decision.proposer,
        confirmers: info.decision.confirmers,
        externalReference: info.decision.externalReference,
        percentage: info.decision.percentage.toString(),
        confirmationCount: info.decision.confirmationCount.toString(),
        endDate: info.decision.endDate.toString(),
        hash: info.decision.hash,
      },
      executed: info.executed,
      appealConfirmed: info.appealConfirmed,
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#castobjection)
   */
  async castObjection(id: BigNumberish, remark: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'castObjection', ASlashingEscrow>('castObjection', [id, remark], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#setremark)
   */
  async setRemark(id: BigNumberish, remark: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'setRemark', ASlashingEscrow>('setRemark', [id, remark], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#setproposerremark)
   */
  async setProposerRemark(
    id: BigNumberish,
    proposerRemark: string,
    appealConfirmed: boolean,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction<'setProposerRemark', ASlashingEscrow>('setProposerRemark', [id, proposerRemark, appealConfirmed], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#execute)
   */
  async execute(id: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'execute', ASlashingEscrow>('execute', [id], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#proposedecision)
   */
  async proposeDecision(
    id: BigNumberish,
    percentage: BigNumberish,
    notAppealed: boolean,
    reference: string,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction<'proposeDecision', ASlashingEscrow>(
      'proposeDecision',
      [id, percentage, notAppealed, reference],
      txOptions
    )
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ASlashingEscrow/#recallproposeddecision)
   */
  async recallProposedDecision(id: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'recallProposedDecision', ASlashingEscrow>(
      'recallProposedDecision',
      [id],
      txOptions
    )
  }
}

