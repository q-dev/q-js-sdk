import type { ContractTransaction } from 'ethers'

import { ValidatorsSlashingVoting } from '../../../ethers-contracts/ValidatorsSlashingVoting'
import { SlashingProposal, SlashingVotingHelper } from '../../SlashingVotingHelperInstance'
import { QNonPayableTx, SignerOrProvider } from '../../../types'
import { SystemVotingInstance } from '../../SystemVotingInstance'

/**
 * Validator Slashing Voting instance to interact with Validator Slashing Voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.validatorsSlashingVoting}
 */
export class ValidatorsSlashingVotingInstance extends SystemVotingInstance<ValidatorsSlashingVoting, SlashingProposal> {
  public static readonly registryKey = 'governance.validators.slashingVoting'
  public static readonly abi = 'ValidatorsSlashingVoting.json'

  private slashingVoting: SlashingVotingHelper<ValidatorsSlashingVoting>

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, ValidatorsSlashingVotingInstance.abi, address)
    this.slashingVoting = new SlashingVotingHelper<ValidatorsSlashingVoting>(signerOrProvider, ValidatorsSlashingVotingInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<SlashingProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.base.remark,
        executed: proposal.base.executed,
        params: {
          votingStartTime: proposal.base.params.votingStartTime.toString(),
          votingEndTime: proposal.base.params.votingEndTime.toString(),
          vetoEndTime: proposal.base.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.base.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.base.params.requiredQuorum.toString(),
          requiredMajority: proposal.base.params.requiredMajority.toString(),
          requiredSMajority: proposal.base.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.base.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.base.counters.weightFor.toString(),
          weightAgainst: proposal.base.counters.weightAgainst.toString(),
          vetosCount: proposal.base.counters.vetosCount.toString(),
        },
      },
      proposer: proposal.proposer,
      candidate: proposal.candidate,
      amountToSlash: proposal.amountToSlash.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#voted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.voted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#proposalcount)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#createproposal)
   */
  async createProposal(remark: string, candidate: string, percentage: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [remark, candidate, percentage], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#slashingaffectswithdrawal)
   */
  async slashingAffectsWithdrawal(proposalId: string | number): Promise<boolean> {
    return this.instance.slashingAffectsWithdrawal(proposalId)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#getproposalstarttime)
   */
  async getProposalStartTime(proposalId: string | number): Promise<string> {
    return (await this.instance.getProposalStartTime(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#getslashingvictim)
   */
  async getSlashingVictim(proposalId: string | number): Promise<string> {
    return this.slashingVoting.getSlashingVictim(proposalId)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidatorsSlashingVoting/#getslashingproposer)
   */
  async getSlashingProposer(proposalId: string | number): Promise<string> {
    return this.slashingVoting.getSlashingProposer(proposalId)
  }
}
