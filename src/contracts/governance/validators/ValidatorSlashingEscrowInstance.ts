import { SignerOrProvider } from '../../../types'
import { ValidatorSlashingEscrow } from '../../../ethers-contracts/ValidatorSlashingEscrow'
import { SystemSlashingEscrowInstance } from '../../SystemSlashingEscrowInstance'

/**
 * Validator Slashing Escrow instance to interact with Validator Slashing Escrow contract.
 * See [onchain documentation](@system-contracts-repo/@network/ValidatorSlashingEscrow/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.validatorSlashingEscrow}
 */
export class ValidatorSlashingEscrowInstance extends SystemSlashingEscrowInstance<ValidatorSlashingEscrow> {
  public static readonly abi = 'ValidatorSlashingEscrow.json'
  public static readonly registryKey = 'governance.validators.slashingEscrow'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, ValidatorSlashingEscrowInstance.abi, address)
  }
}
