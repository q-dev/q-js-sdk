import type { ContractTransaction, BigNumberish } from 'ethers'

import {
  AddressWithBalance,
  ValidatorInfo,
  ValidatorsWithdrawalInfo,
  QNonPayableTx,
  QPayableTx,
  VotingLockInfo,
  SignerOrProvider,
} from '../../../types'
import { Validators } from '../../../ethers-contracts/Validators'
import { AddressStorageStakes } from '../../../ethers-contracts/AddressStorageStakes'
import { SystemContractInstance } from '../../SystemContractInstance'
import { TimeLockHelperInstance } from '../../TimeLockHelperInstance'
import { VotingWeightProxyInstance } from '../VotingWeightProxyInstance'

/**
 * Validators instance to interact with Validators contract.
 * See [onchain documentation](@system-contracts-repo/@network/Validators/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.validators}
 */
export class ValidatorsInstance extends TimeLockHelperInstance<Validators> {
  public static readonly registryKey = 'governance.validators'
  public static readonly abi = 'Validators.json'

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, ValidatorsInstance.abi, address)
  }

  /**
   * @deprecated Use commitStake, instead
   */
  async commitCollateral(txOptions?: QPayableTx): Promise<ContractTransaction> {
    console.warn('ValidatorsInstance.commitCollateral is deprecated. Use commitStake, instead.')
    return this.submitTransaction('commitStake', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#commitstake)
   */
  async commitStake(txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('commitStake', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#announcewithdrawal)
   */
  async announceWithdrawal(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('announceWithdrawal', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#withdraw)
   */
  async withdraw(amount: string, payTo: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [amount, payTo], txOptions)
  }

  /**
   * Validators list.
   * @returns validator addresses.
   */
  async getLongList(): Promise<string[]> {
    const longlistAddress = await this.instance.longList()
    const stakes = new SystemContractInstance<AddressStorageStakes>(
      this.adapter.signerOrProvider, 'AddressStorageStakes.json', longlistAddress)
    return stakes.instance.getAddresses()
  }

  /**
   * Validators list of addresses and balances.
   * @returns validator addresses and balances.
   */
  async getLongListWithBalance(): Promise<AddressWithBalance[]> {
    const longList = await this.getLongList()
    const longListWithBalances = longList.map(async e => {
      return {
        address: e,
        balance: this.adapter.fromWei(
          await this.instance.getAccountableTotalStake(e)
        )
      }
    })
    return Promise.all(longListWithBalances)
  }

  /**
   * Validators recent list of addresses and balances.
   * @returns recent used validator addresses and balances.
   */
  async getShortList(): Promise<AddressWithBalance[]> {
    const shortlist = await this.instance.getValidatorShortList()
    return shortlist.map(e => ({
      address: e.validator,
      balance: this.adapter.fromWei(e.amount)
    }))
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getshortlistmaxlength)
   */
  async getShortListMaxLength(): Promise<string> {
    return (await this.instance.getShortListMaxLength()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#entershortlist)
   */
  async enterShortList(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('enterShortList', [], txOptions)
  }

  /** @deprecated The interest rate has to be set on the ValidationRewardPoolsInstance */
  async setInterestRate(_qiv: string, _txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    throw new Error('The interest rate has to be set on the ValidationRewardPoolsInstance')
  }

  /** @deprecated The delegators share has to be set on the ValidationRewardPoolsInstance */
  async setDelegatorsShare(_qsv: string, _txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    throw new Error('The delegators share has to be set on the ValidationRewardPoolsInstance')
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#isinshortlist)
   */
  async isInShortList(user: string): Promise<boolean> {
    return this.instance.isInShortList(user)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#isinlonglist)
   */
  async isInLongList(user: string): Promise<boolean> {
    return this.instance.isInLongList(user)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getslashingproposalids)
   */
  async getSlashingProposalIds(validator: string): Promise<string[]> {
    return (await this.instance.getSlashingProposalIds(validator))
      .map(i => i.toString())
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getlockinfo)
   */
  async getLockInfo(who: string): Promise<VotingLockInfo> {
    return this.votingWeightProxy.getLockInfo(this.address, who)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#makesnapshot)
   */
  async makeSnapshot(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('makeSnapshot', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#applyslashing)
   */
  async applySlashing(
    proposalId: BigNumberish,
    validator: string,
    amountToSlash: BigNumberish,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction('applySlashing', [proposalId, validator, amountToSlash], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#addslashingproposal)
   */
  async addSlashingProposal(
    validator: string,
    slashingProposalId: BigNumberish,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction('addSlashingProposal', [validator, slashingProposalId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#calculatependingslashingsamount)
   */
  async calculatePendingSlashingsAmount(validator: string, endTime: BigNumberish): Promise<string> {
    return (await this.instance.callStatic.calculatePendingSlashingsAmount(validator, endTime)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getvalidatorslist)
   */
  async getValidatorsList(): Promise<string[]> {
    return this.instance.getValidatorsList()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#purgependingslashings)
   */
  async purgePendingSlashings(validator: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('purgePendingSlashings', [validator], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#refreshdelegatedstake)
   */
  async refreshDelegatedStake(delegatedTo: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('refreshDelegatedStake', [delegatedTo], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#validatorsinfos)
   */
  async getWithdrawalInfo(address: string): Promise<ValidatorsWithdrawalInfo> {
    const withdrawalInfo = await this.instance.validatorsInfos(address)
    return {
      endTime: withdrawalInfo.endTime.toString(),
      amount: withdrawalInfo.amount.toString(),
    }
  }

  /** @deprecated Use getAccountableSelfStake , instead*/
  async getValidatorsOwnStake(address: string): Promise<string> {
    return (await this.instance.getAccountableSelfStake(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getaccountableselfstake)
   */
  async getAccountableSelfStake(address: string): Promise<string> {
    return (await this.instance.getAccountableSelfStake(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getaccountabletotalstake)
   */
  async getAccountableTotalStake(address: string): Promise<string> {
    return (await this.instance.getAccountableTotalStake(address)).toString()
  }

  /** @deprecated Use getSelfStake , instead*/
  async getValidatorsOwnStakeWithoutPendingWithdrawal(address: string): Promise<string> {
    return (await this.instance.getSelfStake(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getselfstake)
   */
  async getSelfStake(address: string): Promise<string> {
    return (await this.instance.getSelfStake(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Validators/#getvalidatortotalstake)
   */
  async getValidatorTotalStake(address: string): Promise<string> {
    return (await this.instance.getValidatorTotalStake(address)).toString()
  }

  /** @deprecated has to be called via ValidationRewardPoolsInstance */
  async getInterestRate(_address: string): Promise<string> {
    throw new Error('interest rate has to be called via ValidationRewardPoolsInstance')
  }

  /** @deprecated has to be called via ValidationRewardPoolsInstance */
  async getDelegatorsShare(_address: string): Promise<string> {
    throw new Error('delegators share has to be called via ValidationRewardPoolsInstance')
  }

  /**
   * Retrieve information about Validators'stake
   * @param address Validator
   * @returns General information about Validator's stakes {@link ValidatorInfo}
   */
  async getValidatorInfo(address: string): Promise<ValidatorInfo> {
    const [
      selfStakeBN,
      withdrawalInfo,
      delegatedStakeBN,
      totalStakeBN,
      accountableStakeBN,
    ] = await Promise.all([
      this.instance.getAccountableSelfStake(address),
      this.getWithdrawalInfo(address),
      this.instance.getValidatorDelegatedStake(address),
      this.instance.getValidatorTotalStake(address),
      this.instance.getAccountableTotalStake(address)
    ])

    const selfStake = this.adapter.fromWei(selfStakeBN)
    const totalStake = this.adapter.fromWei(totalStakeBN)

    return {
      address,
      selfStake,
      pendingStake: this.adapter.fromWei(withdrawalInfo.amount),
      delegatedStake: this.adapter.fromWei(delegatedStakeBN),
      totalStake,
      currentDelegationFactor: Number.parseFloat(totalStake) / Number.parseFloat(selfStake),
      accountableStake: this.adapter.fromWei(accountableStakeBN),
    }
  }
}
