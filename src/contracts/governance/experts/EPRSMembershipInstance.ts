import { SignerOrProvider } from '../../../types'
import { EPRS_Membership } from '../../../ethers-contracts/EPRS_Membership'
import { ExpertsMembershipInstance } from './ExpertsMembershipInstance'

/**
 * EPRS membership instance to interact with EPRS membership contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPRS_Membership/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.EPRSMembership}
 */
export class EPRSMembershipInstance extends ExpertsMembershipInstance<EPRS_Membership> {
  public static readonly registryKey = 'governance.experts.EPRS.membership'
  public static readonly abi = 'EPRS_Membership.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, EPRSMembershipInstance.abi, address)
  }
}
