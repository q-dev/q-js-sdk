import { SignerOrProvider } from '../../../types'
import { EPQFI_ParametersVoting } from '../../../ethers-contracts/EPQFI_ParametersVoting'
import { ExpertsParametersVotingInstance } from '../experts/ExpertsParametersVotingInstance'

/**
 * EPQFI parameters voting instance to interact with EPQFI parameters voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPQFI_ParametersVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epqfiParametersVoting}
 */
export class EPQFIParametersVotingInstance extends ExpertsParametersVotingInstance<EPQFI_ParametersVoting> {
  public static readonly registryKey = 'governance.experts.EPQFI.parametersVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EPQFI_ParametersVoting.json', address)
  }
}
