import { SignerOrProvider } from '../../../types'
import { EPDR_Membership } from '../../../ethers-contracts/EPDR_Membership'
import { ExpertsMembershipInstance } from './ExpertsMembershipInstance'

/**
 * EPDR membership instance to interact with EPDR membership contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPDR_Membership/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epdrMembership}
 */
export class EPDRMembershipInstance extends ExpertsMembershipInstance<EPDR_Membership> {
  public static readonly registryKey = 'governance.experts.EPDR.membership'
  public static readonly abi = 'EPDR_Membership.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, EPDRMembershipInstance.abi, address)
  }
}
