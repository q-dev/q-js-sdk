import { SignerOrProvider } from '../../../types'
import { EPQFI_Parameters } from '../../../ethers-contracts/EPQFI_Parameters'
import { SystemParametersInstance } from '../../SystemParametersInstance'

/**
 * EPQFI parameters instance to interact with EPQFI parameters contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPQFI_Parameters/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epqfiParameters}
 */
export class EPQFIParametersInstance extends SystemParametersInstance<EPQFI_Parameters> {
  public static readonly registryKey = 'governance.experts.EPQFI.parameters'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EPQFI_Parameters.json', address)
  }
}
