import { SignerOrProvider } from '../../../types'
import { EPDR_MembershipVoting } from '../../../ethers-contracts/EPDR_MembershipVoting'
import { ExpertsMembershipVotingInstance } from './ExpertsMembershipVotingInstance'

/**
 * EPDR membership voting instance to interact with EPDR membership voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPDR_MembershipVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epdrMembershipVoting}
 */
export class EPDRMembershipVotingInstance extends ExpertsMembershipVotingInstance<EPDR_MembershipVoting> {
  public static readonly registryKey = 'governance.experts.EPDR.membershipVoting'
  public static readonly abi = 'EPDR_MembershipVoting.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, EPDRMembershipVotingInstance.abi, address)
  }
}
