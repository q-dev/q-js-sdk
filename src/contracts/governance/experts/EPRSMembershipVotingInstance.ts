import { SignerOrProvider } from '../../../types'
import { EPRS_MembershipVoting } from '../../../ethers-contracts/EPRS_MembershipVoting'
import { ExpertsMembershipVotingInstance } from './ExpertsMembershipVotingInstance'

/**
 * EPRS membership voting instance to interact with EPRS membership voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPRS_MembershipVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.EPRSMembershipVoting}
 */
export class EPRSMembershipVotingInstance extends ExpertsMembershipVotingInstance<EPRS_MembershipVoting> {
  public static readonly registryKey = 'governance.experts.EPRS.membershipVoting'
  public static readonly abi = 'EPRS_MembershipVoting.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, EPRSMembershipVotingInstance.abi, address)
  }
}
