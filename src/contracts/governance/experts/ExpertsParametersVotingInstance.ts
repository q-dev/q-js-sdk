import type { ContractTransaction } from 'ethers'

import { ExpertsParametersVoting } from '../../../ethers-contracts/ExpertsParametersVoting'
import {
  QNonPayableTx,
  Parameter,
  ProposalWithBaseInfo,
  RawParameter,
  ParameterType,
} from '../../../types'
import { SystemVotingVetoableInstance } from '../../SystemVotingVetoableInstance'
import { ParamVotingHelper } from '../../../utils/param-voting-helper'

export interface ExpertParameterProposal extends ProposalWithBaseInfo {
  parametersSize: string
}

/**
 * Expert parameters voting contract instance to interact with EPDR and EPQFI parameters voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/ExpertsParametersVoting/) for more details.
 */
export class ExpertsParametersVotingInstance<T extends ExpertsParametersVoting> extends SystemVotingVetoableInstance<T, ExpertParameterProposal> {
  private helper = new ParamVotingHelper()

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsParametersVoting/#hasrootvetoed)
   */
  async hasRootVetoed(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasRootVetoed(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsParametersVoting/#hasuservoted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasUserVoted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsParametersVoting/#proposalcount)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsParametersVoting/#createproposal)
   */
  async createProposal(remark: string, parameters: Parameter[], txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    const mappedParameters = parameters.map(p => this.helper.mapParameterToOnChainJson(p))
    return this.submitTransaction<'createProposal', ExpertsParametersVoting>('createProposal', [remark, mappedParameters], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsParametersVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<ExpertParameterProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.base.remark,
        executed: proposal.base.executed,
        params: {
          votingStartTime: proposal.base.params.votingStartTime.toString(),
          votingEndTime: proposal.base.params.votingEndTime.toString(),
          vetoEndTime: proposal.base.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.base.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.base.params.requiredQuorum.toString(),
          requiredMajority: proposal.base.params.requiredMajority.toString(),
          requiredSMajority: proposal.base.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.base.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.base.counters.weightFor.toString(),
          weightAgainst: proposal.base.counters.weightAgainst.toString(),
          vetosCount: proposal.base.counters.vetosCount.toString(),
        },
      },
      parametersSize: proposal.parametersSize.toString()
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsParametersVoting/#getparametersarr)
   */
  async getParametersArr(proposalId: string | number): Promise<RawParameter[]> {
    const params = await this.instance.getParametersArr(proposalId)

    return params.map((i) => ({
      paramKey: i.paramKey,
      paramType: i.paramType.toString() as ParameterType,
      addrValue: i.addrValue,
      boolValue: i.boolValue,
      bytes32Value: i.bytes32Value,
      strValue: i.strValue,
      uintValue: i.uintValue.toString(),
    }))
  }

  /**
   * Retrieve proposal parameters
   * @param proposalId chosen proposal id
   * @returns proposal parameters
   */
  async getProposedParameters(proposalId: string | number): Promise<Parameter[]> {
    const rawParameters = await this.getParametersArr(proposalId)
    const proposedParameters = rawParameters.map(raw => this.helper.mapOnChainJsonToParameter(raw))
    return proposedParameters
  }
}
