import { SignerOrProvider } from '../../../types'
import { EPQFI_Membership } from '../../../ethers-contracts/EPQFI_Membership'
import { ExpertsMembershipInstance } from './ExpertsMembershipInstance'

/**
 * EPQFI membership instance to interact with EPQFI membership contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPQFI_Membership/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epqfiMembership}
 */
export class EPQFIMembershipInstance extends ExpertsMembershipInstance<EPQFI_Membership> {
  public static readonly registryKey = 'governance.experts.EPQFI.membership'
  public static readonly abi = 'EPQFI_Membership.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, EPQFIMembershipInstance.abi, address)
  }
}
