import { SignerOrProvider } from '../../../types'
import { EPQFI_MembershipVoting } from '../../../ethers-contracts/EPQFI_MembershipVoting'
import { ExpertsMembershipVotingInstance } from './ExpertsMembershipVotingInstance'

/**
 * EPQFI membership voting instance to interact with EPQFI membership voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPQFI_MembershipVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epqfiMembershipVoting}
 */
export class EPQFIMembershipVotingInstance extends ExpertsMembershipVotingInstance<EPQFI_MembershipVoting> {
  public static readonly registryKey = 'governance.experts.EPQFI.membershipVoting'
  public static readonly abi = 'EPQFI_MembershipVoting.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, EPQFIMembershipVotingInstance.abi, address)
  }
}
