import { SignerOrProvider } from '../../../types'
import { EPDR_Parameters } from '../../../ethers-contracts/EPDR_Parameters'
import { SystemParametersInstance } from '../../SystemParametersInstance'

/**
 * EPDR parameters instance to interact with EPDR parameters contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPDR_Parameters/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epdrParameters}
 */
export class EPDRParametersInstance extends SystemParametersInstance<EPDR_Parameters> {
  public static readonly registryKey = 'governance.experts.EPDR.parameters'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EPDR_Parameters.json', address)
  }
}
