import { SignerOrProvider } from '../../../types'
import { EPRS_ParametersVoting } from '../../../ethers-contracts/EPRS_ParametersVoting'
import { ExpertsParametersVotingInstance } from '../experts/ExpertsParametersVotingInstance'

/**
 * EPRS parameters voting instance to interact with EPRS parameters voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPRS_ParametersVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.EPRSParametersVoting}
 */
export class EPRSParametersVotingInstance extends ExpertsParametersVotingInstance<EPRS_ParametersVoting> {
  public static readonly registryKey = 'governance.experts.EPRS.parametersVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EPRS_ParametersVoting.json', address)
  }
}
