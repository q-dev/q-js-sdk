import { SignerOrProvider } from '../../../types'
import { EPRS_Parameters } from '../../../ethers-contracts/EPRS_Parameters'
import { SystemParametersInstance } from '../../SystemParametersInstance'

/**
 * EPRS parameters instance to interact with EPRS parameters contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPRS_Parameters/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.EPRSParameters}
 */
export class EPRSParametersInstance extends SystemParametersInstance<EPRS_Parameters> {
  public static readonly registryKey = 'governance.experts.EPRS.parameters'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EPRS_Parameters.json', address)
  }
}
