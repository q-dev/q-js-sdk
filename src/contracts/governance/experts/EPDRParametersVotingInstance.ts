import { SignerOrProvider } from '../../../types'
import { EPDR_ParametersVoting } from '../../../ethers-contracts/EPDR_ParametersVoting'
import { ExpertsParametersVotingInstance } from '../experts/ExpertsParametersVotingInstance'

/**
 * EPDR parameters voting instance to interact with EPDR parameters voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EPDR_ParametersVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.epdrParametersVoting}
 */
export class EPDRParametersVotingInstance extends ExpertsParametersVotingInstance<EPDR_ParametersVoting> {
  public static readonly registryKey = 'governance.experts.EPDR.parametersVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EPDR_ParametersVoting.json', address)
  }
}
