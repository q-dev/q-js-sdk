import type { ContractTransaction } from 'ethers'

import { ExpertsMembershipVoting } from '../../../ethers-contracts/ExpertsMembershipVoting'
import {
  QNonPayableTx,
  ProposalWithBaseInfo,
  ExpertMembershipProposalDetails,
} from '../../../types'
import { SystemVotingVetoableInstance } from '../../SystemVotingVetoableInstance'

export interface ExpertMembershipProposal extends ProposalWithBaseInfo {
  proposalDetails: ExpertMembershipProposalDetails
}

/**
 * Expert membership voting contract instance to interact with EPDR and EPQFI membership voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/) for more details.
 */
export class ExpertsMembershipVotingInstance<T extends ExpertsMembershipVoting> extends SystemVotingVetoableInstance<T, ExpertMembershipProposal> {
  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#proposalcount)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<ExpertMembershipProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.base.remark,
        executed: proposal.base.executed,
        params: {
          votingStartTime: proposal.base.params.votingStartTime.toString(),
          votingEndTime: proposal.base.params.votingEndTime.toString(),
          vetoEndTime: proposal.base.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.base.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.base.params.requiredQuorum.toString(),
          requiredMajority: proposal.base.params.requiredMajority.toString(),
          requiredSMajority: proposal.base.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.base.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.base.counters.weightFor.toString(),
          weightAgainst: proposal.base.counters.weightAgainst.toString(),
          vetosCount: proposal.base.counters.vetosCount.toString(),
        },
      },
      proposalDetails: proposal.proposalDetails
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#hasrootvetoed)
   */
  async hasRootVetoed(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasRootVetoed(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#hasuservoted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasUserVoted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#createaddexpertproposal)
   */
  async createAddExpertProposal(remark: string, addressToAdd: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'createAddExpertProposal', ExpertsMembershipVoting>('createAddExpertProposal', [remark, addressToAdd], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#createchangeexpertproposal)
   */
  async createChangeExpertProposal(remark: string, addressToAdd: string, addressToRemove: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'createChangeExpertProposal', ExpertsMembershipVoting>('createChangeExpertProposal', [remark, addressToAdd, addressToRemove], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#createremoveexpertproposal)
   */
  async createRemoveExpertProposal(remark: string, addressToRemove: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'createRemoveExpertProposal', ExpertsMembershipVoting>('createRemoveExpertProposal', [remark, addressToRemove], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#getvotesfor)
   */
  async getVotesFor(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesFor(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembershipVoting/#getvotesagainst)
   */
  async getVotesAgainst(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesAgainst(proposalId)).toString()
  }
}
