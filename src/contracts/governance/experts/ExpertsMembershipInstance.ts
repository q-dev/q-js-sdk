import { ExpertsMembership } from '../../../ethers-contracts/ExpertsMembership'
import { SystemContractInstance } from '../../SystemContractInstance'

/**
 * Expert membership contract instance to interact with EPDR and EPQFI membership contract.
 * See [onchain documentation](@system-contracts-repo/@network/ExpertsMembership/) for more details.
 */
export class ExpertsMembershipInstance<T extends ExpertsMembership> extends SystemContractInstance<T> {
  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembership/#getmembers)
   */
  async getMembers(): Promise<string[]> {
    return this.instance.getMembers()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembership/#ismember)
   */
  async isMember(user: string): Promise<boolean> {
    return this.instance.isMember(user)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembership/#getsize)
   */
  async getSize(): Promise<string> {
    return (await this.instance.getSize()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ExpertsMembership/#getlimit)
   */
  async getLimit(): Promise<string> {
    return (await this.instance.getLimit()).toString()
  }
}
