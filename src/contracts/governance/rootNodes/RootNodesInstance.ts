import type { ContractTransaction, BigNumberish } from 'ethers'

import { Roots } from '../../../ethers-contracts/Roots'
import { QNonPayableTx, QPayableTx, RootNodesWithdrawalInfo, VotingLockInfo, SignerOrProvider } from '../../../types'
import { TimeLockHelperInstance } from '../../TimeLockHelperInstance'
import { VotingWeightProxyInstance } from '../VotingWeightProxyInstance'

export interface Stake {
  root: string
  value: string
}

/**
 * Root nodes instance to interact with Root nodes contract.
 * See [onchain documentation](@system-contracts-repo/@network/Roots/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.rootNodes}
 */
export class RootNodesInstance extends TimeLockHelperInstance<Roots> {
  public static readonly registryKey = 'governance.rootNodes'
  public static readonly abi = 'Roots.json'

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, RootNodesInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getlockinfo)
   */
  async getLockInfo(who: string): Promise<VotingLockInfo> {
    return this.votingWeightProxy.getLockInfo(this.address, who)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getslashingproposalids)
   */
  async getSlashingProposalIds(root: string): Promise<string[]> {
    return (await this.instance.getSlashingProposalIds(root))
      .map(i => i.toString())
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#applyslashing)
   */
  async applySlashing(
    proposalId: BigNumberish,
    root: string,
    amountToSlash: BigNumberish,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction('applySlashing', [proposalId, root, amountToSlash], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#addslashingproposal)
   */
  async addSlashingProposal(
    root: string,
    slashingProposalId: BigNumberish,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction('addSlashingProposal', [root, slashingProposalId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getmembers)
   */
  async getMembers(): Promise<string[]> {
    return this.instance.getMembers()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#ismember)
   */
  async isMember(user: string): Promise<boolean> {
    return this.instance.isMember(user)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getsize)
   */
  async getSize(): Promise<string> {
    return (await this.instance.getSize()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getlimit)
   */
  async getLimit(): Promise<string> {
    return (await this.instance.getLimit()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getrootnodestake)
   */
  async getRootNodeStake(root: string): Promise<string> {
    return (await this.instance.getRootNodeStake(root)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#getstakes)
   */
  async getStakes(): Promise<Stake[]> {
    const rawStakes = await this.instance.getStakes()
    return rawStakes.map((stake) => ({
      root: stake.root,
      value: stake.value.toString()
    }))
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#commitstake)
   */
  async commitStake(txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('commitStake', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#announcewithdrawal)
   */
  async announceWithdrawal(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('announceWithdrawal', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#withdraw)
   */
  async withdraw(amount: string, payTo: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [amount, payTo], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Roots/#withdrawals)
   */
  async getWithdrawalInfo(address: string): Promise<RootNodesWithdrawalInfo> {
    const withdrawalInfo = await this.instance.withdrawals(address)
    return {
      endTime: withdrawalInfo.endTime.toString(),
      amount: withdrawalInfo.amount.toString(),
    }
  }

  /**
   * [External documentation](https://q-dev.gitlab.io/system-contracts/latest/Roots/#purgependingslashings)
   */
  async purgePendingSlashings(root: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('purgePendingSlashings', [root], txOptions)
  }
}
