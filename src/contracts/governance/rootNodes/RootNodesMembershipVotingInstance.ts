import type { ContractTransaction } from 'ethers'

import { RootsVoting } from '../../../ethers-contracts/RootsVoting'
import { QNonPayableTx, ProposalWithBaseInfo, SignerOrProvider } from '../../../types'
import { VotingWeightProxyInstance } from '../VotingWeightProxyInstance'
import { SystemVotingVetoableInstance } from '../../SystemVotingVetoableInstance'

export interface RootsProposal extends ProposalWithBaseInfo {
  candidate: string;
  replaceDest: string;
}

/**
 * Root nodes voting instance to interact with Root nodes voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/RootsVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.rootNodesMembershipVoting}
 */
export class RootNodesMembershipVotingInstance extends SystemVotingVetoableInstance<RootsVoting, RootsProposal> {
  public static readonly registryKey = 'governance.rootNodes.membershipVoting'
  public static readonly abi = 'RootsVoting.json'

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, RootNodesMembershipVotingInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<RootsProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.base.remark,
        executed: proposal.base.executed,
        params: {
          votingStartTime: proposal.base.params.votingStartTime.toString(),
          votingEndTime: proposal.base.params.votingEndTime.toString(),
          vetoEndTime: proposal.base.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.base.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.base.params.requiredQuorum.toString(),
          requiredMajority: proposal.base.params.requiredMajority.toString(),
          requiredSMajority: proposal.base.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.base.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.base.counters.weightFor.toString(),
          weightAgainst: proposal.base.counters.weightAgainst.toString(),
          vetosCount: proposal.base.counters.vetosCount.toString(),
        },
      },
      candidate: proposal.candidate,
      replaceDest: proposal.replaceDest,
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#hasrootvetoed)
   */
  async hasRootVetoed(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasRootVetoed(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#hasuservoted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasUserVoted(proposalId, address)
  }

  /**
   * Calculates proposal count.
   * @returns proposal count.
   */
  async proposalCount(): Promise<string> {
    throw Error('"proposalCount" not supported on this kind of contact')
  }

  /**
   * Create proposal for add new member
   * @param remark proposal remark
   * @param addressToAdd new member
   * @param constitutionHash hash
   * @param txOptions sender parameters
   * @returns transaction receipt
   */
  async createAddProposal(remark: string, addressToAdd: string, constitutionHash = this.adapter.ZERO_BYTES_32, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.createProposal(remark, addressToAdd, this.adapter.ZERO_ADDRESS, constitutionHash, txOptions)
  }

  /**
   * Create proposal for remove member
   * @param remark proposal remark
   * @param addressToRemove member to remove
   * @param txOptions sender parameters
   * @returns transaction receipt
   */
  async createRemoveProposal(remark: string, addressToRemove: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.createProposal(remark, this.adapter.ZERO_ADDRESS, addressToRemove, this.adapter.ZERO_BYTES_32, txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#createproposal)
   */
  async createProposal(remark: string, addressToAdd: string, addressToRemove: string, constitutionHash = this.adapter.ZERO_BYTES_32, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [remark, constitutionHash, addressToAdd, addressToRemove], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#getvotesfor)
   */
  async getVotesFor(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesFor(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#getvotesagainst)
   */
  async getVotesAgainst(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesAgainst(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#isoverruleapplied)
   */
  async isOverruleApplied(proposalId: number | string): Promise<boolean> {
    return this.instance.isOverruleApplied(proposalId)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootsVoting/#skipvetophase)
   */
  async skipVetoPhase(proposalId: number | string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('skipVetoPhase', [proposalId], txOptions)
  }
}
