import type { ContractTransaction } from 'ethers'

import { RootNodesSlashingVoting } from '../../../ethers-contracts/RootNodesSlashingVoting'
import { SlashingProposal, SlashingVotingHelper } from '../../SlashingVotingHelperInstance'
import { QNonPayableTx, SignerOrProvider } from '../../../types'
import { SystemVotingVetoableInstance } from '../../SystemVotingVetoableInstance'
import { VotingWeightProxyInstance } from '../VotingWeightProxyInstance'

/**
 * Root nodes Slashing voting instance to interact with Root nodes Slashing voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.rootNodesSlashingVoting}
 */
export class RootNodesSlashingVotingInstance extends SystemVotingVetoableInstance<RootNodesSlashingVoting, SlashingProposal> {
  public static readonly registryKey = 'governance.rootNodes.slashingVoting'
  public static readonly abi = 'RootNodesSlashingVoting.json'

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance
  private slashingVoting: SlashingVotingHelper<RootNodesSlashingVoting>

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, RootNodesSlashingVotingInstance.abi, address)
    this.slashingVoting = new SlashingVotingHelper<RootNodesSlashingVoting>(signerOrProvider, RootNodesSlashingVotingInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#getproposal)
   */
  async getProposal(proposalId: string | number): Promise<SlashingProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.base.remark,
        executed: proposal.base.executed,
        params: {
          votingStartTime: proposal.base.params.votingStartTime.toString(),
          votingEndTime: proposal.base.params.votingEndTime.toString(),
          vetoEndTime: proposal.base.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.base.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.base.params.requiredQuorum.toString(),
          requiredMajority: proposal.base.params.requiredMajority.toString(),
          requiredSMajority: proposal.base.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.base.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.base.counters.weightFor.toString(),
          weightAgainst: proposal.base.counters.weightAgainst.toString(),
          vetosCount: proposal.base.counters.vetosCount.toString(),
        },
      },
      proposer: proposal.proposer,
      candidate: proposal.candidate,
      amountToSlash: proposal.amountToSlash.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#hasrootvetoed)
   */
  async hasRootVetoed(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasRootVetoed(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#hasuservoted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasUserVoted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#proposalcount)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#createproposal)
   */
  async createProposal(remark: string, candidate: string, percentage: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [remark, candidate, percentage], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#slashingaffectswithdrawal)
   */
  async slashingAffectsWithdrawal(proposalId: string | number): Promise<boolean> {
    return this.instance.slashingAffectsWithdrawal(proposalId)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#getproposalstarttime)
   */
  async getProposalStartTime(proposalId: string | number): Promise<string> {
    return (await this.instance.getProposalStartTime(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#isoverruleapplied)
   */
  async isOverruleApplied(proposalId: number | string): Promise<boolean> {
    return this.instance.isOverruleApplied(proposalId)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#skipvetophase)
   */
  async skipVetoPhase(proposalId: number | string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('skipVetoPhase', [proposalId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#getslashingvictim)
   */
  async getSlashingVictim(proposalId: string | number): Promise<string> {
    return this.slashingVoting.getSlashingVictim(proposalId)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodesSlashingVoting/#getslashingproposer)
   */
  async getSlashingProposer(proposalId: string | number): Promise<string> {
    return this.slashingVoting.getSlashingProposer(proposalId)
  }
}
