import { SignerOrProvider } from '../../../types'
import { RootNodeSlashingEscrow } from '../../../ethers-contracts/RootNodeSlashingEscrow'
import { SystemSlashingEscrowInstance } from '../../SystemSlashingEscrowInstance'

/**
 * Root nodes Slashing Escrow instance to interact with Root nodes Slashing Escrow contract.
 * See [onchain documentation](@system-contracts-repo/@network/RootNodeSlashingEscrow/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.rootNodeSlashingEscrow}
 */
export class RootNodeSlashingEscrowInstance extends SystemSlashingEscrowInstance<RootNodeSlashingEscrow> {
  public static readonly abi = 'RootNodeSlashingEscrow.json'
  public static readonly registryKey = 'governance.rootNodes.slashingEscrow'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, RootNodeSlashingEscrowInstance.abi, address)
  }
}
