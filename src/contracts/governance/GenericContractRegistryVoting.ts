import type { BigNumberish, ContractTransaction } from 'ethers'

import { GenericContractRegistryProposal, ProposalStats, ProposalStatus, QNonPayableTx, SignerOrProvider } from '../../types'

import { GenericContractRegistryVoting } from '../../ethers-contracts'
import { SystemRootNodeApprovalVoting } from '../SystemRootNodeApprovalVoting'

export class GenericContractRegistryVotingInstance extends SystemRootNodeApprovalVoting<GenericContractRegistryVoting> {
  public static readonly registryKey = 'governance.contractRegistryVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'GenericContractRegistryVoting.json', address)
  }

  async createProposal(remark: string, calldata: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [remark, calldata], txOptions)
  }

  async approve(id: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('approve', [id], txOptions)
  }

  async getProposal(id: BigNumberish): Promise<GenericContractRegistryProposal> {
    const proposal = await this.instance.getProposal(id)
    return {
      id: id.toString(),
      status: await this.getStatus(id),
      executed: proposal.executed,
      votingStartTime: proposal.votingStartTime.toString(),
      votingExpiredTime: proposal.votingExpiredTime.toString(),
      requiredMajority: proposal.requiredMajority.toString(),
      remark: proposal.remark,
      callData: proposal.callData
    }
  }

  async getStatus(proposalId: BigNumberish): Promise<ProposalStatus> {
    return String(await this.instance.getStatus(proposalId)) as ProposalStatus
  }

  async getProposalStats(id: BigNumberish): Promise<ProposalStats> {
    const proposalStats = await this.instance.getProposalStats(id)
    return {
      requiredMajority: proposalStats.requiredMajority.toString(),
      currentMajority: proposalStats.currentMajority.toString(),
    }
  }
}
