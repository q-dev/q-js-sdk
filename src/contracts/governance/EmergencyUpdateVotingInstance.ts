import type { ContractTransaction } from 'ethers'

import { BaseProposal, QNonPayableTx, SignerOrProvider } from '../../types'
import { EmergencyUpdateVoting } from '../../ethers-contracts/EmergencyUpdateVoting'
import { SystemVotingInstance } from '../SystemVotingInstance'

export interface EmergencyProposal {
  base: BaseProposal;
}

/**
 * Emergency update voting instance to interact with Emergency update voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.emergencyUpdateVoting}
 */
export class EmergencyUpdateVotingInstance extends SystemVotingInstance<EmergencyUpdateVoting, EmergencyProposal> {
  public static readonly registryKey = 'governance.emergencyUpdateVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'EmergencyUpdateVoting.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/#proposalcount)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/#voted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.voted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/#createproposal)
   */
  async createProposal(remark: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [remark], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/#getvotesfor)
   */
  async getVotesFor(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesFor(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/#getvotesagainst)
   */
  async getVotesAgainst(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesAgainst(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/EmergencyUpdateVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<EmergencyProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.remark,
        executed: proposal.executed,
        params: {
          votingStartTime: proposal.params.votingStartTime.toString(),
          votingEndTime: proposal.params.votingEndTime.toString(),
          vetoEndTime: proposal.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.params.requiredQuorum.toString(),
          requiredMajority: proposal.params.requiredMajority.toString(),
          requiredSMajority: proposal.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.counters.weightFor.toString(),
          weightAgainst: proposal.counters.weightAgainst.toString(),
          vetosCount: proposal.counters.vetosCount.toString(),
        },
      },
    }
  }
}
