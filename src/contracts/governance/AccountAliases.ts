import type { ContractTransaction, BigNumberish } from 'ethers'

import { AccountAliases } from '../../ethers-contracts/AccountAliases'
import { SystemContractInstance } from '../SystemContractInstance'
import { QNonPayableTx, AliasPurpose, Alias, SignerOrProvider } from '../../types'

export class AccountAliasesInstance extends SystemContractInstance<AccountAliases> {
  public static readonly registryKey = 'governance.accountAliases'
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'AccountAliases.json', address)
  }

  async setAlias(alias: string, purpose: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('setAlias', [alias, purpose], txOptions)
  }

  async reserve(owner: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('reserve', [owner], txOptions)
  }

  async resolve(address: string, purpose: BigNumberish): Promise<string> {
    return this.instance.resolve(address, purpose)
  }

  async resolveReverse(address: string, purpose: BigNumberish): Promise<string> {
    return this.instance.resolveReverse(address, purpose)
  }

  async resolveBatch(address: string[], purpose: BigNumberish[]): Promise<string[]> {
    return this.instance.resolveBatch(address, purpose)
  }

  async resolveReverseBatch(address: string[], purpose: BigNumberish[]): Promise<string[]> {
    return this.instance.resolveBatchReverse(address, purpose)
  }

  async getAliasOwner(alias: string): Promise<string> {
    return this.instance.reverseAliases(alias)
  }

  async getAliases(owner: string): Promise<Alias[]> {
    const aliases: Alias[] = []
    for (const purpose in AliasPurpose) {
      const alias = await this.resolve(owner, AliasPurpose[purpose])
      aliases.push({ purpose: AliasPurpose[purpose], address: alias })
    }
    return aliases
  }
}
