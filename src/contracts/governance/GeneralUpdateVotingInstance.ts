import type { ContractTransaction } from 'ethers'

import { BaseProposal, QNonPayableTx, SignerOrProvider } from '../../types'
import { GeneralUpdateVoting } from '../../ethers-contracts/GeneralUpdateVoting'
import { SystemVotingVetoableInstance } from '../SystemVotingVetoableInstance'
import { VotingWeightProxyInstance } from './VotingWeightProxyInstance'

export interface GeneralProposal {
  base: BaseProposal;
}

/**
 * General update voting instance to interact with General update voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/GeneralUpdateVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.generalUpdateVoting}
 */
export class GeneralUpdateVotingInstance extends SystemVotingVetoableInstance<GeneralUpdateVoting, GeneralProposal> {
  public static readonly registryKey = 'governance.generalUpdateVoting'
  public static readonly abi = 'GeneralUpdateVoting.json'

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, GeneralUpdateVotingInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#proposalcount)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#hasrootvetoed)
   */
  async hasRootVetoed(proposalId: number | string, address: string): Promise<boolean> {
    return this.instance.hasRootVetoed(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#hasuservoted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasUserVoted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#createproposal)
   */
  async createProposal(remark: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [remark], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#getvotesfor)
   */
  async getVotesFor(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesFor(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#getvotesagainst)
   */
  async getVotesAgainst(proposalId: number | string): Promise<string> {
    return (await this.instance.getVotesAgainst(proposalId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/GeneralUpdateVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<GeneralProposal> {
    const proposal = await this.instance.proposals(proposalId)
    return {
      base: {
        remark: proposal.remark,
        executed: proposal.executed,
        params: {
          votingStartTime: proposal.params.votingStartTime.toString(),
          votingEndTime: proposal.params.votingEndTime.toString(),
          vetoEndTime: proposal.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.params.requiredQuorum.toString(),
          requiredMajority: proposal.params.requiredMajority.toString(),
          requiredSMajority: proposal.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.counters.weightFor.toString(),
          weightAgainst: proposal.counters.weightAgainst.toString(),
          vetosCount: proposal.counters.vetosCount.toString(),
        },
      },
    }
  }
}
