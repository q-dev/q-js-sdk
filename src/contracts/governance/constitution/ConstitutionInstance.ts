import { SignerOrProvider } from '../../../types'
import { Constitution } from '../../../ethers-contracts/Constitution'
import { SystemParametersInstance } from '../../SystemParametersInstance'

/**
 * Constitution instance to interact with Constitution contract.
 * See [onchain documentation](@system-contracts-repo/@network/Constitution/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.constitution}
 */
export class ConstitutionInstance extends SystemParametersInstance<Constitution> {
  public static readonly registryKey = 'governance.constitution.parameters'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'Constitution.json', address)
  }
}
