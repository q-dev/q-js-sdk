import type { ContractTransaction } from 'ethers'

import { Classification,
  Parameter,
  QNonPayableTx,
  RawParameter,
} from '../../../types'
import { ConstitutionVoting } from '../../../ethers-contracts/ConstitutionVoting'
import { ParamVotingHelper } from '../../../utils/param-voting-helper'
import { ProposalWithBaseInfo, ParameterType, SignerOrProvider } from '../../../types'
import { SystemVotingVetoableInstance } from '../../SystemVotingVetoableInstance'
import { VotingWeightProxyInstance } from '../VotingWeightProxyInstance'

export interface ConstitutionProposal extends ProposalWithBaseInfo {
  parametersSize: string,
  classification: Classification,
  newConstitutionHash: string,
  currentConstitutionHash: string
}

/**
 * Constitution voting instance to interact with Constitution voting contract.
 * See [onchain documentation](@system-contracts-repo/@network/ConstitutionVoting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.constitutionVoting}
 */
export class ConstitutionVotingInstance extends SystemVotingVetoableInstance<ConstitutionVoting, ConstitutionProposal> {
  public static readonly registryKey = 'governance.constitution.parametersVoting'
  public static readonly abi = 'ConstitutionVoting.json'

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance
  private helper = new ParamVotingHelper()

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, ConstitutionVotingInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#hasrootvetoed)
   */
  async hasRootVetoed(proposalId: number | string, address: string): Promise<boolean> {
    return this.instance.hasRootVetoed(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#hasuservoted)
   */
  async hasUserVoted(proposalId: string | number, address: string): Promise<boolean> {
    return this.instance.hasUserVoted(proposalId, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#proposalcounter)
   */
  async proposalCount(): Promise<string> {
    return (await this.instance.proposalCounter()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#createproposal)
   */
  async createProposal(remark: string, classification: Classification, newConstitutionHash: string, parameters: Parameter[], txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    const mappedParameters = parameters.map(p => this.helper.mapParameterToOnChainJson(p))
    return this.submitTransaction<'createProposal', ConstitutionVoting>('createProposal', [remark, classification, newConstitutionHash, mappedParameters], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#proposals)
   */
  async getProposal(proposalId: string | number): Promise<ConstitutionProposal> {
    const proposal = await this.instance.proposals(proposalId)

    return {
      base: {
        remark: proposal.base.remark,
        executed: proposal.base.executed,
        params: {
          votingStartTime: proposal.base.params.votingStartTime.toString(),
          votingEndTime: proposal.base.params.votingEndTime.toString(),
          vetoEndTime: proposal.base.params.vetoEndTime.toString(),
          proposalExecutionP: proposal.base.params.proposalExecutionP.toString(),
          requiredQuorum: proposal.base.params.requiredQuorum.toString(),
          requiredMajority: proposal.base.params.requiredMajority.toString(),
          requiredSMajority: proposal.base.params.requiredSMajority.toString(),
          requiredSQuorum: proposal.base.params.requiredSQuorum.toString(),
        },
        counters: {
          weightFor: proposal.base.counters.weightFor.toString(),
          weightAgainst: proposal.base.counters.weightAgainst.toString(),
          vetosCount: proposal.base.counters.vetosCount.toString(),
        },
      },
      parametersSize: proposal.parametersSize.toString(),
      classification: proposal.classification,
      newConstitutionHash: proposal.newConstitutionHash,
      currentConstitutionHash: proposal.currentConstitutionHash
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#getparametersarr)
   */
  async getParametersArr(proposalId: string | number): Promise<RawParameter[]> {
    const params = await this.instance.getParametersArr(proposalId)

    return params.map((i) => ({
      paramKey: i.paramKey,
      paramType: i.paramType.toString() as ParameterType,
      addrValue: i.addrValue,
      boolValue: i.boolValue,
      bytes32Value: i.bytes32Value,
      strValue: i.strValue,
      uintValue: i.uintValue.toString(),
    }))
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#getconstitutionhash)
   */
  async getConstitutionHash(proposalId: string | number): Promise<string> {
    return this.instance.getConstitutionHash(proposalId)
  }

  /**
   * Retrieve proposal parameters.
   * @param proposalId chosen proposal id
   * @returns Proposal parameters
   */
  async getProposedParameters(proposalId: string | number): Promise<Parameter[]> {
    const rawParameters = await this.getParametersArr(proposalId)
    const proposedParameters = rawParameters.map(raw => this.helper.mapOnChainJsonToParameter(raw))
    return proposedParameters
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ConstitutionVoting/#constitutionhash)
   */
  async constitutionHash(): Promise<string> {
    return this.instance.constitutionHash()
  }
}
