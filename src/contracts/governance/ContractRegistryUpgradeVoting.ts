import type { ContractTransaction, BigNumberish } from 'ethers'

import { ContractRegistryUpgradeVoting } from '../../ethers-contracts/ContractRegistryUpgradeVoting'
import { UpgradeProposal, QNonPayableTx, SignerOrProvider } from '../../types'
import { SystemRootNodeApprovalVoting } from '../SystemRootNodeApprovalVoting'

export class ContractRegistryUpgradeVotingInstance extends SystemRootNodeApprovalVoting<ContractRegistryUpgradeVoting> {
  public static readonly registryKey = 'governance.upgrade.contractRegistryVoting'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'ContractRegistryUpgradeVoting.json', address)
  }

  async createProposal(proxy: string, implementation: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createProposal', [proxy, implementation], txOptions)
  }

  async getProposal(id: BigNumberish): Promise<UpgradeProposal> {
    const proposal = await this.instance.getProposal(id)
    return {
      id: id.toString(),
      status: await this.getStatus(id),
      executed: proposal.executed,
      votingStartTime: proposal.votingStartTime.toString(),
      votingExpiredTime: proposal.votingExpiredTime.toString(),
      proxy: proposal.proxy,
      implementation: proposal.implementation,
    }
  }
}
