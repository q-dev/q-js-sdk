import type { ContractTransaction, BigNumberish } from 'ethers'

import { BaseVotingWeightInfo, QNonPayableTx, VotingDelegationInfo, VotingLockInfo, SignerOrProvider } from '../../types'
import { VotingWeightProxy } from '../../ethers-contracts/VotingWeightProxy'
import { SystemContractInstance } from '../SystemContractInstance'

/**
 * Voting weight proxy instance to interact with Voting weight proxy contract.
 * See [onchain documentation](@system-contracts-repo/@network/VotingWeightProxy/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.votingWeightProxy}
 */
export class VotingWeightProxyInstance extends SystemContractInstance<VotingWeightProxy> {
  public static readonly registryKey = 'governance.votingWeightProxy'
  public static readonly abi = 'VotingWeightProxy.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, VotingWeightProxyInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#announcenewvotingagent)
   */
  async announceNewVotingAgent(newVotingAgentAddr: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('announceNewVotingAgent', [newVotingAgentAddr], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#setnewvotingagent)
   */
  async setNewVotingAgent(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('setNewVotingAgent', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#getbasevotingweightinfo)
   */
  async getBaseVotingWeightInfo(address: string, votingEndTime: BigNumberish): Promise<BaseVotingWeightInfo> {
    const result = await this.instance.getBaseVotingWeightInfo(address, votingEndTime)

    return {
      ownWeight: result.ownWeight.toString(),
      votingAgent: result.votingAgent,
      delegationStatus: result.delegationStatus.toString(),
      lockedUntil: result.lockedUntil.toString()
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#delegationinfos)
   */
  async getDelegationInfo(address: string): Promise<VotingDelegationInfo> {
    const result = await this.instance.delegationInfos(address)
    return {
      receivedWeight: result.receivedWeight.toString(),
      votingAgent: result.votingAgent,
      isPending: result.isPending,
      votingAgentPassOverTime: result.votingAgentPassOverTime.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#getlockedamount)
   */
  async getLockedAmount(address: string): Promise<string> {
    return (await this.instance.getLockedAmount(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#getlockinfo)
   */
  async getLockInfo(tokenLockSource: string, who: string): Promise<VotingLockInfo> {
    const rawLockInfo = await this.instance.getLockInfo(tokenLockSource, who)
    return {
      lockedAmount: rawLockInfo.lockedAmount.toString(),
      lockedUntil: rawLockInfo.lockedUntil.toString(),
      pendingUnlockAmount: rawLockInfo.pendingUnlockAmount.toString(),
      pendingUnlockTime: rawLockInfo.pendingUnlockTime.toString()
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/VotingWeightProxy/#getvotingweight)
   */
  async getVotingWeight(address: string, votingEndTime: string): Promise<string> {
    return (await this.instance.getVotingWeight(address, votingEndTime)).toString()
  }}
