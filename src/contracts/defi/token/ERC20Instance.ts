import type { ContractTransaction } from 'ethers'

import { ERC20 } from '../../../ethers-contracts/ERC20'
import { QNonPayableTx, SignerOrProvider } from '../../../types'
import { SystemContractInstance } from '../../SystemContractInstance'

/**
 * ERC20 instance to interact with Stablecoin contract.
 * See [onchain documentation](@system-contracts-repo/@network/ERC20/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.stableCoin}
 */
export class ERC20Instance extends SystemContractInstance<ERC20> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'ERC20.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#approve)
   */
  async approve(spender: string, amount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('approve', [spender, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#allowance)
   */
  async allowance(owner: string, spender: string): Promise<string> {
    return (await this.instance.allowance(owner, spender)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#transfer)
   */
  async transfer(recipient: string, amount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('transfer', [recipient, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#transferfrom)
   */
  async transferFrom(sender: string, recipient: string, amount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('transferFrom', [sender, recipient, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#balanceof)
   */
  async balanceOf(address: string): Promise<string> {
    return (await this.instance.balanceOf(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#totalsupply)
   */
  async totalSupply(): Promise<string> {
    return (await this.instance.totalSupply()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#name)
   */
  async name(): Promise<string> {
    return this.instance.name()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#symbol)
   */
  async symbol(): Promise<string> {
    return this.instance.symbol()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#decimals)
   */
  async decimals(): Promise<string> {
    return (await this.instance.decimals()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#increaseallowance)
   */
  async increaseAllowance(spender: string, amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('increaseAllowance', [spender, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#decreaseallowance)
   */
  async decreaseAllowance(spender: string, amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('decreaseAllowance', [spender, amount], txOptions)
  }
}
