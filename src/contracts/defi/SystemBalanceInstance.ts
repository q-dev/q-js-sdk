import type { ContractTransaction } from 'ethers'

import { SystemBalanceMock } from '../../ethers-contracts/SystemBalanceMock'
import { SystemContractInstance } from '../SystemContractInstance'
import { QPayableTx, SystemBalanceDetails, QNonPayableTx, SignerOrProvider } from '../../types'

/**
 * [System balance contract instance](@system-contracts-repo/@network/SystemBalance/)
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.systemBalance}
 */
/**
 * System balance instance to interact with System balance contract.
 * See [onchain documentation](@system-contracts-repo/@network/SystemBalance/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.systemBalance}
 */
export class SystemBalanceInstance extends SystemContractInstance<SystemBalanceMock> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'SystemBalance.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#getdebt)
   */
  async getDebt(): Promise<string> {
    return (await this.instance.getDebt()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#getbalance)
   */
  async getBalance(): Promise<string> {
    return (await this.instance.getBalance()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#getsurplus)
   */
  async getSurplus(): Promise<string> {
    return (await this.instance.getSurplus()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#getbalancedetails)
   */
  async getBalanceDetails(): Promise<SystemBalanceDetails> {
    const balanceDetails = await this.instance.getBalanceDetails()
    return {
      isDebtAuctionPossible: balanceDetails.isDebtAuctionPossible,
      isSurplusAuctionPossible: balanceDetails.isSurplusAuctionPossible,
      currentDebt: balanceDetails.currentDebt.toString(),
      debtThreshold: balanceDetails.debtThreshold.toString(),
      currentSurplus: balanceDetails.currentSurplus.toString(),
      surplusThreshold: balanceDetails.surplusThreshold.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#setdebt)
   */
  async setDebt(debt: number | string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('setDebt', [debt], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#increasedebt)
   */
  async increaseDebt(debtAmount: string, txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('increaseDebt', [debtAmount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemBalance/#performnetting)
   */
  async performNetting(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('performNetting', [], txOptions)
  }
}
