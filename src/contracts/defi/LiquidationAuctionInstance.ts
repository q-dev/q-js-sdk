import type { ContractTransaction } from 'ethers'

import { LiquidationAuction } from '../../ethers-contracts/LiquidationAuction'
import { SystemContractInstance } from '../SystemContractInstance'
import { AuctionInfo, QNonPayableTx, AuctionStatus, SignerOrProvider } from '../../types'

/**
 * Liquidation auction instance to interact with Liquidation auction contract.
 * See [onchain documentation](@system-contracts-repo/@network/LiquidationAuction/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.liquidationAuction}
 */
export class LiquidationAuctionInstance extends SystemContractInstance<LiquidationAuction> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'LiquidationAuction.json', address)
  }

  /**
   * Get all auctions from block diapason
   * @param fromBlock first block from which we get all auctions
   * @param toBlock last block from which we get all auctions
   * @returns auctions
   */
  async getAuctions(fromBlock: string | number = 0, toBlock: string | number = 'latest'): Promise<{user: string; vaultId: string}[]> {
    const auctionEvents = await this.instance.queryFilter(
      this.instance.filters.AuctionStarted(),
      fromBlock,
      toBlock
    )
    const auctionIds = auctionEvents.map((evt) => {
      return {
        user: evt.args._user,
        vaultId: evt.args._vaultId.toString(),
      }
    })

    return auctionIds
  }


  /**
   * [External documentation](@system-contracts-repo/@network/LiquidationAuction/#auctions)
   */
  async getAuctionInfo(user: string, vaultId: string|number): Promise<AuctionInfo> {
    const rawInfo = await this.instance.auctions(user, vaultId)
    return {
      status: rawInfo.status.toString() as AuctionStatus,
      bidder: rawInfo.bidder,
      highestBid: rawInfo.highestBid.toString(),
      endTime: rawInfo.endTime.toString() as unknown as Date,
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/LiquidationAuction/#startauction)
   */
  async startAuction(user: string, vaultId: string|number, bid: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('startAuction', [user, vaultId, bid], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/LiquidationAuction/#bid)
   */
  async bid(user: string, vaultId: string|number, bid: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('bid', [user, vaultId, bid], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/LiquidationAuction/#execute)
   */
  async execute(user: string, vaultId: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('execute', [user, vaultId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/LiquidationAuction/#getraisingbid)
   */
  async getRaisingBid(user: string, vaultId: string|number): Promise<string> {
    return (await this.instance.getRaisingBid(user, vaultId)).toString()
  }
}
