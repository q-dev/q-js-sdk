import type { ContractTransaction } from 'ethers'

import { BorrowingCore } from '../../ethers-contracts/BorrowingCore'
import { SystemContractInstance } from '../SystemContractInstance'
import { BorrowingTotals, QNonPayableTx, Vault, VaultStats, SignerOrProvider } from '../../types'
import { CompoundRateKeeperInstance } from '../common/CompoundRateKeeperInstance'

/**
 * Borrowing core instance to interact with Borrowing core contract.
 * See [onchain documentation](@system-contracts-repo/@network/BorrowingCore/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.borrowingCore}
 */
export class BorrowingCoreInstance extends SystemContractInstance<BorrowingCore> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'BorrowingCore.json', address)
  }

  /** @deprecated use getCompoundRateKeeper */
  async compoundRateKeeper(colKey: string): Promise<string> {
    return this.instance.compoundRateKeeper(colKey)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#getcompoundratekeeper)
   */
  async getCompoundRateKeeper(colKey: string): Promise<CompoundRateKeeperInstance> {
    const compoundRateKeeperAddress = await this.compoundRateKeeper(colKey)
    return new CompoundRateKeeperInstance(this.adapter.signerOrProvider, compoundRateKeeperAddress)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#totalstcbackedbycol)
   */
  async totalStcBackedByCol(colKey: string): Promise<string> {
    return (await this.instance.totalStcBackedByCol(colKey)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#updatecompoundrate)
   */
  async updateCompoundRate(col: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('updateCompoundRate', [col], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#uservaults)
   */
  async userVaults(address: string, vaultId: number | string): Promise<Vault> {
    const vault = await this.instance.userVaults(address, vaultId)
    return {
      colKey: vault.colKey,
      colAsset: vault.colAsset.toString(),
      normalizedDebt: vault.normalizedDebt.toString(),
      mintedAmount: vault.mintedAmount.toString(),
      isLiquidated: vault.isLiquidated,
      liquidationFullDebt: vault.liquidationFullDebt.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#uservaultscount)
   */
  async userVaultsCount(address: string): Promise<string> {
    return (await this.instance.userVaultsCount(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#createvault)
   */
  async createVault(colKey: string, txOPtions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('createVault', [colKey], txOPtions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#depositcol)
   */
  async depositCol(vaultId: string|number, amount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('depositCol', [vaultId, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#generatestc)
   */
  async generateStc(vaultId: string|number, stcAmount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('generateStc', [vaultId, stcAmount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#clearvault)
   */
  async clearVault(user: string, vaultId: number | string, amount: number | string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('clearVault', [user, vaultId, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#liquidate)
   */
  async liquidate(user: string, vaultId: number | string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('liquidate', [user, vaultId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#transfercol)
   */
  async transferCol(user: string, vaultId: number | string, recipient: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('transferCol', [user, vaultId, recipient], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#paybackstc)
   */
  async payBackStc(vaultId: string|number, amount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('payBackStc', [vaultId, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#withdrawcol)
   */
  async withdrawCol(vaultId: string|number, amount: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdrawCol', [vaultId, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#getaggregatedtotals)
   */
  async getAggregatedTotals(): Promise<BorrowingTotals> {
    const aggregatedTotals = await this.instance.getAggregatedTotals()
    return {
      outstandingDebt: aggregatedTotals.outstandingDebt.toString(),
      mintedAmount: aggregatedTotals.mintedAmount.toString(),
      owedBorrowingFees: aggregatedTotals.owedBorrowingFees.toString(),
    }
  }

  /**
   * Retrieve all user vaults
   * @param address account
   * @returns user vaults
   */
  async getAllUserVaults(address: string): Promise<Vault[]> {
    const vaultsCount = Number(await this.getUserVaultsCount(address))
    const resultArr: Vault[] = []
    for (let i = 0; i < vaultsCount; i++) {
      resultArr.push(await this.getUserVault(address, i))
    }

    return resultArr
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#uservaults)
   */
  async getUserVault(address: string, vaultId: string|number): Promise<Vault> {
    const vault = await this.instance.userVaults(address, vaultId)
    return {
      colKey: vault.colKey,
      colAsset: vault.colAsset.toString(),
      normalizedDebt: vault.normalizedDebt.toString(),
      mintedAmount: vault.mintedAmount.toString(),
      isLiquidated: vault.isLiquidated,
      liquidationFullDebt: vault.liquidationFullDebt.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#userVaultsCount)
   */
  async getUserVaultsCount(address: string): Promise<string> {
    return (await this.instance.userVaultsCount(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#getvaultstats)
   */
  async getVaultStats(address: string, vaultId: string|number): Promise<VaultStats> {
    const resultArr = await this.instance.getVaultStats(address, vaultId)

    const colStatsArr = resultArr[0]
    const stcStatsArr = resultArr[1]

    return {
      colStats: {
        key: colStatsArr.key,
        balance: colStatsArr.balance.toString(),
        price: colStatsArr.price.toString(),
        withdrawableAmount: colStatsArr.withdrawableAmount.toString(),
        liquidationPrice: colStatsArr.liquidationPrice.toString()
      },
      stcStats: {
        key: stcStatsArr.key,
        outstandingDebt: stcStatsArr.outstandingDebt.toString(),
        normalizedDebt: stcStatsArr.normalizedDebt.toString(),
        compoundRate: stcStatsArr.compoundRate.toString(),
        lastUpdateOfCompoundRate: stcStatsArr.lastUpdateOfCompoundRate.toString(),
        borrowingLimit: stcStatsArr.borrowingLimit.toString(),
        availableToBorrow: stcStatsArr.availableToBorrow.toString(),
        liquidationLimit: stcStatsArr.liquidationLimit.toString(),
        borrowingFee: stcStatsArr.borrowingFee.toString(),
      }
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#getfulldebt)
   */
  async getFullDebt(address: string, vaultId: string|number): Promise<string> {
    return (await this.instance.getFullDebt(address, vaultId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#getcurrentcolratio)
   */
  async getCurrentColRatio(address: string, vaultId: string|number): Promise<string> {
    return (await this.instance.getCurrentColRatio(address, vaultId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/BorrowingCore/#totalStcBackedByCol)
   */
  async getTotalStcBackedByCol(colKey: string): Promise<string> {
    return (await this.instance.totalStcBackedByCol(colKey)).toString()
  }
}
