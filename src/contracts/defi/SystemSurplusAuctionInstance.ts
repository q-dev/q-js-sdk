import type { ContractTransaction } from 'ethers'

import { SystemSurplusAuction } from '../../ethers-contracts/SystemSurplusAuction'
import { SystemContractInstance } from '../SystemContractInstance'
import { AuctionStatus, QNonPayableTx, QPayableTx, SystemSurplusAuctionInfo, SignerOrProvider } from '../../types'
import { UnitConverter } from '../../utils/unit-converter'

const c = new UnitConverter()

/**
 * System surplus auction instance to interact with System surplus auction contract.
 * See [onchain documentation](@system-contracts-repo/@network/SystemSurplusAuction/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.systemSurplusAuction}
 */
export class SystemSurplusAuctionInstance extends SystemContractInstance<SystemSurplusAuction> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'SystemSurplusAuction.json', address)
  }

  /**
   * Get all auctions from block diapason
   * @param fromBlock first block from which we get all auctions
   * @param toBlock last block from which we get all auctions
   * @returns auctions
   */
  async getAuctions(fromBlock: string | number = 0, toBlock: string | number = 'latest'): Promise<string[]> {
    const auctionEvents = await this.instance.queryFilter(
      this.instance.filters.AuctionStarted(),
      fromBlock,
      toBlock
    )
    const auctionIds = auctionEvents.map((evt) => evt.args._auctionId.toString())

    return auctionIds
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemSurplusAuction/#auctions)
   */
  async getAuctionInfo(auctionId: string|number): Promise<SystemSurplusAuctionInfo> {
    const rawInfo = await this.instance.auctions(auctionId)
    const endTime = c.fromTimestamp(rawInfo.endTime.toString())
    const status = rawInfo.isExecuted ? AuctionStatus.CLOSED : AuctionStatus.ACTIVE

    return {
      status,
      bidder: rawInfo.bidder,
      highestBid: rawInfo.highestBid.toString(),
      endTime,
      lot: rawInfo.lot.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemSurplusAuction/#startauction)
   */
  async startAuction(txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('startAuction', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemSurplusAuction/#bid)
   */
  async bid(auctionId: string|number, txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('bid', [auctionId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemSurplusAuction/#execute)
   */
  async execute(auctionId: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('execute', [auctionId], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemSurplusAuction/#getraisingbid)
   */
  async getRaisingBid(auctionId: string|number): Promise<string> {
    return (await this.instance.getRaisingBid(auctionId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemSurplusAuction/#auctionscount)
   */
  async auctionsCount(): Promise<string> {
    return (await this.instance.auctionsCount()).toString()
  }
}
