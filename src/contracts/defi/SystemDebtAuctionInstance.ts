import type { ContractTransaction } from 'ethers'

import { SystemDebtAuction } from '../../ethers-contracts/SystemDebtAuction'
import { SystemContractInstance } from '../SystemContractInstance'
import { AuctionStatus, QNonPayableTx, SystemDebtAuctionInfo, SignerOrProvider } from '../../types'
import { UnitConverter } from '../../utils/unit-converter'

const c = new UnitConverter()

/**
 * System debt auction instance to interact with System debt auction contract.
 * See [onchain documentation](@system-contracts-repo/@network/SystemDebtAuction/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.systemDebtAuction}
 */
export class SystemDebtAuctionInstance extends SystemContractInstance<SystemDebtAuction> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'SystemDebtAuction.json', address)
  }

  /**
   * Get all auctions from block diapason
   * @param fromBlock first block from which we get all auctions
   * @param toBlock last block from which we get all auctions
   * @returns auctions
   */
  async getAuctions(fromBlock: string | number = 0, toBlock: string | number = 'latest'): Promise<string[]> {
    const auctionEvents = await this.instance.queryFilter(
      this.instance.filters.AuctionStarted(),
      fromBlock,
      toBlock
    )
    const auctionIds = auctionEvents.map((evt) => evt.args._auctionId.toString())

    return auctionIds
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#auctions)
   */
  async getAuctionInfo(auctionId: string|number): Promise<SystemDebtAuctionInfo> {
    const rawInfo = await this.instance.auctions(auctionId)
    const endTime = c.fromTimestamp(rawInfo.endTime.toString())

    return {
      status: rawInfo.status.toString() as AuctionStatus,
      bidder: rawInfo.bidder,
      highestBid: rawInfo.highestBid.toString(),
      endTime,
      lot: rawInfo.reserveLot.toString()
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#startauction)
   */
  async startAuction(bid: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('startAuction', [bid], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#bid)
   */
  async bid(bid: string|number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('bid', [bid], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#execute)
   */
  async execute(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('execute', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#getraisingbid)
   */
  async getRaisingBid(auctionId?: string|number): Promise<string> {
    if (!auctionId)
      auctionId = await this.currentAuctionId()
    return (await this.instance.getRaisingBid(auctionId)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#currentauctionid)
   */
  async currentAuctionId(): Promise<string> {
    return (await this.instance.currentAuctionId()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemDebtAuction/#hasactiveauction)
   */
  async hasActiveAuction(): Promise<boolean> {
    return this.instance.hasActiveAuction()
  }
}
