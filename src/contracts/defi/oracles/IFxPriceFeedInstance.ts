import { SignerOrProvider } from '../../../types'

import { IFxPriceFeed } from '../../../ethers-contracts/IFxPriceFeed'
import { SystemContractInstance } from '../../SystemContractInstance'

/**
 * IFxPrice instance to interact with IFxPrice contract.
 * See [onchain documentation](@system-contracts-repo/@network/IFxPriceFeed/) for more details.
 */
export class IFxPriceFeedInstance extends SystemContractInstance<IFxPriceFeed> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'IFxPriceFeed.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#exchangerate)
   */
  async exchangeRate(): Promise<string> {
    return (await this.instance.exchangeRate()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#pair)
   */
  async pair(): Promise<string> {
    return this.instance.pair()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#basetokenaddr)
   */
  async baseTokenAddr(): Promise<string> {
    return this.instance.baseTokenAddr()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#quotetokenaddr)
   */
  async quoteTokenAddr(): Promise<string> {
    return this.instance.quoteTokenAddr()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#decimalplaces)
   */
  async decimalPlaces(): Promise<string> {
    return (await this.instance.decimalPlaces()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#updatetime)
   */
  async updateTime(): Promise<string> {
    return (await this.instance.updateTime()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#getmaintainers)
   */
  async getMaintainers(): Promise<string[]> {
    return this.instance.getMaintainers()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/IFxPriceFeed/#pricingtime)
   */
  async pricingTime(): Promise<string> {
    return (await this.instance.pricingTime()).toString()
  }
}
