import type { ContractTransaction } from 'ethers'

import { Saving } from '../../ethers-contracts/Saving'
import { SystemContractInstance } from '../SystemContractInstance'
import { QNonPayableTx, SavingBalanceDetails, SignerOrProvider } from '../../types'
import { CompoundRateKeeperInstance } from '../common/CompoundRateKeeperInstance'

/**
 * Saving instance to interact with Saving contract.
 * See [onchain documentation](@system-contracts-repo/@network/Saving/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.saving}
 */
export class SavingInstance extends SystemContractInstance<Saving> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'Saving.json', address)
  }

  /** @deprecated use getCompoundRateKeeper */
  async compoundRateKeeper(): Promise<string> {
    return this.instance.compoundRateKeeper()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Saving/#getcompoundratekeeper)
   */
  async getCompoundRateKeeper(): Promise<CompoundRateKeeperInstance> {
    const compoundRateKeeperAddress = await this.instance.compoundRateKeeper()
    return new CompoundRateKeeperInstance(this.adapter.signerOrProvider, compoundRateKeeperAddress)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Saving/#normalizedcapitals)
   */
  async normalizedCapitals(address: string): Promise<string> {
    return (await this.instance.normalizedCapitals(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Saving/#getbalancedetails)
   */
  async getBalanceDetails(address: string): Promise<SavingBalanceDetails> {
    const balanceDe = await this.instance.getBalanceDetails({ from: address })
    return {
      currentBalance: balanceDe.currentBalance.toString(),
      normalizedBalance: balanceDe.normalizedBalance.toString(),
      compoundRate: balanceDe.compoundRate.toString(),
      lastUpdateOfCompoundRate: balanceDe.lastUpdateOfCompoundRate.toString(),
      interestRate: balanceDe.interestRate.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Saving/#updatecompoundrate)
   */
  async updateCompoundRate(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('updateCompoundRate', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Saving/#deposit)
   */
  async deposit(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('deposit', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Saving/#withdraw)
   */
  async withdraw(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [amount], txOptions)
  }
}
