import type { ContractTransaction } from 'ethers'

import { SystemContractInstance } from '../SystemContractInstance'
import { TokenBridgeAdminProxy } from '../../ethers-contracts/TokenBridgeAdminProxy'
import { QNonPayableTx, SignerOrProvider } from '../../types'


/**
 * Token Bridge Admin Proxy Instance to interact with Token Bridge Admin Proxy contract.
 * See [onchain documentation](@system-contracts-repo/@network/TokenBridgeAdminProxy/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.tokenBridgeAdminProxy}
 */
export class TokenBridgeAdminProxyInstance extends SystemContractInstance<TokenBridgeAdminProxy> {
  public static readonly registryKey = 'defi.tokenBridgeAdminProxy'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'TokenBridgeAdminProxy.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/TokenBridgeAdminProxy/#owner)
   */
  async owner(): Promise<string> {
    return this.instance.owner()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/TokenBridgeAdminProxy/#updatetokenbridgevalidators)
   */
  async updateTokenbridgeValidators(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('updateTokenbridgeValidators', [], txOptions)
  }
}
