import type { ContractTransaction, BigNumberish } from 'ethers'

import { ERC20 } from '../ethers-contracts/ERC20'
import { QVault } from '../ethers-contracts/QVault'
import { SystemContractInstance } from './SystemContractInstance'
import { QNonPayableTx } from '../types'

/**
 * ERC20 helper instance to interact with ERC20 contracts.
 * See [onchain documentation](@system-contracts-repo/@network/ERC20/) for more details.
 */
export class ERC20HelperInstance<T extends ERC20 | QVault> extends SystemContractInstance<T> {
  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#name)
   */
  async name(): Promise<string> {
    return this.instance.name()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#symbol)
   */
  async symbol(): Promise<string> {
    return this.instance.symbol()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#decimals)
   */
  async decimals(): Promise<string> {
    return (await this.instance.decimals()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#totalsupply)
   */
  async totalSupply(): Promise<string> {
    return (await this.instance.totalSupply()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#balanceof)
   */
  async balanceOf(userAddress: string): Promise<string> {
    return (await this.instance.balanceOf(userAddress)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#transfer)
   */
  async transfer(receiver: string, amount: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'transfer', ERC20>('transfer', [receiver, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#approve)
   */
  async approve(spender: string, amount: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'approve', ERC20>('approve', [spender, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#transferfrom)
   */
  async transferFrom(owner: string, receiver: string, amount: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'transferFrom', ERC20>('transferFrom', [owner, receiver, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#allowance)
   */
  async allowance(tokenOwner: string, spender: string): Promise<string> {
    return (await this.instance.allowance(tokenOwner, spender)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#increaseallowance)
   */
  async increaseAllowance(spender: string, addedValue: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'increaseAllowance', ERC20>('increaseAllowance', [spender, addedValue], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ERC20/#decreaseallowance)
   */
  async decreaseAllowance(spender: string, subtractedValue: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'decreaseAllowance', ERC20>('decreaseAllowance', [spender, subtractedValue], txOptions)
  }
}
