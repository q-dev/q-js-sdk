import { BaseContract } from 'ethers'
import { getAbi } from '../abi/AbiImporter'
import { BaseContractInstance } from './BaseContractInstance'
import { SignerOrProvider } from '../types'

export class SystemContractInstance<T extends BaseContract> extends BaseContractInstance<T> {
  /**
   * Constructor
   * @param provider ethers provider instance
   * @param abiFile abi json file
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abiFile: string, public readonly address: string) {
    const abi = getAbi(abiFile)
    super(signerOrProvider, abi, address)
  }
}