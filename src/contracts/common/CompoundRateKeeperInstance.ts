import { SignerOrProvider } from '../../types'
import { CompoundRateKeeper } from '../../ethers-contracts/CompoundRateKeeper'
import { SystemContractInstance } from '../SystemContractInstance'

/**
 * Compound rate keeper instance to interact with Compound rate keeper contract.
 * See [onchain documentation](@system-contracts-repo/@network/CompoundRateKeeper/) for more details.
 */
export class CompoundRateKeeperInstance extends SystemContractInstance<CompoundRateKeeper> {
  public static readonly abi = 'CompoundRateKeeper.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, CompoundRateKeeperInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/CompoundRateKeeper/#getcurrentrate)
   */
  async getCurrentRate(): Promise<string> {
    return (await this.instance.getCurrentRate()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/CompoundRateKeeper/#getlastupdate)
   */
  async getLastUpdate(): Promise<string> {
    return (await this.instance.getLastUpdate()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/CompoundRateKeeper/#normalizeamount)
   */
  async normalizeAmount(targetAmount: string | number): Promise<string> {
    return (await this.instance.normalizeAmount(targetAmount)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/CompoundRateKeeper/#denormalizeamount)
   */
  async denormalizeAmount(normalizedAmount: string | number): Promise<string> {
    return (await this.instance.denormalizeAmount(normalizedAmount)).toString()
  }
}