import type { ContractTransaction, BigNumberish } from 'ethers'

import { BaseContractInstance } from './BaseContractInstance'

import { QNonPayableTx } from '../types'
import { ProposalStats, ProposalStatus } from '../types'

import { ARootNodeApprovalVoting } from '../ethers-contracts'

/**
 * Voting interface to interact with Voting implementation contracts.
 * See [onchain documentation](@system-contracts-repo/@network/IVoting/) for more details.
 */
export abstract class BaseRootNodeApprovalVoting<T extends ARootNodeApprovalVoting> extends BaseContractInstance<T> {
  async getStatus(proposalId: BigNumberish): Promise<ProposalStatus> {
    const status = await this.instance.getStatus(proposalId)
    switch (status) {
      case 2:
        return ProposalStatus.EXPIRED
      case 3:
        return ProposalStatus.EXECUTED
      default:
        return status.toString() as ProposalStatus
    }
  }

  async getProposalStats(proposalId: BigNumberish): Promise<ProposalStats> {
    const _stats = await this.instance.getProposalStats(proposalId)
    return {
      requiredMajority: _stats.requiredMajority.toString(),
      currentMajority: _stats.currentMajority.toString()
    }
  }

  /**
   * @deprecated
   */
  async aprove(proposalId: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    console.warn('aprove() is deprecated, use approve() instead')
    return this.approve(proposalId, txOptions)
  }

  async approve(proposalId: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'approve', ARootNodeApprovalVoting>('approve', [proposalId], txOptions)
  }
}
