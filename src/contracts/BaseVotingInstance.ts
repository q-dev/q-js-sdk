import type { ContractTransaction } from 'ethers'

import { QNonPayableTx } from '..'
import { ProposalStatus, ProposalWithBaseInfo, ProposalWithStatus, VotingStats } from '../types'
import { IVoting } from '../ethers-contracts/IVoting'
import { BaseContractInstance } from './BaseContractInstance'

/**
 * Voting interface to interact with Voting implementation contracts.
 * See [onchain documentation](@system-contracts-repo/@network/IVoting/) for more details.
 */
export abstract class BaseVotingInstance<T extends IVoting, P extends ProposalWithBaseInfo> extends BaseContractInstance<T> {
  abstract hasUserVoted(proposalId: number | string, address: string): Promise<boolean>

  /**
   * Get all proposals in specific block diapason
   * @param fromBlock start block from which we get proposals
   * @param toBlock end block from which we get proposals
   * @returns proposals
   */
  async getProposalIds(fromBlock: string | number = 0, toBlock: string | number = 'latest'): Promise<string[]> {
    const proposalEvents = await this.instance.queryFilter(
      this.instance.filters.ProposalCreated(),
      fromBlock,
      toBlock
    )
    const proposalIds = proposalEvents.map((evt) => evt.args._id.toString())

    return proposalIds
  }

  abstract getProposal(proposalId: string | number): Promise<P>

  abstract proposalCount(): Promise<string>

  /**
   * Get proposal information
   * @param proposalId proposal id
   * @returns information about proposal
   */
  async getProposalWithStatus(proposalId: string | number): Promise<P & ProposalWithStatus> {
    const p = await this.getProposal(proposalId) as P
    const status = await this.getStatus(proposalId)
    return { id: proposalId.toString(), ...p, status }
  }

  /**
   * Get proposals by ids
   * @param proposalIds proposal ids
   * @returns proposals
   */
  async getProposals(...proposalIds: string[]): Promise<(P & ProposalWithStatus)[] > {
    if (!proposalIds || proposalIds.length==0) {
      proposalIds = await this.getProposalIds()
    }

    const promises = proposalIds.map(async id =>(await this.getProposalWithStatus(id)) as P & ProposalWithStatus)
    return Promise.all(promises)
  }

  /**
   * Get proposal statistics
   * @param proposalId proposal id
   * @returns proposal statistics
   */
  async getProposalStats(proposalId: string): Promise<VotingStats> {
    const proposalStats = await this.instance.getProposalStats(proposalId)
    return {
      requiredQuorum: proposalStats.requiredQuorum.toString(),
      currentQuorum: proposalStats.currentQuorum.toString(),
      requiredMajority: proposalStats.requiredMajority.toString(),
      currentMajority: proposalStats.currentMajority.toString(),
      currentVetoPercentage: proposalStats.currentVetoPercentage.toString(),
    }
  }

  /**
   * Get proposal status
   * @param proposalId proposal id
   * @returns status of proposal
   */
  async getStatus(proposalId: string | number): Promise<ProposalStatus> {
    const statusMethod = this.instance['getStatus']
    if (!statusMethod) throw Error('status not supported on this kind of voting')

    return (await statusMethod(proposalId)).toString()
  }

  mapStatus(rawStatus: ProposalStatus): string {
    switch (rawStatus) {
      case ProposalStatus.NONE: return 'NONE'
      case ProposalStatus.PENDING: return 'PENDING'
      case ProposalStatus.REJECTED: return 'REJECTED'
      case ProposalStatus.ACCEPTED: return 'ACCEPTED'
      case ProposalStatus.PASSED: return 'PASSED'
      case ProposalStatus.EXECUTED: return 'EXECUTED'
      case ProposalStatus.OBSOLETE: return 'OBSOLETE'
      case ProposalStatus.EXPIRED: return 'EXPIRED'
      default: throw new Error(`cannot convert unknown proposal status ${rawStatus}`)
    }
  }

  /**
   * Vote for proposal
   * @param proposalId current proposal id
   * @param txOptions transaction options
   * @returns transaction receipt
   */
  async voteFor(proposalId: string | number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'voteFor', IVoting>('voteFor', [proposalId], txOptions)
  }

  /**
   * Vote against proposal
   * @param proposalId current proposal id
   * @param txOptions transaction options
   * @returns transaction receipt
   */
  async voteAgainst(proposalId: string | number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'voteAgainst', IVoting>('voteAgainst', [proposalId], txOptions)
  }

  /**
   * execute proposal
   * @param proposalId current proposal id
   * @param txOptions transaction options
   * @returns transaction receipt
   */
  async execute(proposalId: string | number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'execute', IVoting>('execute', [proposalId], txOptions)
  }
}