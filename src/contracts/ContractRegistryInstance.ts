import { BytesLike, ContractTransaction } from 'ethers'

import { QNonPayableTx, SystemContractWithQBalance } from '..'
import { ContractRegistry } from '../ethers-contracts'
import { SystemContractInstance } from './SystemContractInstance'
import { DefaultAllocationProxyInstance } from './tokeneconomics/DefaultAllocationProxyInstance'
import { RootNodeRewardProxyInstance } from './tokeneconomics/RootNodeRewardProxyInstance'
import { ValidationRewardProxyInstance } from './tokeneconomics/ValidationRewardProxyInstance'
import { ValidationRewardPoolsInstance } from './tokeneconomics/ValidationRewardPoolsInstance'
import { RootNodesInstance } from './governance/rootNodes/RootNodesInstance'
import { RootNodesMembershipVotingInstance } from './governance/rootNodes/RootNodesMembershipVotingInstance'
import { RootNodesSlashingVotingInstance } from './governance/rootNodes/RootNodesSlashingVotingInstance'
import { ValidatorsInstance } from './governance/validators/ValidatorsInstance'
import { ValidatorsSlashingVotingInstance } from './governance/validators/ValidatorsSlashingVotingInstance'
import { ConstitutionInstance } from './governance/constitution/ConstitutionInstance'
import { VotingWeightProxyInstance } from './governance/VotingWeightProxyInstance'
import { EPDRParametersInstance } from './governance/experts/EPDRParametersInstance'
import { EPDRMembershipInstance } from './governance/experts/EPDRMembershipInstance'
import { EPDRMembershipVotingInstance } from './governance/experts/EPDRMembershipVotingInstance'
import { EPDRParametersVotingInstance } from './governance/experts/EPDRParametersVotingInstance'
import { EPRSParametersInstance } from './governance/experts/EPRSParametersInstance'
import { EPRSMembershipInstance } from './governance/experts/EPRSMembershipInstance'
import { EPRSMembershipVotingInstance } from './governance/experts/EPRSMembershipVotingInstance'
import { EPRSParametersVotingInstance } from './governance/experts/EPRSParametersVotingInstance'
import { EPQFIParametersInstance } from './governance/experts/EPQFIParametersInstance'
import { EPQFIMembershipInstance } from './governance/experts/EPQFIMembershipInstance'
import { EPQFIMembershipVotingInstance } from './governance/experts/EPQFIMembershipVotingInstance'
import { QVaultInstance } from './tokeneconomics/QVaultInstance'
import { SystemReserveInstance } from './tokeneconomics/SystemReserveInstance'
import { BorrowingCoreInstance } from './defi/BorrowingCoreInstance'
import { SavingInstance } from './defi/SavingInstance'
import { ERC20Instance } from './defi/token/ERC20Instance'
import { SystemBalanceInstance } from './defi/SystemBalanceInstance'
import { LiquidationAuctionInstance } from './defi/LiquidationAuctionInstance'
import { SystemSurplusAuctionInstance } from './defi/SystemSurplusAuctionInstance'
import { SystemDebtAuctionInstance } from './defi/SystemDebtAuctionInstance'
import { TokenBridgeAdminProxyInstance } from './defi/TokenBridgeAdminProxyInstance'
import { ConstitutionVotingInstance } from './governance/constitution/ConstitutionVotingInstance'
import { EPQFIParametersVotingInstance } from './governance/experts/EPQFIParametersVotingInstance'
import { VestingInstance } from './tokeneconomics/VestingInstance'
import { EmergencyUpdateVotingInstance } from './governance/EmergencyUpdateVotingInstance'
import { GeneralUpdateVotingInstance } from './governance/GeneralUpdateVotingInstance'
import { ValidatorSlashingEscrowInstance } from './governance/validators/ValidatorSlashingEscrowInstance'
import { RootNodeSlashingEscrowInstance } from './governance/rootNodes/RootNodeSlashingEscrowInstance'
import { PushPaymentsInstance } from './tokeneconomics/PushPaymentsInstance'
import { ContractRegistryUpgradeVotingInstance } from './governance/ContractRegistryUpgradeVoting'
import { ContractRegistryAddressVotingInstance } from './governance/ContractRegistryAddressVoting'
import { AccountAliasesInstance } from './governance/AccountAliases'
import { WithdrawAddressesInstance } from './tokeneconomics/WithdrawAddresses'

import { SignerOrProvider } from '../types'

import { PromiseOrValue } from '../ethers-contracts/common'
import { GenericContractRegistryVotingInstance } from './governance/GenericContractRegistryVoting'

const defaultAddress = '0xc3E589056Ece16BCB88c6f9318e9a7343b663522'

/**
 * Contract registry instance to interact with Contract registry contract.
 * See [onchain documentation](@system-contracts-repo/@network/ContractRegistry/) for more details.
 */
export class ContractRegistryInstance extends SystemContractInstance<ContractRegistry> {
  constructor(signerOrProvider: SignerOrProvider, address?: string) {
    super(signerOrProvider, 'ContractRegistry.json', address || defaultAddress)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ContractRegistry/#getmaintainers)
   */
  async getMaintainers(): Promise<string[]> {
    return this.instance.getMaintainers()
  }

  async multicall(data: PromiseOrValue<BytesLike>[], txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('multicall', [data], txOptions)
  }

  /**
   * Retrieve contract balances
   * @returns contracts with balance and address
   */
  async getSystemContractsAndBalances(): Promise<SystemContractWithQBalance[]> {
    const systemContractEntries = await this.instance.getContracts()
    const balances = await this.adapter.getBalances(systemContractEntries.map(e => e[1]))

    return systemContractEntries.map((e, i) => {
      return {
        ...balances[i],
        registryKey: e.key,
      }
    })
  }

  /**
   * Main method to create instance.
   * @param t constructor
   * @param registryKey contract registry key
   * @returns contract instance
   */
  async createInstance<T>(t: new (signerOrProvider: SignerOrProvider, address: string) => T, registryKey?: string): Promise<T> {
    const address = await this.instance.mustGetAddress(registryKey)
    // console.log(`creating instance for registryKey '${registryKey}' at ${address}`)
    return new t(this.adapter.signerOrProvider, address)
  }

  async rootNodes(): Promise<RootNodesInstance> {
    const roots = await this.createInstance(RootNodesInstance, RootNodesInstance.registryKey)
    roots.votingWeightProxy = await this.votingWeightProxy()
    return roots
  }

  async rootNodesMembershipVoting(): Promise<RootNodesMembershipVotingInstance> {
    const rootsMV = await this.createInstance(RootNodesMembershipVotingInstance, RootNodesMembershipVotingInstance.registryKey)
    rootsMV.votingWeightProxy = await this.votingWeightProxy()
    return rootsMV
  }

  async rootNodesSlashingVoting(): Promise<RootNodesSlashingVotingInstance> {
    const rootsSV = await this.createInstance(RootNodesSlashingVotingInstance, RootNodesSlashingVotingInstance.registryKey)
    rootsSV.votingWeightProxy = await this.votingWeightProxy()
    return rootsSV
  }

  async validators(): Promise<ValidatorsInstance> {
    const validators = await this.createInstance(ValidatorsInstance, ValidatorsInstance.registryKey)
    validators.votingWeightProxy = await this.votingWeightProxy()
    return validators
  }

  async validatorsSlashingVoting(): Promise<ValidatorsSlashingVotingInstance> {
    return this.createInstance(ValidatorsSlashingVotingInstance, ValidatorsSlashingVotingInstance.registryKey)
  }

  async vesting(): Promise<VestingInstance> {
    return this.createInstance(VestingInstance, VestingInstance.registryKey)
  }

  async constitution(): Promise<ConstitutionInstance> {
    return this.createInstance(ConstitutionInstance, ConstitutionInstance.registryKey)
  }

  async constitutionVoting(): Promise<ConstitutionVotingInstance> {
    const constitutionVoting = await this.createInstance(ConstitutionVotingInstance, ConstitutionVotingInstance.registryKey)
    constitutionVoting.votingWeightProxy = await this.votingWeightProxy()
    return constitutionVoting
  }

  async votingWeightProxy(): Promise<VotingWeightProxyInstance> {
    return this.createInstance(VotingWeightProxyInstance, VotingWeightProxyInstance.registryKey)
  }

  async defaultAllocationProxy(): Promise<DefaultAllocationProxyInstance> {
    return this.createInstance(DefaultAllocationProxyInstance, DefaultAllocationProxyInstance.registryKey)
  }

  async validationRewardProxy(): Promise<ValidationRewardProxyInstance> {
    return this.createInstance(ValidationRewardProxyInstance, ValidationRewardProxyInstance.registryKey)
  }

  async validationRewardPools(): Promise<ValidationRewardPoolsInstance> {
    return this.createInstance(ValidationRewardPoolsInstance, ValidationRewardPoolsInstance.registryKey)
  }

  async rootNodeRewardProxy(): Promise<RootNodeRewardProxyInstance> {
    return this.createInstance(RootNodeRewardProxyInstance, RootNodeRewardProxyInstance.registryKey)
  }

  async emergencyUpdateVoting(): Promise<EmergencyUpdateVotingInstance> {
    return this.createInstance(EmergencyUpdateVotingInstance, EmergencyUpdateVotingInstance.registryKey)
  }

  async generalUpdateVoting(): Promise<GeneralUpdateVotingInstance> {
    const generalUpdateVoting = await this.createInstance(GeneralUpdateVotingInstance, GeneralUpdateVotingInstance.registryKey)
    generalUpdateVoting.votingWeightProxy = await this.votingWeightProxy()
    return generalUpdateVoting
  }

  /** @deprecated use qVault instead */
  async piggyBank(): Promise<QVaultInstance> {
    return this.createInstance(QVaultInstance, QVaultInstance.registryKey)
  }

  async qVault(): Promise<QVaultInstance> {
    const qVault = await this.createInstance(QVaultInstance, QVaultInstance.registryKey)
    qVault.votingWeightProxy = await this.votingWeightProxy()
    return qVault
  }

  async systemReserve(): Promise<SystemReserveInstance> {
    return this.createInstance(SystemReserveInstance, SystemReserveInstance.registryKey)
  }

  async epqfiParameters(): Promise<EPQFIParametersInstance> {
    return this.createInstance(EPQFIParametersInstance, EPQFIParametersInstance.registryKey)
  }

  async epqfiParametersVoting(): Promise<EPQFIParametersVotingInstance> {
    return this.createInstance(EPQFIParametersVotingInstance, EPQFIParametersVotingInstance.registryKey)
  }

  async epqfiMembershipVoting(): Promise<EPQFIMembershipVotingInstance> {
    return this.createInstance(EPQFIMembershipVotingInstance, EPQFIMembershipVotingInstance.registryKey)
  }

  async epqfiMembership(): Promise <EPQFIMembershipInstance> {
    return this.createInstance(EPQFIMembershipInstance, EPQFIMembershipInstance.registryKey)
  }

  async epdrParameters(): Promise<EPDRParametersInstance> {
    return this.createInstance(EPDRParametersInstance, EPDRParametersInstance.registryKey)
  }

  async epdrParametersVoting(): Promise<EPDRParametersVotingInstance> {
    return this.createInstance(EPDRParametersVotingInstance, EPDRParametersVotingInstance.registryKey)
  }

  async epdrMembership(): Promise<EPDRMembershipInstance> {
    return this.createInstance(EPDRMembershipInstance, EPDRMembershipInstance.registryKey)
  }

  async epdrMembershipVoting(): Promise<EPDRMembershipVotingInstance> {
    return this.createInstance(EPDRMembershipVotingInstance, EPDRMembershipVotingInstance.registryKey)
  }

  async eprsParameters(): Promise<EPRSParametersInstance> {
    return this.createInstance(EPRSParametersInstance, EPRSParametersInstance.registryKey)
  }

  async eprsParametersVoting(): Promise<EPRSParametersVotingInstance> {
    return this.createInstance(EPRSParametersVotingInstance, EPRSParametersVotingInstance.registryKey)
  }

  async eprsMembership(): Promise<EPRSMembershipInstance> {
    return this.createInstance(EPRSMembershipInstance, EPRSMembershipInstance.registryKey)
  }

  async eprsMembershipVoting(): Promise<EPRSMembershipVotingInstance> {
    return this.createInstance(EPRSMembershipVotingInstance, EPRSMembershipVotingInstance.registryKey)
  }

  async borrowingCore(stc: string): Promise<BorrowingCoreInstance> {
    return this.createInstance(BorrowingCoreInstance, `defi.${stc}.borrowing`)
  }

  async saving(stc: string): Promise<SavingInstance> {
    return this.createInstance(SavingInstance, `defi.${stc}.saving`)
  }

  async stableCoin(stc: string): Promise<ERC20Instance> {
    return this.createInstance(ERC20Instance, `defi.${stc}.coin`)
  }

  async systemBalance(stc: string): Promise<SystemBalanceInstance> {
    return this.createInstance(SystemBalanceInstance, `defi.${stc}.systemBalance`)
  }

  async liquidationAuction(stc: string): Promise<LiquidationAuctionInstance> {
    return this.createInstance(LiquidationAuctionInstance, `defi.${stc}.liquidationAuction`)
  }

  async systemSurplusAuction(stc: string): Promise<SystemSurplusAuctionInstance> {
    return this.createInstance(SystemSurplusAuctionInstance, `defi.${stc}.systemSurplusAuction`)
  }

  async systemDebtAuction(stc: string): Promise<SystemDebtAuctionInstance> {
    return this.createInstance(SystemDebtAuctionInstance, `defi.${stc}.systemDebtAuction`)
  }

  async validatorSlashingEscrow(): Promise<ValidatorSlashingEscrowInstance> {
    return this.createInstance(ValidatorSlashingEscrowInstance, ValidatorSlashingEscrowInstance.registryKey)
  }

  async rootNodeSlashingEscrow(): Promise<RootNodeSlashingEscrowInstance> {
    return this.createInstance(RootNodeSlashingEscrowInstance, RootNodeSlashingEscrowInstance.registryKey)
  }

  async pushPayments(): Promise<PushPaymentsInstance> {
    return this.createInstance(PushPaymentsInstance, PushPaymentsInstance.registryKey)
  }

  async tokenBridgeAdminProxy(): Promise<TokenBridgeAdminProxyInstance> {
    return this.createInstance(TokenBridgeAdminProxyInstance, TokenBridgeAdminProxyInstance.registryKey)
  }

  async upgradeVoting(): Promise<ContractRegistryUpgradeVotingInstance> {
    return this.createInstance(ContractRegistryUpgradeVotingInstance, ContractRegistryUpgradeVotingInstance.registryKey)
  }

  async addressVoting(): Promise<ContractRegistryAddressVotingInstance> {
    return this.createInstance(ContractRegistryAddressVotingInstance, ContractRegistryAddressVotingInstance.registryKey)
  }

  async accountAliases(): Promise<AccountAliasesInstance> {
    return this.createInstance(AccountAliasesInstance, AccountAliasesInstance.registryKey)
  }

  async withdrawAddresses(): Promise<WithdrawAddressesInstance> {
    return this.createInstance(WithdrawAddressesInstance, WithdrawAddressesInstance.registryKey)
  }

  async genericContractRegistryVoting(): Promise<GenericContractRegistryVotingInstance> {
    return this.createInstance(GenericContractRegistryVotingInstance, GenericContractRegistryVotingInstance.registryKey)
  }
}
