import { getAbi } from '../abi/AbiImporter'
import { ProposalWithBaseInfo, SignerOrProvider } from '../types'
import { IVetoable } from '../ethers-contracts/IVetoable'
import { IVoting } from '../ethers-contracts/IVoting'
import { BaseVotingVetoableInstance } from './BaseVotingVetoableInstance'

export abstract class SystemVotingVetoableInstance<T extends IVoting & IVetoable, P extends ProposalWithBaseInfo> extends BaseVotingVetoableInstance<T, P> {
  /**
   * Constructor
   * @param provider ethers provider instance
   * @param abiFile abi json file
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abiFile: string, public readonly address: string) {
    const abi = getAbi(abiFile)
    super(signerOrProvider, abi, address)
  }
}