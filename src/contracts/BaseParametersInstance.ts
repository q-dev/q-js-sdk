import { KeyValuePair } from '../types'
import { AParameters } from '../ethers-contracts/AParameters'
import { BaseContractInstance } from './BaseContractInstance'

export type ParameterType = 'Uint' | 'String' | 'Bool' | 'Addr' | 'Bytes32'

/**
 * [Abstract Parameters contract](@system-contracts-repo/@network/AParameters/)
 */
/**
 * Abstract Parameters instance to interact with Constitution, EPDR and EPQFI Parameters contracts.
 * See [onchain documentation](@system-contracts-repo/@network/AParameters/) for more details.
 */
export class BaseParametersInstance<T extends AParameters> extends BaseContractInstance<T> {
  async getUintKeys(): Promise<string[]> {
    return this.getKeys('Uint')
  }

  /**
   * Get uint value
   * @param key key of value
   * @returns value
   */
  async getUint(key: string): Promise<string> {
    return this.getParameter('Uint', key)
  }

  /**
   * Get address keys
   * @returns keys
   */
  async getAddrKeys(): Promise<string[]> {
    return this.getKeys('Addr')
  }

  /**
   * Get address value
   * @param key key of value
   * @returns value
   */
  async getAddr(key: string): Promise<string> {
    return this.getParameter('Addr', key)
  }

  /**
   * Get keys with specific type
   * @param type type of keys
   * @returns keys
   */
  async getKeys(type: ParameterType): Promise<string[]> {
    switch (type) {
      case 'Uint':
        return this.instance.getUintKeys()
      case 'String':
        return this.instance.getStringKeys()
      case 'Bool':
        return this.instance.getBoolKeys()
      case 'Addr':
        return this.instance.getAddrKeys()
      case 'Bytes32':
        return this.instance.getBytes32Keys()
    }
  }

  /**
   * Get parameter with specific type and key
   * @param type parameter type
   * @param key parameter key
   * @returns parameter
   */
  async getParameter(type: ParameterType, key: string): Promise<string> {
    switch (type) {
      case 'Uint':
        return (await this.instance.getUint(key)).toString()
      case 'String':
        return this.instance.getString(key)
      case 'Bool':
        return (await this.instance.getBool(key)).toString()
      case 'Addr':
        return this.instance.getAddr(key)
      case 'Bytes32':
        return this.instance.getBytes32(key)
    }
  }

  /**
   * Get all parameters with specific type
   * @param type specific type
   * @returns parameters (key-value array)
   */
  async getParameters(type: ParameterType): Promise<KeyValuePair<string, string>[]> {
    const keys = await this.getKeys(type)
    const valuesPromises = keys.map(key => this.getParameter(type, key))
    const values = await Promise.all(valuesPromises)


    const keyValuePairs = keys.map((key, i) => ({
      key,
      value: values[i]
    }))

    return keyValuePairs
  }
}