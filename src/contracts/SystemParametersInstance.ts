import { SignerOrProvider } from '../types'
import { getAbi } from '../abi/AbiImporter'
import { AParameters } from '../ethers-contracts/AParameters'
import { BaseParametersInstance } from './BaseParametersInstance'

export abstract class SystemParametersInstance<T extends AParameters> extends BaseParametersInstance<T> {
  /**
   * Constructor
   * @param provider ethers provider instance
   * @param abiFile abi json file
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abiFile: string, public readonly address: string) {
    const abi = getAbi(abiFile)
    super(signerOrProvider, abi, address)
  }
}