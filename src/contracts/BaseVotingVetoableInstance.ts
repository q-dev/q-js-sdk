import type { ContractTransaction } from 'ethers'

import { ProposalWithBaseInfo, QNonPayableTx } from '../types'
import { IVetoable } from '../ethers-contracts/IVetoable'
import { IVoting } from '../ethers-contracts/IVoting'
import { BaseVotingInstance } from './BaseVotingInstance'

/**
 * Vetoable interface to interact with Vetoable implementation contracts.
 * See [onchain documentation](@system-contracts-repo/@network/IVetoable/) for more details.
 */
export abstract class BaseVotingVetoableInstance<T extends IVoting & IVetoable, P extends ProposalWithBaseInfo> extends BaseVotingInstance<T, P> {
  abstract hasRootVetoed(proposalId: number | string, address: string): Promise<boolean>

  /**
   * Veto specific proposal
   * @param proposalId proposal id
   * @param txOptions transaction options
   * @returns transaction receipt
   */
  async veto(proposalId: string | number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction<'veto', IVetoable>('veto', [proposalId], txOptions)
  }

  /**
   * Get vetoes number for specific proposal
   * @param proposalId proposal id
   * @returns vetoes number
   */
  async getVetoesNumber(proposalId: string | number): Promise<string> {
    return (await this.instance.getVetosNumber(proposalId)).toString()
  }

  /**
   * Get vetoes percentage for specific proposal
   * @param proposalId proposal id
   * @returns vetoes number
   */  async getVetoesPercentage(proposalId: string | number): Promise<string> {
    return (await this.instance.getVetosPercentage(proposalId)).toString()
  }
}
