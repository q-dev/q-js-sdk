import { FnArgsWithoutOverrides, QNonPayableTx, QPayableTx, SignerOrProvider } from '../types'

import {
  BaseContract,
  Contract,
  ContractInterface,
  BigNumber,
  ContractTransaction,
  CallOverrides
} from 'ethers'
import { Web3Adapter } from '../utils/web3-adapter'
import omit from 'lodash/omit'

/**
 * Base contract instance for all instances
 */
export class BaseContractInstance<T extends BaseContract> {
  /**
   * @field contract interface
   */
  public instance: T
  protected adapter: Web3Adapter

  /**
   * @field default to estimate - 1.3 would mean 30% above estimate
   * @example BaseContractInstance.DEFAULT_GASBUFFER = 1.3
   */
  public static DEFAULT_GASBUFFER = 1

  /**
   * Constructor
   * @param signerOrProvider signer or provider
   * @param abi abi object
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abi: ContractInterface, public readonly address: string) {
    this.instance = new Contract(address, abi, signerOrProvider) as T
    this.adapter = new Web3Adapter(signerOrProvider)
  }

  async getBalance(convertToQ?: boolean): Promise<string> {
    return this.adapter.getBalance(this.address, convertToQ)
  }

  async submitTransaction<K extends keyof T, U extends BaseContract = T>(
    method: K,
    // @ts-ignore: TODO: types
    args: FnArgsWithoutOverrides<U[K]>,
    txOptions: QNonPayableTx | QPayableTx = {}
  ): Promise<ContractTransaction> {
    const populatedTxOptions = await this.populateTxOptions(txOptions)
    if (!populatedTxOptions.gasLimit) {
      const gasBuffer = Number(txOptions.gasBuffer || BaseContractInstance.DEFAULT_GASBUFFER)
      // @ts-ignore - estimateGas properties duplicate contract method properties
      const estimate = await this.instance.estimateGas[method](...args, populatedTxOptions)
      populatedTxOptions.gasLimit = BigNumber.from(estimate)
        .mul(gasBuffer * 10)
        .div(10)
    }

    // @ts-ignore: TODO: types
    return this.instance[method](...args, populatedTxOptions)
  }

  async populateTxOptions(txOptions: QPayableTx): Promise<CallOverrides> {
    const populatedTxOptions = Object.assign({}, txOptions)
    if (txOptions.qAmount) {
      if (txOptions.value) throw new Error('QPayableTx must specify either value (in wei) OR qAmount, but not both')
      populatedTxOptions.value = this.adapter.toWei(txOptions.qAmount.toString())
    }

    if (!txOptions.from) {
      populatedTxOptions.from = await this.adapter.getDefaultAccount()
    }

    return omit(populatedTxOptions, ['qAmount', 'gasBuffer'])
  }
}
