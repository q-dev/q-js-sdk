import { SignerOrProvider } from '../types'
import { getAbi } from '../abi/AbiImporter'
import { ARootNodeApprovalVoting } from '../ethers-contracts/ARootNodeApprovalVoting'
import { BaseRootNodeApprovalVoting } from './BaseRootNodeApprovalVoting'

export class SystemRootNodeApprovalVoting<T extends ARootNodeApprovalVoting> extends BaseRootNodeApprovalVoting<T> {
  /**
   * Constructor
   * @param provider ethers provider instance
   * @param abiFile abi json file
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abiFile: string, public readonly address: string) {
    const abi = getAbi(abiFile)
    super(signerOrProvider, abi, address)
  }
}