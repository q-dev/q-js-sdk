import { BigNumberish, BytesLike, ContractTransaction } from 'ethers'
import { SystemContractInstance } from '../SystemContractInstance'
import { QNonPayableTx, SignerOrProvider } from '../../types'

import { GnosisSafe } from '../../ethers-contracts'
import { PromiseOrValue } from '../../ethers-contracts/common'

export class GnosisSafeInstance extends SystemContractInstance<GnosisSafe> {
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'GnosisSafe.json', address)
  }

  async approveHash(encodedDataHash: PromiseOrValue<string>, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('approveHash', [encodedDataHash], txOptions)
  }

  async execTransaction(
    to: PromiseOrValue<string>,
    value: PromiseOrValue<BigNumberish>,
    data: PromiseOrValue<BytesLike>,
    operation: PromiseOrValue<BigNumberish>,
    safeTxGas: PromiseOrValue<BigNumberish>,
    baseGas: PromiseOrValue<BigNumberish>,
    gasPrice: PromiseOrValue<BigNumberish>,
    gasToken: PromiseOrValue<string>,
    refundReceiver: PromiseOrValue<string>,
    signatures: PromiseOrValue<BytesLike>,
    txOptions?: QNonPayableTx
  ): Promise<ContractTransaction> {
    return this.submitTransaction('execTransaction', [
      to, value, data, operation, safeTxGas, baseGas, gasPrice, gasToken, refundReceiver, signatures
    ], txOptions)
  }

  async nonce(): Promise<string> {
    return (await this.instance.nonce()).toString()
  }

  async getTransactionHash(
    to: PromiseOrValue<string>,
    value: PromiseOrValue<BigNumberish>,
    data: PromiseOrValue<BytesLike>,
    operation: PromiseOrValue<BigNumberish>,
    safeTxGas: PromiseOrValue<BigNumberish>,
    baseGas: PromiseOrValue<BigNumberish>,
    gasPrice: PromiseOrValue<BigNumberish>,
    gasToken: PromiseOrValue<string>,
    refundReceiver: PromiseOrValue<string>,
    nonce: PromiseOrValue<BigNumberish>
  ): Promise<string> {
    return (
      await this.instance.getTransactionHash(
        to, value, data, operation, safeTxGas, baseGas, gasPrice, gasToken, refundReceiver, nonce
      )
    ).toString()
  }
}
