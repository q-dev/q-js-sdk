import { SignerOrProvider } from '../types'
import { getAbi } from '../abi/AbiImporter'
import { ASlashingEscrow } from '../ethers-contracts/ASlashingEscrow'
import { BaseSlashingEscrowInstance } from './governance/BaseSlashingEscrowInstance'

export class SystemSlashingEscrowInstance<T extends ASlashingEscrow> extends BaseSlashingEscrowInstance<T> {
/**
   * Constructor
   * @param provider ethers provider instance
   * @param abiFile abi json file
   * @param address contract address
   */
  constructor(signerOrProvider: SignerOrProvider, abiFile: string, public readonly address: string) {
    const abi = getAbi(abiFile)
    super(signerOrProvider, abi, address)
  }
}