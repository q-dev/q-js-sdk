import type { ContractTransaction } from 'ethers'

import { PushPayments } from '../../ethers-contracts/PushPayments'
import { SystemContractInstance } from '../SystemContractInstance'
import { QNonPayableTx, QPayableTx, SignerOrProvider } from '../../types'

/**
 * Push payments instance to interact with Push payments contract.
 * See [onchain documentation](@system-contracts-repo/@network/PushPayments/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.pushPayments}
 */
export class PushPaymentsInstance extends SystemContractInstance<PushPayments> {
  public static readonly registryKey = 'tokeneconomics.pushPayments'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'PushPayments.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/PushPayments/#balanceof)
   */
  async balanceOf(account: string): Promise<string> {
    return (await this.instance.balanceOf(account)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/PushPayments/#safetransferto)
   */
  async safeTransferTo(account: string, txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('safeTransferTo', [account], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/PushPayments/#withdraw)
   */
  async withdraw(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [], txOptions)
  }
}
