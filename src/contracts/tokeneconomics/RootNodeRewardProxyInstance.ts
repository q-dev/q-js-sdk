import type { ContractTransaction } from 'ethers'

import { RootNodeRewardProxy } from '../../ethers-contracts/RootNodeRewardProxy'
import { QNonPayableTx, SignerOrProvider } from '../../types'
import { SystemContractInstance } from '../SystemContractInstance'

/**
 * Root node reward proxy instance to interact with Root node reward proxy contract.
 * See [onchain documentation](@system-contracts-repo/@network/RootNodeRewardProxy/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.rootNodeRewardProxy}
 */
export class RootNodeRewardProxyInstance extends SystemContractInstance<RootNodeRewardProxy> {
  public static readonly registryKey = 'tokeneconomics.rootNodeRewardProxy'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'RootNodeRewardProxy.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/RootNodeRewardProxy/#allocate)
   */
  async allocate(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('allocate', [], txOptions)
  }
}
