import type { ContractTransaction } from 'ethers'

import { WithdrawAddresses } from '../../ethers-contracts/WithdrawAddresses'
import { SystemContractInstance } from '../SystemContractInstance'
import { QNonPayableTx, WithdrawalAccount, SignerOrProvider } from '../../types'

export class WithdrawAddressesInstance extends SystemContractInstance<WithdrawAddresses> {
  public static readonly registryKey = 'tokeneconomics.withdrawAddresses'
  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'WithdrawAddresses.json', address)
  }

  async change(withdrawalAddress: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('change', [withdrawalAddress], txOptions)
  }

  async finalize(mainAddress: string, withdrawalAddress: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('finalize', [mainAddress, withdrawalAddress], txOptions)
  }

  async resolve(mainAddress: string): Promise<string> {
    return this.instance.resolve(mainAddress)
  }

  async getWithdrawalAccount(mainAddress: string): Promise<WithdrawalAccount> {
    return this.instance.map(mainAddress)
  }
}
