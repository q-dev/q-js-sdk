import type { ContractTransaction } from 'ethers'

import { QNonPayableTx, SignerOrProvider } from '../../types'
import { DefaultAllocationProxy } from '../../ethers-contracts/DefaultAllocationProxy'
import { SystemContractInstance } from '../SystemContractInstance'

/**
 * Default allocation proxy instance to interact with Default allocation proxy contract.
 * See [onchain documentation](@system-contracts-repo/@network/DefaultAllocationProxy/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.defaultAllocationProxy}
 */
export class DefaultAllocationProxyInstance extends SystemContractInstance<DefaultAllocationProxy> {
  public static readonly registryKey = 'tokeneconomics.defaultAllocationProxy'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'DefaultAllocationProxy.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/DefaultAllocationProxy/#allocate)
   */
  async allocate(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('allocate', [], txOptions)
  }
}
