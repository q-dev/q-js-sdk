import type { ContractTransaction } from 'ethers'

import { SystemReserve } from '../../ethers-contracts/SystemReserve'
import { SystemContractInstance } from '../SystemContractInstance'
import { QNonPayableTx, SignerOrProvider } from '../../types'

/**
 * System reserve instance to interact with System reserve contract.
 * See [onchain documentation](@system-contracts-repo/@network/SystemReserve/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.systemReserve}
 */
export class SystemReserveInstance extends SystemContractInstance<SystemReserve> {
  public static readonly registryKey = 'tokeneconomics.systemReserve'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'SystemReserve.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemReserve/#cooldownphase)
   */
  async coolDownPhase(): Promise<string> {
    return (await this.instance.coolDownPhase()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemReserve/#systempaused)
   */
  async systemPaused(): Promise<boolean> {
    return this.instance.systemPaused()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemReserve/#availableamount)
   */
  async availableAmount(): Promise<string> {
    return (await this.instance.availableAmount()).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemReserve/#setpausestate)
   */
  async setPauseState(state: boolean, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('setPauseState', [state], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemReserve/#withdraw)
   */
  async withdraw(amount: string | number, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/SystemReserve/#geteligiblecontractkeys)
   */
  async getEligibleContractKeys(): Promise<string[]> {
    return this.instance.getEligibleContractKeys()
  }
}
