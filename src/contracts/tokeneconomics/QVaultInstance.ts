import type { ContractTransaction, BigNumberish } from 'ethers'

import { QVault } from '../../ethers-contracts/QVault'
import {
  VotingLockInfo,
  QNonPayableTx,
  QPayableTx,
  StakeDelegationInfo,
  TimeLockEntry,
  QVaultBalanceDetails,
  BaseVotingWeightInfo,
  SignerOrProvider,
} from '../../types'
import { UnitConverter } from '../../utils/unit-converter'
import BN from 'bn.js'
import { CompoundRateKeeperInstance } from '../common/CompoundRateKeeperInstance'
import { ERC20HelperInstance } from '../ERC20HelperInstance'
import { VotingWeightProxyInstance } from '../governance/VotingWeightProxyInstance'

/**
 * QVault instance to interact with QVault contract.
 * See [onchain documentation](@system-contracts-repo/@network/QVault/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.qVault}
 */
export class QVaultInstance extends ERC20HelperInstance<QVault> {
  public static readonly registryKey = 'tokeneconomics.qVault'
  public static readonly abi = 'QVault.json'

  private c = new UnitConverter()

  /**
   * VotingWeightProxy instance. Used by local methods.
   */
  public votingWeightProxy: VotingWeightProxyInstance

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, QVaultInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#aggregatednormalizedbalance)
   */
  async aggregatedNormalizedBalance(): Promise<string> {
    return (await this.instance.aggregatedNormalizedBalance()).toString()
  }

  /** @deprecated use getCompoundRateKeeper */
  async compoundRateKeeper(): Promise<string> {
    return this.instance.compoundRateKeeper()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#getcompoundratekeeper)
   */
  async getCompoundRateKeeper(): Promise<CompoundRateKeeperInstance> {
    const compoundRateKeeperAddress = await this.instance.compoundRateKeeper()
    return new CompoundRateKeeperInstance(this.adapter.signerOrProvider, compoundRateKeeperAddress)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#deposit)
   */
  async deposit(txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('deposit', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#lock)
   */
  async lock(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('lock', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#announceunlock)
   */
  async announceUnlock(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('announceUnlock', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#unlock)
   */
  async unlock(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('unlock', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#withdraw)
   */
  async withdraw(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [amount], txOptions)
  }

  /**
   * Withdraw all available amount.
   * Use {@link QVaultInstance.getReadyToWithdrawBalance}
   * @param user account.
   * @returns Transaction receipt.
   */
  async withdrawEntireBalance(user: string): Promise<ContractTransaction> {
    return this.withdraw(await this.getReadyToWithdrawBalance(user), { from: user })
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#updatecompoundrate)
   */
  async updateCompoundRate(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('updateCompoundRate', [], txOptions)
  }

  /** @deprecated compound rate has to be updated via ValidationRewardPoolsInstance */
  async updateValidatorsCompoundRate(_validator: string, _txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    throw new Error('This has to be set via ValidationRewardPoolsInstance')
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#delegatestake)
   */
  async delegateStake(delegatedTo: string[], stakes: string[], txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('delegateStake', [delegatedTo, stakes], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#transferandcall)
   */
  async transferAndCall(to: string, value: BigNumberish, data: string | number[], txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('transferAndCall', [to, value, data], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#withdrawto)
   */
  async withdrawTo(recipient: string, amount: BigNumberish, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdrawTo', [recipient, amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#depositto)
   */
  async depositTo(recipient: string, txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('depositTo', [recipient], txOptions)
  }

  /**
   * Token amount that can be withdrawn at current moment.
   * This method takes into account all locks of QVault
   * @param user account
   * @returns Withdrawable amount
   */
  async getReadyToWithdrawBalance(user: string): Promise<string> {
    const currentTime = new BN((await this.adapter.provider.getBlock('latest')).timestamp)

    const votingWeightInfo: BaseVotingWeightInfo = await this.votingWeightProxy.getBaseVotingWeightInfo(user, currentTime.toString())
    const votingAgentLockUntil = new BN(votingWeightInfo.lockedUntil)

    const lockInfo: VotingLockInfo = await this.getLockInfo(user)
    const currentBalance = new BN(await this.balanceOf(user))
    const nonLockedAmount = currentBalance.sub(new BN(lockInfo.lockedAmount)).sub(new BN(lockInfo.pendingUnlockAmount))
    const minimumFromTimeLocks = new BN(await this.getMinimumBalance(user, currentTime.toString()))
    const minimumFromStakeDelegation = new BN(await this.getTotalDelegatedStake(user))
    const minimumBalance = BN.max(minimumFromStakeDelegation, minimumFromTimeLocks)

    let readyToWithdrawBalance = currentBalance.sub(minimumBalance)

    if (currentBalance.toString() == '0') {
      return '0'
    }

    if (readyToWithdrawBalance.lte(nonLockedAmount)) {
      return readyToWithdrawBalance.toString()
    }

    const amountToUnlock = readyToWithdrawBalance.sub(nonLockedAmount)

    if (currentTime.gte(new BN(lockInfo.pendingUnlockTime))) {
      const pendingUnlockAmount = new BN(lockInfo.pendingUnlockAmount)

      if (amountToUnlock.lte(pendingUnlockAmount)) {
        return readyToWithdrawBalance.toString()
      }

      if (currentTime.gte(new BN(lockInfo.lockedUntil)) && currentTime.gte(votingAgentLockUntil)) {
        return readyToWithdrawBalance.toString()
      }

      readyToWithdrawBalance = readyToWithdrawBalance.add(pendingUnlockAmount)
    }

    readyToWithdrawBalance = readyToWithdrawBalance.sub(amountToUnlock)

    return readyToWithdrawBalance.toString()
  }

  /**
   * Calculate total delegation stake for chosen account.
   * @param user account.
   * @returns total delegated stake.
   */
  async getTotalDelegatedStake(user: string): Promise<string> {
    const validatorInfos = await this.instance.getDelegationsList(user)
    let totalDelegatedStake = new BN(0)

    for (let i = 0; i < validatorInfos.length; i++) {
      totalDelegatedStake = totalDelegatedStake.add(new BN(validatorInfos[i].actualStake.toString()))
    }

    return totalDelegatedStake.toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#getdelegationslist)
   */
  async getDelegationsList(user: string): Promise<StakeDelegationInfo[]> {
    const validatorInfos = await this.instance.getDelegationsList(user)

    return validatorInfos.map(i => ({
      validator: i.validator,
      actualStake: i.actualStake.toString(),
      normalizedStake: i.normalizedStake.toString(),
      compoundRate: i.compoundRate.toString(),
      latestUpdateOfCompoundRate: i.latestUpdateOfCompoundRate.toString(),
      idealStake: i.idealStake.toString(),
      claimableReward: i.claimableReward.toString()
    }))
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#getnormalizedbalance)
   */
  async getNormalizedBalance(userAddress: string): Promise<string> {
    return (await this.instance.getNormalizedBalance(userAddress)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#claimstakedelegatorreward)
   */
  async claimStakeDelegatorReward(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('claimStakeDelegatorReward', [], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#depositfrompool)
   */
  async depositFromPool(txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('depositFromPool', [], txOptions)
  }

  /** @deprecated user balance has to be received via balanceOf */
  async getUserBalance(user: string): Promise<string> {
    return (await this.instance.balanceOf(user)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#getbalancedetails)
   */
  async getBalanceDetails(user: string): Promise<QVaultBalanceDetails> {
    const details = await this.instance.getBalanceDetails({ from: user })
    return {
      currentBalance: details.currentBalance.toString(),
      normalizedBalance: details.normalizedBalance.toString(),
      compoundRate: details.compoundRate.toString(),
      lastUpdateOfCompoundRate: details.lastUpdateOfCompoundRate.toString(),
      interestRate: details.interestRate.toString(),
    }
  }

  /**
   * [External documentation](@system-contracts-repo/@network/QVault/#getlockinfo)
   */
  async getLockInfo(user: string): Promise<VotingLockInfo> {
    const result = await this.instance.getLockInfo({ from: user })

    return {
      lockedAmount: result.lockedAmount.toString(),
      lockedUntil: result.lockedUntil.toString(),
      pendingUnlockAmount: result.pendingUnlockAmount.toString(),
      pendingUnlockTime: result.pendingUnlockTime.toString(),
    }
  }

  /**
   * Calculate available to delegate amount
   * @param user account
   * @returns delegatable amount
   */
  async getDelegatableAmount(user: string): Promise<string> {
    const balance = await this.balanceOf(user)
    const alreadyDelegated = this.c.toBigNumber(await this.getTotalDelegatedStake(user))
    const delegatableAmount = this.c.toBigNumber(balance).minus(alreadyDelegated)
    return this.c.fromBigNumber(delegatableAmount)
  }

  /**
   * Calculate available to lock amount
   * @param user account
   * @returns lockable amount
   */
  async getLockableAmount(user: string): Promise<string> {
    const balance = await this.balanceOf(user)
    const lockInfo = await this.getLockInfo(user)
    const alreadyLocked = this.c.toBigNumber(lockInfo.lockedAmount).plus(this.c.toBigNumber(lockInfo.pendingUnlockAmount))
    const lockableAmount = this.c.toBigNumber(balance).minus(alreadyLocked)
    return this.c.fromBigNumber(lockableAmount)
  }

  /**
   * {@link TimeLockHelperInstance.depositOnBehalfOf | Internal documentation}
   */
  async depositOnBehalfOf(account: string, start: BigNumberish, end: BigNumberish,
    txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('depositOnBehalfOf', [account, start, end], txOptions)
  }

  /**
   * {@link TimeLockHelperInstance.getTimeLocks | Internal documentation}
   */
  async getTimeLocks(account: string): Promise<TimeLockEntry[]> {
    const timelocks = await this.instance.getTimeLocks(account)
    return timelocks.map(i => ({
      amount: i.amount.toString(),
      releaseStart: i.releaseStart.toString(),
      releaseEnd: i.releaseEnd.toString()
    }))
  }

  /**
   * {@link TimeLockHelperInstance.getMinimumBalance | Internal documentation}
   */
  async getMinimumBalance(account: string, timestamp: BigNumberish): Promise<string> {
    return (await this.instance.getMinimumBalance(account, timestamp)).toString()
  }

  /**
   * {@link TimeLockHelperInstance.purgeTimeLocks | Internal documentation}
   */
  async purgeTimeLocks(account: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('purgeTimeLocks', [account], txOptions)
  }
}

/** @deprecated use QVaultInstance instead */
export type PiggyBankInstance = QVaultInstance
/** @deprecated use QVault instead */
export type PiggyBank = QVault
