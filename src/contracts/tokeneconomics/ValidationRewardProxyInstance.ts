import type { ContractTransaction } from 'ethers'

import { QNonPayableTx, SignerOrProvider } from '../../types'
import { ValidationRewardProxy } from '../../ethers-contracts/ValidationRewardProxy'
import { SystemContractInstance } from '../SystemContractInstance'

/**
 * Validation reward proxy instance to interact with Validation reward proxy contract.
 * See [onchain documentation](@system-contracts-repo/@network/ValidationRewardProxy/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.validationRewardProxy}
 */
export class ValidationRewardProxyInstance extends SystemContractInstance<ValidationRewardProxy> {
  public static readonly registryKey = 'tokeneconomics.validationRewardProxy'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'ValidationRewardProxy.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardProxy/#allocate)
   */
  async allocate(txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('allocate', [], txOptions)
  }
}
