import type { ContractTransaction } from 'ethers'

import { QNonPayableTx, QPayableTx, ValidatorPoolInfo, ValidatorProperties, SignerOrProvider } from '../../types'
import { ValidationRewardPools } from '../../ethers-contracts/ValidationRewardPools'
import { SystemContractInstance } from '../SystemContractInstance'

/**
 * Validation reward pools instance to interact with Validation reward pools contract.
 * See [onchain documentation](@system-contracts-repo/@network/ValidationRewardPools/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.validationRewardPools}
 */
export class ValidationRewardPoolsInstance extends SystemContractInstance<ValidationRewardPools> {
  public static readonly registryKey = 'tokeneconomics.validationRewardPools'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, 'ValidationRewardPools.json', address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#validatorproperties)
   */
  async validatorProperties(address: string): Promise<ValidatorProperties> {
    const properties = await this.instance.validatorsProperties(address)
    return {
      balance: properties.balance.toString(),
      reservedForClaim: properties.reservedForClaim.toString(),
      delegatorsShare: properties.delegatorsShare.toString(),
      aggregatedNormalizedStake: properties.aggregatedNormalizedStake.toString(),
      compoundRate: properties.compoundRate,
    }
  }

  /**
   * Checks whether a user has CompoundRateKeeper
   * @param address user
   * @returns CRKeeper existence for given address
   */
  async compoundRateKeeperExists(address: string): Promise<boolean> {
    const properties = await this.validatorProperties(address)
    return properties.compoundRate != this.adapter.ZERO_ADDRESS
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#setdelegatorsshare)
   */
  async setDelegatorsShare(qsv: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('setDelegatorsShare', [qsv], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#increase)
   */
  async increase(address: string, txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('increase', [address], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#updatevalidatorscompoundrate)
   */
  async updateValidatorsCompoundRate(address: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('updateValidatorsCompoundRate', [address], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#reserveadditionalfunds)
   */
  async reserveAdditionalFunds(validator: string, txOptions?: QPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('reserveAdditionalFunds', [validator], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#getcompoundrate)
   */
  async getCompoundRate(address: string): Promise<string> {
    return (await this.instance.getCompoundRate(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#getlastupdateofcompoundrate)
   */
  async getLastUpdateOfCompoundRate(address: string): Promise<string> {
    return (await this.instance.getLastUpdateOfCompoundRate(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#getbalance)
   */
  async getValidatorBalance(address: string): Promise<string> {
    return (await this.instance.getBalance(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#getdelegatedstake)
   */
  async getDelegatedStake(address: string): Promise<string> {
    return (await this.instance.getDelegatedStake(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#getdelegatorsshare)
   */
  async getDelegatorsShare(address: string): Promise<string> {
    return (await this.instance.getDelegatorsShare(address)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/ValidationRewardPools/#getpoolinfo)
   */
  async getPoolInfo(address: string): Promise<ValidatorPoolInfo> {
    const poolInfo = await this.instance.getPoolInfo(address)
    return {
      poolBalance: poolInfo.poolBalance.toString(),
      reservedForClaims: poolInfo.reservedForClaims.toString(),
      aggregatedNormalizedStake: poolInfo.aggregatedNormalizedStake.toString(),
      delegatedStake: poolInfo.delegatedStake.toString(),
      compoundRate: poolInfo.compoundRate.toString(),
      lastUpdateOfCompoundRate: poolInfo.lastUpdateOfCompoundRate.toString(),
      delegatorsShare: poolInfo.delegatorsShare.toString(),
    }
  }
}
