import type { ContractTransaction } from 'ethers'

import { Vesting } from '../../ethers-contracts/Vesting'
import { VotingLockInfo, QNonPayableTx, SignerOrProvider } from '../../types'
import { TimeLockHelperInstance } from '../TimeLockHelperInstance'

/**
 * Vesting instance to interact with Vesting contract.
 * See [onchain documentation](@system-contracts-repo/@network/Vesting/) for more details.
 * An instance of this class for a deployed network can be obtained via {@link ContractRegistryInstance.vesting}
 */
export class VestingInstance extends TimeLockHelperInstance<Vesting> {
  public static readonly registryKey = 'tokeneconomics.vesting'

  public static readonly abi = 'Vesting.json'

  constructor(signerOrProvider: SignerOrProvider, address: string) {
    super(signerOrProvider, VestingInstance.abi, address)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#lock)
   */
  async lock(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('lock', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#announceunlock)
   */
  async announceUnlock(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('announceUnlock', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#unlock)
   */
  async unlock(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('unlock', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#withdraw)
   */
  async withdraw(amount: string, txOptions?: QNonPayableTx): Promise<ContractTransaction> {
    return this.submitTransaction('withdraw', [amount], txOptions)
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#balanceof)
   */
  async balanceOf(userAddress: string): Promise<string> {
    return (await this.instance.balanceOf(userAddress)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#withdrawablebalanceof)
   */
  async withdrawableBalanceOf(account: string): Promise<string> {
    return (await this.instance.withdrawableBalanceOf(account)).toString()
  }

  /**
   * [External documentation](@system-contracts-repo/@network/Vesting/#getlockinfo)
   */
  async getLockInfo(user: string): Promise<VotingLockInfo> {
    const result = await this.instance.getLockInfo({ from: user })
    return {
      lockedAmount: result.lockedAmount.toString(),
      lockedUntil: result.lockedUntil.toString(),
      pendingUnlockAmount: result.pendingUnlockAmount.toString(),
      pendingUnlockTime: result.pendingUnlockTime.toString()
    }
  }
}
