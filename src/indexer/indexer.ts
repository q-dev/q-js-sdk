import axios, { AxiosInstance } from 'axios'

export interface ValidatorStats {
  lastBlockValidated: number
  lastBlockValidatedTime: Date
  lastAvailability: number
}

export interface GetBlockOptions {
  'filter[to]'?: number
  'filter[from]'?: number
  sort?: string
  'page[cursor]'?: number
  'page[limit]'?: number
  'filter[signer]'?: string
  'filter[inturnsigner]'?: string
  'filter[anysigner]'?: string
  'filter[outofturn]'?: boolean
}

export interface BlockInfo {
  id: string
  type: string
  attributes: {
    hash: string
    parent_hash: string
    miner: string
    mainaccount: string
    timestamp: number
    inturn_signer: string
    difficulty: number
  }
}

export interface GetMetricOptions {
  'filter[cycles]'?: number
  'filter[signer]'?: string
  'filter[mainaccount]'?: string
  'page[cursor]'?: string
  'page[limit]'?: number
  sort?: string
}

export interface GetOnchainRootNodeMetricOptions {
  'filter[rootAddress]'?: string
  'page[cursor]'?: number
  'page[limit]'?: number
  sort?: string
}

export interface GetRootNodeMetricOptions {
  cycles?: number,
  rootAddress?: string,
  blocksDelay?: number,
  sort?: 'asc' | 'desc',
}

export interface RootNodeAggregatedOptions {
  rootAddress?: string,
  startBlock?: number,
  sort?: 'asc' | 'desc',
}

export interface RootNodeAggregatedParams {
  'filter[rootAddress]'?: string,
  'filter[startBlock]'?: number,
  'page[limit]': number,
  sort: 'asc' | 'desc',
}


interface MetricStats {
  DueBlocks: number
  InTurnBlocks: number
  OutOfTurnBlocks: number
}

interface Metric {
  id: string
  type: string
  attributes: MetricStats & {
    MainAccount: string
    MetricsByMiner: { [key: string]: MetricStats }
  }
}

export interface VotingItemResponse {
  ProposalId: number,
  VotingType: string,
  VotingStatus: number,
  VotingStartTime: number,
  CurrentQuorum: string,
  RequiredQuorum: string,
  VotingEndTime: number,
  VetoEndTime: number,
  VotingSubType?: number,
  Candidate?: string,
  ReplaceDest?: string,
}

export interface VotingsResponse {
  data: VotingItemResponse[]
  links: {
    self: string,
    prev?: string,
    next?: string
  }
}

export interface VotingOptions {
  filterType?: string;
  filterStatus?: string;
  sort?: 'voting_end_time' | '-voting_end_time';
  pageCursor?: string;
  pageLimit?: number;
}

export interface ValidatorMetric {
  address: string
  dueBlocks: number
  inTurnBlocks: number
  outOfTurnBlocks: number
  inTurnAvailability: number
  totalAvailability: number
}

export interface OnchainRootNodeMetric {
  id: string
  type: string
  attributes: {
    rootAddress: string
    startBlock: number
    endBlock: number
    startTime: number
    endTime: number
  };
}

interface FirstObservedApproval {
  block: number,
  dueCycles: number | null,
}

interface LastObservedApproval {
  block: number,
  offlineCycles: number,
}

export interface RootNodeMetricApproval {
  mainAccount: string,
  firstObservedApproval: FirstObservedApproval,
  lastObservedApproval: LastObservedApproval,
  observedApprovals: {
    alias: string,
    firstObservedApproval: FirstObservedApproval,
    lastObservedApproval: LastObservedApproval
  }[]
}
export interface RootNodeMetric {
  firstTransitionBlock: number
  lastTransitionBlock: number
  byAddress: RootNodeMetricApproval[];
}

export interface RootNodeQTHVotingsAggregated {
  total: {
    all: number
    constitutionVoting?: number
    epdrMembershipVoting?: number
    epqfiMembershipVoting?: number
    eprsMembershipVoting?: number
    generalUpdateVoting?: number
    rootNodesMembershipVoting?: number
    rootNodesSlashingVoting?: number
  }
  byAddress: {
    accountAddress: string,
    counts: {
      all: number
      constitutionVoting?: number
      epdrMembershipVoting?: number
      epqfiMembershipVoting?: number
      eprsMembershipVoting?: number
      generalUpdateVoting?: number
      rootNodesMembershipVoting?: number
      rootNodesSlashingVoting?: number
    }
  }[]
}

export interface RootNodeVotingsAggregated {
  total: {
    all: number
    addressVoting?: number
    emergencyUpdateVoting?: number
    upgradeVoting?: number
    validatorsSlashingVoting?: number
  }
  byAddress: {
    accountAddress: string,
    counts: {
      all: number
      addressVoting?: number
      emergencyUpdateVoting?: number
      upgradeVoting?: number
      validatorsSlashingVoting?: number
    }
  }[]
}

export interface RootNodeProposalsAggregated {
  total: {
    all: number
    emergencyUpdateVoting?: number
    rootNodesSlashingVoting?: number
    validatorsSlashingVoting?: number
  }
  byAddress: {
    accountAddress: string,
    counts: {
      all: number
      emergencyUpdateVoting?: number
      rootNodesSlashingVoting?: number
      validatorsSlashingVoting?: number
    }
  }[]
}

export type L0ListItemStatus = 'active' | 'proposed'
export interface L0AddressEntry {
  signer: string
  mainAccount: string
}
export interface L0RootListItem {
  status: L0ListItemStatus
  timestamp: string;
  signers: L0AddressEntry[] | null,
  roots: L0AddressEntry[],
  activeRootPercentage?: number
}

export interface L0ExclusionListItem {
  status: L0ListItemStatus
  timestamp: string;
  signers: L0AddressEntry[] | null,
  exclusions: L0AddressEntry[],
  activeRootPercentage?: number
}

export class Indexer {
  private _axios: AxiosInstance

  constructor(protected baseURL: string) {
    this._axios = axios.create({ baseURL })
  }

  public async getValidatorStats(
    signers: string[] = [],
    options: GetBlockOptions = { sort: '-id', 'page[limit]': 1000 }
  ): Promise<ValidatorStats[]> {
    if (!options['filter[inturnsigner]'] && !signers)
      throw new Error('Signers list is null!')
    if (signers && options['filter[inturnsigner]'])
      throw new Error('Cannot filter for inturn signer, already have list!')

    const blocks = await this.getBlocks(options)
    return signers.map(signer => {
      const tblocks = blocks.filter(block => {
        const { mainaccount, miner } = block.attributes
        return (mainaccount || miner) === signer.toLowerCase()
      })

      return this.countValidatorStats(tblocks)
    })
  }

  public async getInactiveValidators(signers: string[]): Promise<number> {
    const blocks = await this.getBlocks({ sort: '-id', 'page[limit]': 101 })
    return signers
      .filter(signer => !blocks.some(block => {
        const { mainaccount, miner } = block.attributes
        return (mainaccount || miner) === signer.toLowerCase()
      }))
      .length
  }

  public async getValidatorMetrics(cycles: number): Promise<ValidatorMetric[]> {
    const PAGE_LIMIT = 100
    const metrics: Metric[] = []

    let tempMetrics: Metric[]
    do {
      tempMetrics = await this.getMetrics({
        'filter[cycles]': cycles,
        'page[limit]': PAGE_LIMIT,
        'page[cursor]': metrics[metrics.length - 1]?.id
      })
      metrics.push(...tempMetrics)
    } while (tempMetrics.length === PAGE_LIMIT)

    return metrics.map(metric => this.countValidatorMetric(metric))
  }

  private async getBlocks(options: GetBlockOptions): Promise<BlockInfo[]> {
    const response = await this._axios.get('/blocks', { params: options })
    return response.data.data
  }

  private async getMetrics(options: GetMetricOptions): Promise<Metric[]> {
    const response = await this._axios.get('/metrics', { params: options })
    return response.data.data
  }

  public async getOnchainRootNodeMetrics(
    options: GetOnchainRootNodeMetricOptions
  ): Promise<OnchainRootNodeMetric[]> {
    const response = await this._axios.get(
      '/root-node-metrics/onchain-membership',
      { params: options }
    )
    return response.data.data
  }

  public async getVotings(options: VotingOptions): Promise<VotingsResponse> {
    const response = await this._axios.get('/votings', { params: {
      ...(options.filterType ? { 'filter[type]': options.filterType } : {}),
      ...(options.filterStatus ? { 'filter[status]': options.filterStatus } : {}),
      sort: options.sort || '-voting_end_time',
      ...(options.pageCursor ? { 'page[cursor]': options.pageCursor } : {}),
      'page[limit]': options.pageLimit || 10,

    } })
    return response.data
  }

  public async getVotingsByCursor(cursor: string): Promise<VotingsResponse> {
    const response = await this._axios.get(cursor)
    return response.data
  }

  public async getL0RootList(status: L0ListItemStatus): Promise<L0RootListItem> {
    const { data: { data: { attributes } } } = await this._axios.get('/root-list', {
      params: { status }
    })
    return {
      status: attributes.status,
      timestamp: attributes.timestamp,
      signers: attributes.signers.map(i => ({
        signer: i.signer,
        mainAccount: i.main_account
      })),
      roots: attributes.roots.map(i => ({
        signer: i.signer,
        mainAccount: i.main_account
      })),
      activeRootPercentage: attributes.active_root_percentage
    }
  }

  public async getL0ExclusionList(status: L0ListItemStatus): Promise<L0ExclusionListItem> {
    const { data: { data: { attributes } } } = await this._axios.get('/exclusion-list', {
      params: { status }
    })
    return {
      status: attributes.status,
      timestamp: attributes.timestamp,
      signers: attributes.signers.map(i => ({
        signer: i.signer,
        mainAccount: i.main_account
      })),
      exclusions: attributes.exclusions.map(i => ({
        signer: i.signer,
        mainAccount: i.main_account
      })),
      activeRootPercentage: attributes.active_root_percentage
    }
  }

  public async getRootNodeMetrics(options: GetRootNodeMetricOptions): Promise<RootNodeMetric> {
    const { data: { data: { attributes } } } = await this._axios.get('root-node-metrics', {
      params: {
        ...(options.cycles !== undefined ? { 'filter[cycles]': options.cycles } : {}),
        ...(options.rootAddress ? { 'filter[rootAddress]': options.rootAddress } : {}),
        ...(options.blocksDelay !== undefined ? { 'filter[blocksDelay]': options.blocksDelay } : {}),
        sort: options.sort || 'desc',
        'page[limit]': 100,
      }
    })

    return attributes
  }

  public async getRNQTHVotingsAggregated(options?: RootNodeAggregatedOptions): Promise<RootNodeQTHVotingsAggregated> {
    const { data: { data: { attributes } } } = await this._axios.get('root-node-metrics/q-token-holder-votings/aggregated', {
      params: this.buildRootNodeAggregatedParams(options)
    })

    return attributes
  }

  public async getRNVotingsAggregated(options?: RootNodeAggregatedOptions): Promise<RootNodeVotingsAggregated> {
    const { data: { data: { attributes } } } = await this._axios.get('root-node-metrics/rootnode-votings/aggregated', {
      params: this.buildRootNodeAggregatedParams(options)
    })

    return attributes
  }

  public async getRNProposalsAggregated(options?: RootNodeAggregatedOptions): Promise<RootNodeProposalsAggregated> {
    const { data: { data: { attributes } } } = await this._axios.get('root-node-metrics/rootnode-proposals/aggregated', {
      params: this.buildRootNodeAggregatedParams(options)
    })

    return attributes
  }

  private countValidatorStats(blocks: BlockInfo[]): ValidatorStats {
    return {
      lastBlockValidated: Number(blocks[0]?.id ?? 0),
      lastBlockValidatedTime: new Date((blocks[0]?.attributes.timestamp ?? 0) * 1000),
      lastAvailability: blocks.length / 10
    }
  }

  private countValidatorMetric(metric: Metric): ValidatorMetric {
    const attrs = metric.attributes
    return {
      address: attrs.MainAccount,
      dueBlocks: attrs.DueBlocks,
      inTurnBlocks: attrs.InTurnBlocks,
      outOfTurnBlocks: attrs.OutOfTurnBlocks,
      inTurnAvailability: attrs.DueBlocks > 0
        ? attrs.InTurnBlocks / attrs.DueBlocks
        : 0,
      totalAvailability: attrs.DueBlocks > 0
        ? (attrs.InTurnBlocks + attrs.OutOfTurnBlocks) / attrs.DueBlocks
        : 0
    }
  }

  private buildRootNodeAggregatedParams(options?: RootNodeAggregatedOptions): RootNodeAggregatedParams {
    return {
      ...(options?.rootAddress ? { 'filter[rootAddress]': options.rootAddress } : {}),
      ...(options?.startBlock !== undefined ? { 'filter[startBlock]': options.startBlock } : {}),
      sort: options?.sort || 'desc',
      'page[limit]': 100,
    }
  }
}
